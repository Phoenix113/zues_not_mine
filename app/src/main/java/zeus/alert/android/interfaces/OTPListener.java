package zeus.alert.android.interfaces;

/**
 * Created by Jethva Sagar (Piml Sid) on 1/18/17.
 * Email : Jethvasagar2@gmail.com
 */

public interface OTPListener {
    public void otpReceived(String messageText);
}
