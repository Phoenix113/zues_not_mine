package zeus.alert.android.models;

/**
 * Created by moubeen on 8/2/16.
 */
public class Coupon {


    private String _Coupon_Id = "";
    private String _Promo_Name = "";
    private String _Description = "";
    private String _amount_deduct = "";


    public String get_Coupon_Id() {
        return _Coupon_Id;
    }

    public void set_Coupon_Id(String _Coupon_Id) {
        this._Coupon_Id = _Coupon_Id;
    }

    public String get_Promo_Name() {
        return _Promo_Name;
    }

    public void set_Promo_Name(String _Promo_Name) {
        this._Promo_Name = _Promo_Name;
    }

    public String get_Description() {
        return _Description;
    }

    public void set_Description(String _Description) {
        this._Description = _Description;
    }

    public String get_amount_deduct() {
        return _amount_deduct;
    }

    public void set_amount_deduct(String _amount_deduct) {
        this._amount_deduct = _amount_deduct;
    }
}
