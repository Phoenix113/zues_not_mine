package zeus.alert.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import zeus.alert.android.R;
import zeus.alert.android.interfaces.Verfication_Code_Receive_Listener;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.Country_Codes;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Moubeen Warar on 11/7/2015.
 */
public class Register_User extends Activity implements Verfication_Code_Receive_Listener {

    public static String isfb="";
    public static String fbid ="";


    @InjectView(R.id.username)
    EditText _UserName;

    @InjectView(R.id.email)
    EditText _Email;

    @InjectView(R.id.password)
    EditText _Password;

    @InjectView(R.id.confirm_password)
    EditText _Confirm_Password;

    @InjectView(R.id.counrty_code)
    TextView _Country_Code_ET;

    @InjectView(R.id.number)
    EditText _Number;

    @InjectView(R.id.term_and_condition)
    TextView _Term_and_Condiotion;

    @InjectView(R.id.sign_up)
    Button _SignUp_Button;

    Country_Codes _Country_codes;

    List<String> _Codes = new ArrayList<String>();
    ArrayList<String> _Names = new ArrayList<String>();
    String _Selected_Code = "";



    private String _Selected_Country_Code = "";

    private String _Selected_Country_Name = "";
    String number = "";

    JsonObjectRequest register_User_Request_To_Server;
    private String country_code="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register_user);

        ButterKnife.inject(this);

        Utils.TRANSLUCENT_STATUS_BAR(this);

        _Country_codes = new Country_Codes();

        get_Data_From_Previous_Activity();

        Create_Listerners_For_TextViews();


        try {

            JSONObject jsonObject = new JSONObject(_Country_codes.get_Country_Codes());

            JSONArray _Countries = jsonObject.getJSONArray("countries");

            for (int i = 0; i < _Countries.length(); i++) {

                JSONObject _Country = _Countries.getJSONObject(i);
                String _name = _Country.getString("name");
                String _code = _Country.getString("code");

                _Codes.add(_code);
                _Names.add(_name);

            }


        } catch (Exception e) {

            Log.e("", "exception in codes : " + e.toString());

        }


        _Country_Code_ET.setText("Select Country");
        _Country_Code_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Register_User.this, NumberVerificaiton.class);
                intent.putStringArrayListExtra("country_names", _Names);
                startActivityForResult(intent, 1);
            }
        });


//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Register_User.this,
//                R.layout.spinner_item, _Names);
//        //  dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        _Country_Code_ET.setAdapter(dataAdapter);
//
//        _Country_Code_ET.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                if (!_Names.get(position).equals("Select country")) {
//                    _Number.setText("" + _Codes.get(position));
//                    _Selected_Code = _Codes.get(position);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

/*

        _SmsReceiver = new smsReceiver(this);
        mIntentFilter = new IntentFilter();

        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(_SmsReceiver, mIntentFilter);


        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        String number = tm.getLine1Number();
*/

        //_Number.setText(number);
      //  _Number.setEnabled(false);

    }

    private void get_Data_From_Previous_Activity() {

        Intent intent = getIntent();

        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
        isfb = intent.getStringExtra("isfb");
        fbid = intent.getStringExtra("fbid");

        if (!name.equals("")) {

            _UserName.setText("" + name);
        }
        if (!email.equals("")) {

            _Email.setText("" + email);

        }
    }

    private void Create_Listerners_For_TextViews() {
        _Term_and_Condiotion.setText(Html.fromHtml(getString(R.string.terms_text)));

    }


    @OnClick(R.id.sign_up)
    public void register_User(View view) {

            Register_User_On_Server();
    }

    private void Register_User_On_Server() {


        String username = _UserName.getText().toString().trim();
        String email = _Email.getText().toString().trim();
        String password = _Password.getText().toString().trim();
        String confirm_password = _Confirm_Password.getText().toString().trim();
        number = _Number.getText().toString().trim().replaceAll(" ","");
        number = country_code + number;

        if (Utils.DETECT_INTERNET_CONNECTION(this)) {


            if (!username.equals("")) {


                if (!email.equals("")) {

                    if (Utils.IS_EMAIL_VALID(email)) {


                        if (!password.equals("")) {


                            if (confirm_password.equals("")) {
                                Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_confirm_password));
                            } else {

                                if (password.equals(confirm_password)) {


                                    if (number.equals("")) {
                                        Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_number));
                                    } else {


                                        // Now_verify_Phone_Number();

                                        if (_Selected_Country_Code.equals("")) {
                                            Utils.TOAST_WRONG_CREDENTIAL(this, "Please Enter Country");
                                        } else {

//                                            if (number.contains("+")) {
//                                                Utils.TOAST_WRONG_CREDENTIAL(this, "No Need To Add Country Code");
//                                            } else {
//                                                if (number.contains(_Selected_Country_Code)) {
//                                                    Utils.TOAST_WRONG_CREDENTIAL(this, "No Need To Add Country Code");
//                                                } else {

//                                                    if (number.startsWith("0")) {
//
//                                                        StringBuilder newphone = new StringBuilder(number);
//                                                        newphone.setCharAt(0, ' ');
//
//                                                        number = newphone.toString().replace(" ", "").trim();
//
//                                                        number = _Selected_Country_Code + newphone.toString().replace(" ", "").trim();
//
//                                                    } else {
//
//                                                        number = _Selected_Country_Code + number;
//                                                    }

                                                Log.e("zeus", "number is : " + number);


                                                if (number.length() > 6 && number.length() < 15) {


                                                    if (username.contains(" ")) {

                                                        Utils.TOAST_ERROR_RESPONSE(this, "Username cannot contain spaces");
                                                    } else {

                                                        Log.d("RRR","OTP SEND :: "+number);



                                                        Random random=new Random();
                                                        int otpno = 100000 + random.nextInt(900000);
                                                        /*-- @Copyright : piml sid--*/
                                                        String otp_msg="Your verification code is "+otpno+".";

                                                        Log.d("RRR","random number n :: "+ otpno);

                                                        sendOTP(username,email,password,confirm_password,_Selected_Country_Code,number,otp_msg,""+otpno);


                                                        //Now_Register_User(number);

                                                    }
                                                } else {
                                                    Utils.TOAST_WRONG_CREDENTIAL(this, "Wrong Phone Number");
                                                }
                                                //  }
                                           // }

                                        }


                                    }

                                } else {
                                    Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.password_not_match));
                                }

                            }


                        } else {

                            Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_password));
                        }


                    } else {
                        Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.invalid_email));
                    }

                } else {

                    Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_email));

                }


            } else {
                Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_username));
            }


        } else {
            Utils.TOAST_NO_INTERNET_CONNECTION(this, "" + getResources().getString(R.string.no_internet_connection));
        }


    }

    /*private void Now_verify_Phone_Number(final String number) {

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("phoneNumber", "" + number);

        Utils.show_Dialog(Register_User.this, "Verifying Phone number");

        ParseCloud.callFunctionInBackground("sendVerificationCode", params, new FunctionCallback<String>() {


            @Override
            public void done(String response, com.parse.ParseException e) {

                if (e == null) {
                    Log.e("Response", "no exceptions! " + response.toString());


                    //   Toast.makeText(RegisterScreen.this,  "no exceptions! " + response.toString(), Toast.LENGTH_LONG).show();

                    //   Intent intent = new Intent(Register_User.this, NumberVerificaiton.class);
                    //  startActivityForResult(intent, 1);


                    // Code sent successfully you have to wait it or ask the user to enter the code for verification
                } else {
                    Log.e("Now_verify_Phone_Number", "Exception is :" + e);
                    if (e.toString().contains("400") || e.toString().contains("is not a valid phone number.")) {

                        Utils.TOAST_ERROR_RESPONSE(Register_User.this, "Invalid Phone number");
                    } else {

                        Utils.TOAST_ERROR_RESPONSE(Register_User.this, "" + getResources().getString(R.string.went_wrong));

                    }

                    _SignUp_Button.setEnabled(false);
                    ParseUser parseUser = ParseUser.getCurrentUser();
                    parseUser.deleteInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            _SignUp_Button.setEnabled(true);
                            Log.e("zeus", "user deleted cos of excption in Now_verify_Phone_Number");
                            Utils.hide_dialog();
                        }
                    });
                    Utils._USER = null;
                    Utils.SAVE_USER_TO_SHAREDPREFS(Register_User.this, Utils._USER);


                }


            }
        });

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {

                String _Country_Name = data.getStringExtra("result");

                _Country_Code_ET.setText(_Country_Name);

                for (int i = 0; i < _Names.size(); i++) {

                    if (_Names.get(i).equalsIgnoreCase(_Country_Name)) {

                        _Selected_Country_Code = _Codes.get(i).replace(" ", "");

                        _Selected_Country_Name = _Names.get(i);

                        country_code = _Selected_Country_Code;

                        break;
                    }
                }

                Log.e("user app", "_Selected_Country_Name: " + _Selected_Country_Name);
                Log.e("user app", "country code : " + _Selected_Country_Code);

            }

        } else if (resultCode == RESULT_CANCELED) {


            _SignUp_Button.setEnabled(false);
           /* ParseUser parseUser = ParseUser.getCurrentUser();
            parseUser.deleteInBackground(new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    _SignUp_Button.setEnabled(true);
                }
            });*/


        }

    }


 /*   private void verify_That_Code(String _Code) {
        // TODO Auto-generated method stub

//        HashMap<String, Object> params = new HashMap<String, Object>();
//        params.put("phoneNumber", _Number.getText().toString());
//        params.put("phoneVerificationCode", _Code);
//        Log.e("", "phone Number : " + _Number.getText().toString());
        Log.e("zeus", "Code : " + _Code);
//        ParseCloud.callFunctionInBackground("verifyPhoneNumber", params, new FunctionCallback<String>() {
//
//
//            @Override
//            public void done(String response, com.parse.ParseException e) {
//
//                if (e == null) {
//
//                    Utils.hide_dialog();
//                    Log.e("Response", "phone verified : ");
//                    Intent intent = new Intent(Register_User.this, App_Home_Page.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
//                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    startActivity(intent);
//                    finish();
//                    // Now_Register_User(number);
//
//
//                } else {
//                    Log.e("Response", "Exception: " + response + e);
//                    _SignUp_Button.setEnabled(false);
//                    ParseUser parseUser = ParseUser.getCurrentUser();
//                    parseUser.deleteInBackground(new DeleteCallback() {
//                        @Override
//                        public void done(ParseException e) {
//                            _SignUp_Button.setEnabled(true);
//
//                            Utils.hide_dialog();
//
//                        }
//                    });
//
//                    if (e.toString().contains("Invalid verification cod")) {
//                        Utils.TOAST_ERROR_RESPONSE(Register_User.this, "Invalid code. Try again");
//                    }
//
//
//                }
//
//            }
//        });

    }*/




    private void sendOTP(final String username, final String email, final String password, final String confirm_password,
                         final String _Selected_Country_Code, final String number, final String otp_msg, final String otpno) {
        Log.d("msg",otpno);


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.REGISTORUSERMESSAGESEND(getApplicationContext());
        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                Intent intent =new Intent(Register_User.this,RegisterOTP.class);
                                intent.putExtra("isfb",isfb);
                                intent.putExtra("fbid",fbid);
                                intent.putExtra("username",username);
                                intent.putExtra("email",email);
                                intent.putExtra("password",password);
                                intent.putExtra("confirm_password",confirm_password);
                                intent.putExtra("country",_Selected_Country_Code);
                                intent.putExtra("number",number);
                                intent.putExtra("otp",otpno);
                                startActivity(intent);
                                //   Utils.TOAST_SUCCESS_RESPONSE(getApplicationContext(),"Message Send Succefully");
                            }
                        } catch (JSONException e) {

                            Log.d("rathod","JSON EXCEPTION :: "+e.getMessage());
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("rathod","onErrorResponse :: "+error.getMessage());
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("num",number);
                params.put("msg",otp_msg);
                Log.d("RRR","num :: "+number);
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister the SMS receiver
       // unregisterReceiver(_SmsReceiver);


    }

    @Override
    public void _Verification_Code_Received(String code) {

        Log.e("zeus", "code is : " + code);
      //  verify_That_Code(code);

    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);

    }




    /*
    * below  code is not use for this activity
    * */
    private void _Go_To_APp_Home_Page() {


        if (Utils._LONGITUDE == 0.0 && Utils._LATITUDE == 0.0) {


            Utils.GET_LAST_KNOWN_LOCATION(Register_User.this);

            if (Utils._LONGITUDE == 0.0 && Utils._LATITUDE == 0.0) {

                Utils._Show_Ok_Dialog(Register_User.this, "Alert", "Your Gps is not working Properly. Restart it and comeback. But Your account is registered with us");

            } else {
                Intent intent = new Intent(Register_User.this, App_Home_Page.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }

        } else {
            Intent intent = new Intent(Register_User.this, App_Home_Page.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }


    }

    private void Now_Register_User(final String number) {


        //   ParseUser parseUser = new ParseUser();

        final String username = _UserName.getText().toString();
        final String password = _Password.getText().toString();
        final String email = _Email.getText().toString();
        // String city = Utils.GET_USER_CITY_NAME(Register_User.this);
        String countryName = _Selected_Country_Name;

        Utils.show_Dialog(Register_User.this, "Registering user");
        Utils.HIDE_KEYBOARD(_UserName, Register_User.this);

//
        String register_User_Url = Webservices.REGISTER_USER(this, username, password, email, "" + Utils._LATITUDE, "" + Utils._LONGITUDE, countryName, number,"0");

        Log.e("jeeeni", "login url : " + register_User_Url);

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    if (_Status) {

                        User _User = new User();

                        sendMessage(number,"You have register in Zues");

                        JSONObject jsonObject=response.getJSONObject("user");

                        _User.set_User_Id(jsonObject.getString("uid"));
                        _User.set_User_Name(jsonObject.getString("username"));
                        _User.set_Email(jsonObject.getString("email"));
                        _User.set_Phone(jsonObject.getString("phone_number"));
                        _User.set_Height(jsonObject.getString("height"));
                        _User.set_Weight(jsonObject.getString("weight"));


                        if(jsonObject.getString("is_safe").equals("1"))
                        {
                            _User.set_Safe(true);
                        }else
                        {
                            _User.set_Safe(false);
                        }

                        if(jsonObject.getString("is_login").equals("1"))
                        {
                            _User.set_Login(true);
                        }else
                        {
                            _User.set_Login(false);
                        }


                        _User.set_Lat(jsonObject.getString("lat"));
                        _User.set_Lng(jsonObject.getString("lon"));

                        _User.set_User_Type("0");

                        Utils.SAVE_USER_TO_SHAREDPREFS(Register_User.this, _User);


                        _Go_To_APp_Home_Page();

                    } else {
                        Utils.TOAST_ERROR_RESPONSE(Register_User.this, "" + response.getString("error_msg"));
                    }

                    Utils.hide_dialog();
                } catch (Exception e) {
                    Log.e("zues", "exception in register user : " + e.toString());
                    Utils.hide_dialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //    Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);


//        parseUser.signUpInBackground(new SignUpCallback() {
//
//
//            @Override
//            public void done(com.parse.ParseException e) {
//
//                if (e == null) {
//
//                    Log.e("", "User Registered : ");
//                    User user = new User();
//
//                    user.set_User_Name(_UserName.getText().toString());
//                    user.set_Email(_Email.getText().toString());
//                    user.set_Password(_Password.getText().toString());
//                    user.set_Phone(_Number.getText().toString());
//                    user.set_Login(true);
//                    user.set_Safe(true);
//                    Utils._USER = user;
//                    Utils._PARSE_USER = ParseUser.getCurrentUser();
//
//                    Utils.SAVE_USER_TO_SHAREDPREFS(Register_User.this, Utils._USER);
//                    Utils.hide_dialog();
//                    try {
//                        Thread.sleep(200);
//                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
//                    }
//                    Now_verify_Phone_Number(number);
//
//
//                } else if (e.getCode() == com.parse.ParseException.USERNAME_TAKEN) {
//                    Utils.hide_dialog();
//                    Utils.TOAST_ERROR_RESPONSE(Register_User.this, "" + getResources().getString(R.string.user_already_exists));
//                } else {
//                    Utils.hide_dialog();
//                    Utils.TOAST_ERROR_RESPONSE(Register_User.this, "" + getResources().getString(R.string.went_wrong));
//                }
//
//            }
//        });


    }
    //mesage send service
    private void sendMessage(final String num, final String msg) {
        Log.d("msg",msg);


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.REGISTORUSERMESSAGESEND(getApplicationContext());
        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                //   Utils.TOAST_SUCCESS_RESPONSE(getApplicationContext(),"Message Send Succefully");
                            }
                        } catch (JSONException e) {
                            Log.d("RRR","JSONException :: "+e.getMessage());
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("RRR","Error :: "+error.getMessage());
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("num",num);
                params.put("msg",msg);
                Log.d("RRR","num :: "+num);
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);

    }
}
