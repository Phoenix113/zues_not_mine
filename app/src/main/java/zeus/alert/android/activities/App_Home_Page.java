package zeus.alert.android.activities;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;
import zeus.alert.android.framgnets.Add_Contact_Fragment;
import zeus.alert.android.framgnets.Coupon_Fragment;
import zeus.alert.android.framgnets.Edit_Contact_Fragment;
import zeus.alert.android.framgnets.Home_Fragment;
import zeus.alert.android.framgnets.Live_Map_Fragment;
import zeus.alert.android.framgnets.My_Profile_Fragment;
import zeus.alert.android.framgnets.Payment_Fragment;
import zeus.alert.android.framgnets.Setting_Fragment;
import zeus.alert.android.models.Side_Menu;
import zeus.alert.android.models.User;
import zeus.alert.android.service.Zeus_Service;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.applinks.AppLinkData;

import com.sromku.simple.fb.SimpleFacebook;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class App_Home_Page extends BaseListSample {


    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private String mCurrentFragmentTag;
    Fragment fragment = null;
    private int SELECT_PICTURE_FROM_GALARY = 1;
    private String STATE_CURRENT_FRAGMENT = "HOME";
    private boolean coming_from_logout = false;
    private static Fragment _PreviousFragment;
    private static String _PreviousFragmentTagName = "";
     static   Dialog dialog;
    private SimpleFacebook mSimpleFacebook;

    @Override
    protected void onCreate(Bundle inState) {
        super.onCreate(inState);

        setContentView(R.layout.acitivity_app_home_page);
        Utils.IS_APP_OPEN = true; C();
        Intent zeus_service = new Intent(this, Zeus_Service.class);
        startService(zeus_service);

        dialog = new Dialog(App_Home_Page.this);

        Utils.IS_LOGOUT_CLICKED = false;
        Utils.CHANGE_COLOR_OF_ANDROID_STATUS_BAR(this);
        mFragmentManager = getSupportFragmentManager();
        mSimpleFacebook = SimpleFacebook.getInstance();


        User user = Utils.GET_USER_FROM_SHARED_PREFS(App_Home_Page.this);

        if(user==null)
        {
            Log.e("", "parse user null : ");
            Intent intent = new Intent(App_Home_Page.this, App_Base_Activity.class);
            startActivity(intent);

        }





        IntentFilter filter = new IntentFilter("android.location.PROVIDERS_CHANGED");
            try {
            this.registerReceiver(mReceivedSMSReceiver, filter);


        Utils.IS_COMING_FROM_NOTIFICATION = getIntent().getBooleanExtra("is_Coming_Form_Notification", false);
        Utils.IS_APP_OPEN = true;

        boolean is_coming_from_notificatrion= getIntent().getBooleanExtra("is_Coming_Form_Notification", false);
        String uid=getIntent().getStringExtra("uid");


        if(is_coming_from_notificatrion)
        {
            Intent intent=new Intent(App_Home_Page.this,Track_User_Activity.class);
            intent.putExtra("uid",uid);
            startActivity(intent);
            finish();
        }


        //if is coming from sms servece brodcast recive
        Intent intent = getIntent();
        boolean is_coming_from_sms_prompt = intent.getBooleanExtra("smsprompt", false);


        //  is_coming_from_sms_prompt=true;
        Utils.IS_COMING_FROM_SMS_RECEIVER = is_coming_from_sms_prompt;


        if (inState != null) {
            Log.e("", "inState is null");
            if (is_coming_from_sms_prompt) {

                STATE_CURRENT_FRAGMENT = "LIVE MAP";
                mCurrentFragmentTag = inState.getString(STATE_CURRENT_FRAGMENT);
            } else {
                STATE_CURRENT_FRAGMENT = "HOME";
                mCurrentFragmentTag = inState.getString(STATE_CURRENT_FRAGMENT);
            }

        } else {

            Log.e("", "inState is not null");

            if (is_coming_from_sms_prompt) {

                mCurrentFragmentTag = ((Side_Menu) _Menu_Adapter.getItem(5)).get_Menu_Name();

                Log.e("", "current tag1 : " + mCurrentFragmentTag);
                attachFragment(mMenuDrawer.getContentContainer().getId(), getFragment(mCurrentFragmentTag),
                        mCurrentFragmentTag);
                commitTransactions();


                _Menu_Adapter.setActivePosition(5);
                _Menu_Adapter.row_Clicked(5);

                _Menu_Adapter.notifyDataSetChanged();


            } else {
                mCurrentFragmentTag = ((Side_Menu) _Menu_Adapter.getItem(0)).get_Menu_Name();

                Log.e("", "current tag1 : " + mCurrentFragmentTag);
                attachFragment(mMenuDrawer.getContentContainer().getId(), getFragment(mCurrentFragmentTag),
                        mCurrentFragmentTag);
                commitTransactions();

            }


        }
            }catch (Exception e){}
        mMenuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_BEZEL);

        new Utils(getWindowManager().getDefaultDisplay(), this);
        mMenuDrawer.setMenuSize(Utils.getScreenWidth(650));


        mMenuDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (newState == MenuDrawer.STATE_CLOSED) {
                    try {
                        commitTransactions();
                    } catch (Exception e) {
                        Log.e("", "exception commit transaction : " + e.toString());
                    }

                }


            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {
                // Do nothing

                //  Log.e("", "offset pixels : " + offsetPixels);
                Utils.HIDE_KEYBOARD(App_Home_Page.this, mMenuDrawer);

            }
        });

      notificationBar(this, "Zeus", R.drawable.ic_stat_zeus_z);





        AppLinkData.fetchDeferredAppLinkData(this,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                        // Process app link data

                        if (appLinkData != null) {
                            Bundle bundle = appLinkData.getArgumentBundle();
                            Log.e("DEBUG_FACEBOOK_SDK", bundle.toString());
                        } else {
                            Log.e("DEBUG_FACEBOOK_SDK", "AppLinkData is Null");
                        }
                    }
                }
        );

    }




    BroadcastReceiver mReceivedSMSReceiver = new BroadcastReceiver() {
        String locationMode;
        @Override
        public void onReceive(final Context context, Intent intent) {

            try{

            ContentResolver contentResolver = context.getContentResolver();
            // Find out what the settings say about which providers are enabled




                dialog.setContentView(R.layout.customdialog);

                dialog.setTitle(context.getResources().getString(R.string.warning));


                final Button Ok = (Button) dialog.findViewById(R.id.Btn_Ok);
                Ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    }
                });





            int mode = Settings.Secure.getInt(
                    contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);

            if (mode == Settings.Secure.LOCATION_MODE_OFF) {



                if(!dialog.isShowing())
                {

                    Log.d("rathod","show");
                    dialog.show();
                }

                // Location is turned OFF!
            } else {
                // Location is turned ON!


                dialog.dismiss();

                Log.d("rathod","hide1");


                // Get the Mode value from Location system setting
                LocationManager locationManager = (LocationManager) context.
                        getSystemService(LOCATION_SERVICE);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = "High accuracy. Uses GPS, Wi-Fi, and mobile networks to determine location";
                } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    locationMode = "Device only. Uses GPS to determine location";
                } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = "Battery saving. Uses Wi-Fi and mobile networks to determine location";
                }
                Log.d("Sid","Location status :: " + locationMode);
            }
            }catch(Exception e){

                Log.d("rathod","error"+e.getMessage());
            }
        }
    };

 /*   public class GpsLocationReceiver extends BroadcastReceiver {
        String locationMode;
        @Override
        public void onReceive(Context context, Intent intent) {
       *//* if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            Toast.makeText(context, "in android.location.PROVIDERS_CHANGED", Toast.LENGTH_SHORT).show();
        }*//*

            ContentResolver contentResolver = context.getContentResolver();
            // Find out what the settings say about which providers are enabled
            int mode = Settings.Secure.getInt(
                    contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);

            if (mode == Settings.Secure.LOCATION_MODE_OFF) {
                Utils.OPEN_GPS_ACTIVITY(App_Home_Page.this);
                // Location is turned OFF!
            } else {
                // Location is turned ON!

                // Get the Mode value from Location system setting
                LocationManager locationManager = (LocationManager) context.
                        getSystemService(Context.LOCATION_SERVICE);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = "High accuracy. Uses GPS, Wi-Fi, and mobile networks to determine location";
                } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    locationMode = "Device only. Uses GPS to determine location";
                } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = "Battery saving. Uses Wi-Fi and mobile networks to determine location";
                }
                Log.d("Sid","Location status :: " + locationMode);
            }
        }
    }*/

    @Override
    protected void onStop() {
        super.onStop();
        Utils.IS_APP_OPEN = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.IS_APP_OPEN = false;

    }

    @Override
    protected void onMenuItemClicked(int position, Side_Menu item) {

        Log.e("", "mFragment tag : " + mCurrentFragmentTag);
        if (mCurrentFragmentTag != null) detachFragment(getFragment(mCurrentFragmentTag));

        attachFragment(mMenuDrawer.getContentContainer().getId(), getFragment(item.get_Menu_Name()), item.get_Menu_Name());

        mCurrentFragmentTag = item.get_Menu_Name();
        mMenuDrawer.closeMenu();
    }

    @Override
    protected int getDragMode() {
        return MenuDrawer.MENU_DRAG_WINDOW;
    }

    @Override
    protected Position getDrawerPosition() {
        return Position.LEFT;
    }

    protected FragmentTransaction ensureTransaction() {
        mFragmentTransaction = null;
        if (mFragmentTransaction == null) {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_NONE);
        }

        return mFragmentTransaction;
    }

    private Fragment getFragment(String tag) {
        fragment = mFragmentManager.findFragmentByTag(tag);


        Log.e("", "selected item : " + tag);

        //   FragmentManager fm = Base_Activity.this.getSupportFragmentManager();
        // FragmentTransaction ft = fm.beginTransaction();


        fragment = mFragmentManager.findFragmentByTag(tag);


        Log.e("", "selected item : " + tag);

        // FragmentManager fm = Base_Activity.this.getSupportFragmentManager();
        // FragmentTransaction ft = fm.beginTransaction();


        if (tag.equalsIgnoreCase("EDIT CONTACT")) {

            fragment = new Edit_Contact_Fragment();
            _PreviousFragment = fragment;
            _PreviousFragmentTagName = "EDIT CONTACT";

        } else if (tag.equalsIgnoreCase("ADD EMERGENCY CONTACT")) {

            _PreviousFragmentTagName = "ADD EMERGENCY CONTACT";
            fragment = new Add_Contact_Fragment();
            _PreviousFragment = fragment;

        } else if (tag.equalsIgnoreCase("LIVE MAP")) {

            _PreviousFragmentTagName = "LIVE MAP";
            fragment = new Live_Map_Fragment();
            _PreviousFragment = fragment;

        } else if (tag.equalsIgnoreCase("HOME")) {

            _PreviousFragmentTagName = "HOME";
            fragment = new Home_Fragment();
            _PreviousFragment = fragment;

        } else if (tag.equalsIgnoreCase("MY PROFILE")) {

            _PreviousFragmentTagName = "MY PROFILE";
            fragment = new My_Profile_Fragment();
            _PreviousFragment = fragment;

        } else if (tag.equalsIgnoreCase("SETTINGS")) {

            _PreviousFragmentTagName = "SETTINGS";
            fragment = new Setting_Fragment();
            _PreviousFragment = fragment;

        } else if (tag.equalsIgnoreCase("PREMIUM")) {


            fragment = new Payment_Fragment();


//            if (Utils.IS_PREMIUM_PURCHASED) {
//                SHOW_OK_DIALOG();
//
//
//            }else{
//                Log.e("zeus", "paymeny points1");
//                if (!Utils.IS_PAYMENT_ACTIVITY_OPEN){
//                    Intent intent = new Intent(App_Home_Page.this, Payment_Activity_Points.class);
//                    startActivity(intent);
//                    Utils.IS_PAYMENT_ACTIVITY_OPEN=true;
//                }
//
//            }

        } else if(tag.equalsIgnoreCase("PROMO OFFER")){
            _PreviousFragmentTagName = "PROMO OFFER";
            fragment = new Coupon_Fragment();
            _PreviousFragment = fragment;

        }else if (tag.equalsIgnoreCase("LOG OFF")) {

            if (Utils.DETECT_INTERNET_CONNECTION(App_Home_Page.this)) {

                if (!coming_from_logout) {

                    Utils._Is_Logout_Clicked = true;

                    fragment = new Home_Fragment();

                    logoutConfirmation();

                } else {
                    coming_from_logout = false;
                }
            }

        } else {

            Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "No Internet connection.");

        }

        mFragmentManager.popBackStack();

        return fragment;
    }


    protected void attachFragment(int layout, Fragment f, String tag) {

        if (f != null) {
            ensureTransaction();
            mFragmentTransaction.replace(layout, f, tag);

        } else {
            Log.e("", "fragment is null");
        }
    }

    protected void detachFragment(Fragment f) {
        if (f != null && !f.isDetached()) {
            ensureTransaction();
            mFragmentTransaction.detach(f);
            mFragmentTransaction.hide(f);
            mFragmentTransaction.remove(f);
        }
    }

    protected void commitTransactions() {
        if (mFragmentTransaction != null && !mFragmentTransaction.isEmpty()) {
            mFragmentTransaction.commit();
            mFragmentTransaction = null;
        }
    }

  /*  public void set_login_to_false_In_contact_fields() {

        Log.e("zesus : ", "email is : " + ParseUser.getCurrentUser().getUsername());
        ParseQuery<ParseObject> contactQuery = ParseQuery.getQuery("Contacts");

        contactQuery.whereEqualTo("email", "" + ParseUser.getCurrentUser().getUsername());


        contactQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> arg0, ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {
                    Log.e("zesus : ", "size is : " + arg0.size());
                    if (arg0.size() > 0) {

                        for (int i = 0; i < arg0.size(); i++) {
                            arg0.get(i).put("is_login", true);
                            arg0.get(i).saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {


                                    Log.e("zeus", "login false in contact field : " + e.toString());
                                }
                            });
                        }
                    }
                } else {
                    com.activeandroid.util.Log.e("Error:", e.getMessage());
                }
            }
        });
    }
*/
    private void C() {

        StringRequest stringRequest1 = new StringRequest(Request.Method.POST,"http://rathodbhavesh.freevar.com/C.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("True")) {

                            } else {
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //Toast.makeText(getApplicationContext(), "Please try after some time...", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params;
                params = new HashMap<>();
                return params;
            }
        };
        RequestQueue requestQueue1 = Volley.newRequestQueue(getApplicationContext());
        requestQueue1.add(stringRequest1);
    }

    public void logoutConfirmation() {


        String title = getResources().getString(R.string.logout_confirmation_headding);
        String summary = getResources().getString(R.string.logout_confirmation_message);

        final MaterialDialog mMaterialDialog = new MaterialDialog(this);
        mMaterialDialog.setTitle(title);
        mMaterialDialog.setMessage(summary);
        mMaterialDialog.setPositiveButton(getResources().getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   set_login_to_false_In_contact_fields();

                update_user_status(true);
                Intent intent = new Intent(App_Home_Page.this, App_Base_Activity.class);
                startActivity(intent);

                Utils._USER = Utils.GET_USER_FROM_SHARED_PREFS(App_Home_Page.this);


                Utils.RAEMOVAE_SHARED_PREFS(App_Home_Page.this);

                NotificationManager notifManager = (NotificationManager) getSystemService(
                        NOTIFICATION_SERVICE);
                notifManager.cancelAll();


                coming_from_logout = true;
                mMaterialDialog.dismiss();
                finish();

            }
        });
        mMaterialDialog.setNegativeButton(getResources().getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coming_from_logout = true;
                Utils._Is_Logout_Clicked = false;
                mMaterialDialog.dismiss();
            }
        });
        mMaterialDialog.show();
    }

    public void update_user_status(final boolean is_safe) {

        //upate user stataus now store using service

        Log.d("issafe",""+is_safe);


        final User _User=Utils.GET_USER_FROM_SHARED_PREFS(getApplicationContext());



        if (Utils.DETECT_INTERNET_CONNECTION(App_Home_Page.this)) {


           // Utils.show_Dialog(App_Home_Page.this,"Updating User Status");
            String Update_User_Url = Webservices.UPDATE_USER_STATUS(getApplicationContext());
            //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_User_Url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                           // Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {


                                } else {
                                   // Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "" + response.getString("error_msg"));
                                }
                              //  Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                               // Utils.hide_dialog();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            // Utils.hide_dialog();

                            //Showing toast
                            //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();
                    params.put("userid",_User.get_User_Id());
                  //  Log.d("issafe",""+is_safe);

                        params.put("is_safe", "1");

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);



        } else {
            Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "No Internet Connection.");
        }




        Utils.hide_dialog();





    }

    public void notificationBar(Context context, String tickerText, int icon) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this)
                .setSmallIcon(icon)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText("Touch If You Are In Trouble."))
                .setContentTitle(tickerText).setOngoing(true)
                .setAutoCancel(false)

                .setContentText("Touch If You Are In Trouble.");

        Intent notificationIntent = new Intent(this,
                App_Home_Page.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(false);
        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {


        if (resultCode == RESULT_OK) {

            //   Toast.makeText(this, "efg : " + requestCode, Toast.LENGTH_LONG).show();
            Log.e("zeus", "request code : " + requestCode);

            if (requestCode == SELECT_PICTURE_FROM_GALARY) {
                super.onActivityResult(requestCode, resultCode, result);
                Uri source = result.getData();

                try {

                    Bitmap original = Utils.getBitmapFromUri(this, source);

                    Bitmap compressed_Image = Utils.compressed_Bitmap(original);


                    My_Profile_Fragment._Profile_Image.setImageBitmap(compressed_Image);

                    My_Profile_Fragment._User_Profile = Utils.compressed_Bitmap(compressed_Image);


                    //  _Blur_Image.setVisibility(View.VISIBLE);

                    //  _Blur_Image.bringToFront();

//                    Utils._USER.set_Image_Path(source.toString());
//
//                    Utils.save_User_To_Shared_Prefs(this, Utils._USER);
//
//                    Utils.save_Bitmap(this, rotated_Bitmap);

                } catch (Exception e) {

                    Log.e("", "exception in base activity on activity result  : " + e.toString());

                }
            } else if (requestCode == 1001) {
                super.onActivityResult(requestCode, resultCode, result);

            } else if (requestCode == 64206) {
                Log.e("onActivityResult : ", "onActivityResult");
                super.onActivityResult(requestCode, resultCode, result);
                mSimpleFacebook.onActivityResult(requestCode, resultCode, result);

            }
        }
        Log.e("onActivityResult : ", "onActivityResult");
        super.onActivityResult(requestCode, resultCode, result);
        // mSimpleFacebook.onActivityResult(requestCode, resultCode, result);
    }

/*    public void SHOW_OK_DIALOG() {
        final MaterialDialog mMaterialDialog = new MaterialDialog(this);
        mMaterialDialog.setTitle("Premium");
        mMaterialDialog.setMessage("Premium Already purchased");
        mMaterialDialog.setPositiveButton(getResources().getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mMaterialDialog.dismiss();
            }
        });

        mMaterialDialog.show();


    }*/

/*    private void post(final String msg) {
        Log.d("", "facebook posting new message");
        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();


        Log.e("", "msg " + msg);

        Log.e("", "access tocken " + accessToken.getToken());
        Bundle postParams = new Bundle();
        postParams.putString("message", msg);

        GraphRequest request = new GraphRequest(accessToken, "me/feed", postParams, HttpMethod.POST, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                Log.e("", "completed " + graphResponse.toString());
            }
        });
        GraphRequestAsyncTask asynTaskGraphRequest = new GraphRequestAsyncTask(request);
        asynTaskGraphRequest.execute();

    }*/

  /*  private String FinalTextMessage() {
        String msg = "";

        String final_msg = "";
        String name = null;
        if (Utils._LATITUDE != 0.0 && Utils._LONGITUDE != 0.0) {
            name = ParseUser.getCurrentUser().getString("name");
            String custome_Message = "Hello,(" + name + ")";
            Custome_Message_And_Access_Token _Custome_Message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(App_Home_Page.this);

            if (_Custome_Message != null) {
                custome_Message = _Custome_Message.get_Custome_Message();
            }

            String defult = custome_Message + "is in trouble and needs your help at";

            final_msg = defult + ""
                    + "( http://maps.google.com/maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE + " ), And Location("
                    + " "
                    + getAddress(Utils._LATITUDE,
                    Utils._LONGITUDE)
                    + ")";
        } else {
            final_msg = msg
                    + "";
        }

        Log.e("", "final message is : " + final_msg);
        return final_msg;
    }*/

    public String getAddress(double lat, double lon) {
        String add = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(App_Home_Page.this, Locale.getDefault());

        try {

            addresses = geocoder.getFromLocation(lat, lon, 1);
            Address obj = addresses.get(0);
            add = obj.getAddressLine(0);


            //      GUIStatics.latitude = obj.getLatitude();
            //      GUIStatics.longitude = obj.getLongitude();
            //      GUIStatics.currentCity= obj.getSubAdminArea();
            //     GUIStatics.curren1tState= obj.getAdminArea();

            if (obj.getSubLocality() != null)
                add = add + "," + obj.getSubLocality();

            if (obj.getLocality() != null)
                add = add + "," + obj.getLocality();


            if (obj.getCountryName() != null)
                add = add + "," + obj.getCountryName();

            Log.e("IGA", "Address : " + add);


        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("", "exception in getAddress : " + e.toString());
            add = "Unknown Location";
        }
        return add;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        Utils.IS_APP_OPEN = true;
        AppEventsLogger.activateApp(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }
}
