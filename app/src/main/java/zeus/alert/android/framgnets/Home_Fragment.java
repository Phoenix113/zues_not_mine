package zeus.alert.android.framgnets;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import zeus.alert.android.R;
import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.activities.ZeusApplication;
import zeus.alert.android.models.Custome_Message_And_Access_Token;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.LogDisplay;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.volley.app_controller.MySingleton;
import zeus.alert.android.webservices.Webservices;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Home_Fragment extends Fragment {


    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    @InjectView(R.id.send_ALert_Iv)
    ImageView _Send_Alert;


    @InjectView(R.id.text)
    TextView text;



    private CountDownTimer countDownTimer;
    private final long startTime = 5 * 1000;
    private final long interval = 1 * 1000;
    String name = null;
    User _User;
    String msg = "";
    boolean timerHasStarted=false;
    private SimpleFacebook mSimpleFacebook;
    Custome_Message_And_Access_Token custome_message;


    public static Home_Fragment newInstance() {

        Home_Fragment f = new Home_Fragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view1 = inflater.inflate(R.layout.fragment_home, container, false);


        ButterKnife.inject(this, view1);


        //facebook hask key

        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.v("Sid:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }


        //get usre details
        _User=Utils.GET_USER_FROM_SHARED_PREFS(getActivity());
        //   _User.set_Safe(true); if user want to change when home page come and show alway alert
        //get token from fcm

        if (Utils.IS_FRISTTIMELOGIN){
            //Toast.makeText(getActivity(), "yes", Toast.LENGTH_SHORT).show();

            update_user_status(true);
            Utils.IS_FRISTTIMELOGIN=false;
        }
        String token = FirebaseInstanceId.getInstance().getToken();

        //register token

        if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {
            if(token!=null)
            {
                registerFCMToken(token);
            }


        }else {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.TOAST_NO_INTERNET_CONNECTION(getActivity(), "" + getResources().getString(R.string.no_internet_connection));
                        }
                    });
                }
            }).start();
        }


        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseListSample.mMenuDrawer.toggleMenu();
            }
        });



        //check user is safe or not
        if (_User.is_Safe()) {
            _Send_Alert.setBackgroundResource(R.drawable.alert_button);
        } else {
            _Send_Alert.setBackgroundResource(R.drawable.safe_button);
        }



        _Send_Alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {






                custome_message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());

                text.setText("Zeus is now Online");
                name = _User.get_User_Name();

                //   Log.d("msg",custome_message.get_Custome_Message());


                if (_User.is_Safe()) {


                    if(timerHasStarted){
                        cancel_Timer();
                    }else {

                        timerHasStarted = true;

                        countDownTimer = new MyCountDownTimer(startTime, interval);

                        countDownTimer.start();
                    }



                } else {

                    if (custome_message != null && !custome_message.get_Custome_Message().trim().equals("")) {
                        msg="";
                        msg= custome_message.get_Custome_Message();
                        Log.d("msg=c",msg);

                    }else {
                        msg="";
                        msg= safe_Text_Message();
                        Log.d("msg=s",msg);
                    }
                    Log.d("msg=",msg);
                    finalsend();
                    //fb();

                }



















            }
        });






        return view1;
    }




    @Override
    public void onResume() {
        super.onResume();


        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());


    }


    public  void fb() throws UnsupportedEncodingException {


        if (custome_message != null) {

            if (custome_message.is_Fb_Checked()) {


                if (mSimpleFacebook == null) {
                    mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
                }

                if (mSimpleFacebook.isLogin()) {

                    Log.e("user", "user is login");



                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "posting on facebook.");

                    post_On_Facebook();

                    /*Bundle parameters = new Bundle();
                    parameters.putString("message", "this is a test");// the message to post to the wall
                    parameters.putString("caption", "zeus caphon");// the message to post to the wall
                    parameters.putString("picture", "http://www.zeusapp.me/images/zeusapp.png");// the message to post to the wall
                    parameters.putString("name", "Zeus Title");// the message to post to the wall
                    parameters.putString("link", textForFb(msg));

                    */

                } else {
                    Log.e("user", "user is not login");
                    open_login();
                }

            } else {

                Utils.TOAST_ERROR_RESPONSE(getActivity(), " not allowed the app to post your status on Facebook.");

            }
        } else {

            Utils.TOAST_ERROR_RESPONSE(getActivity(), " You also have not allowed the app to post your status on Facebook.");

        }

    }
    // count 5 secound and then send message or notifaction
    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }

        @Override
        public void onFinish() {

            text.setText("Zeus is now Online");

            timerHasStarted=false;

            if (custome_message != null && !custome_message.get_Custome_Message().trim().equals("")) {
                msg="";
                msg= custome_message.get_Custome_Message() + " "+FinalTextMessage_u();
                Log.d("msg=c",msg);

            }else {
                msg="";
                msg= FinalTextMessage();
                Log.d("msg=f",msg);
            }
            Log.d("msg",msg);
            finalsend();
            try {
                fb();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.e("unsuported"," un suported error");
            }

        }

        @Override
        public void onTick(long millisUntilFinished) {

            text.setText("Alerting in " + millisUntilFinished / 1000);

        }

    }
    public void cancel_Timer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            timerHasStarted = false;
            text.setText("Zeus is now Online");
        }
    }

    private void finalsend() {


        if (_User.is_Safe()) {
            update_user_status(false);
        } else {
            update_user_status(true);
        }


        if(_User.get_User_Type().equals("1"))//premium user
        {
            nearbyuser(msg);
            sendMessage(msg);

            //   Toast.makeText(getActivity(), " You are Premim user so send Notification & message "+_User.get_User_Type(), Toast.LENGTH_SHORT).show();


        }else {

            sendMessage(msg);
            // sendMessagetoaddedContacts();
            //  Toast.makeText(getActivity(), " you are Nomal user so only message send "+_User.get_User_Type(), Toast.LENGTH_SHORT).show();

        }

        //_Send_Alert.setEnabled(true);

    }

    // update user status
    public void update_user_status(final boolean is_safe) {

        //upate user stataus now store using service

        Log.d("issafe",""+is_safe);




        if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {


            Utils.show_Dialog(getActivity(),"Updating User Status");
            String Update_User_Url = Webservices.UPDATE_USER_STATUS(getActivity());
            //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_User_Url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {





                                    JSONObject jsonObject= response.getJSONObject("user");

                                    _User.set_User_Id(jsonObject.getString("uid"));
                                    _User.set_User_Name(jsonObject.getString("username"));
                                    _User.set_Email(jsonObject.getString("email"));
                                    _User.set_Phone(jsonObject.getString("phone_number"));

                                    if(jsonObject.getString("is_safe").equals("1"))
                                    {
                                        _Send_Alert.setBackgroundResource(R.drawable.alert_button);
                                        _User.set_Safe(true);
                                    }else {
                                        _Send_Alert.setBackgroundResource(R.drawable.safe_button);
                                        _User.set_Safe(false);
                                    }
                                    if (jsonObject.getString("is_login").equals("1")){
                                        _User.set_Login(true);
                                    }else {
                                        _User.set_Login(false);
                                    }

                                    _User.set_Lat(jsonObject.getString("lat"));
                                    _User.set_Lng(jsonObject.getString("lon"));
                                    _User.set_User_Type(jsonObject.getString("usertype_id"));
                                    _User.set_Height(jsonObject.getString("height"));
                                    _User.set_Weight(jsonObject.getString("weight"));

                                    _User.set_Img_Path(jsonObject.getString("img_path"));



                                    Utils.SAVE_USER_TO_SHAREDPREFS(getActivity(), _User);





                                } else {
                                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                                }
                                Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Utils.hide_dialog();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            // Utils.hide_dialog();

                            //Showing toast
                            //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();
                    params.put("userid",_User.get_User_Id());
                    Log.d("issafe",""+is_safe);
                    if(is_safe)
                    {
                        params.put("is_safe", "1");
                    }else {
                        params.put("is_safe", "0");
                    }




                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);



        } else {
            Utils.TOAST_ERROR_RESPONSE(getActivity(), "No Internet Connection.");
        }




        Utils.hide_dialog();





    }



    //mesage send service
    private void sendMessage(final String msg) {
        //Log.d("msg",msg);

        LogDisplay.displayLog(getContext(),"message "+msg);


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.MESSAGESEND(getActivity());
        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                LogDisplay.displayLog(getContext(),"Message Send Successfully ");
                                Utils.TOAST_SUCCESS_RESPONSE(getActivity(),"Message Send Successfully");
                            }
                        } catch (JSONException e) {
                            Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError){

                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "No  Internet Connection." );

                }else if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Oops. Connection Timeout!" );
                    }
                }else{
                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "Something Went Wrong Please Try Again" );
                }
               // Console.LogMSG(Home_Fragment.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("userid",_User.get_User_Id());
                params.put("msg",msg);
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);

    }



    //send notifation near by user
    private void nearbyuser(final String msg) {
        Log.d("msg",msg);



        String url = Webservices.NEARBYUSER(getActivity());
        //   String url="http://kaprat.com/dev/zeusapp/API/nearbyuser.php";

        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                Utils.TOAST_SUCCESS_RESPONSE(getActivity(),"Notification Send Near By User");
                            }
                        } catch (JSONException e) {
                            Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Near By User message send please try again!");
                            e.printStackTrace();
                        }
                        // Console.LogMSG(HomeScreen.this, response.toString());
                        //call near by user api

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof NoConnectionError){

                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "No  Internet Connection." );

                }else if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Oops. Connection Timeout!" );
                    }
                }else{
                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "Something Went Wrong Please Try Again" );
                }



                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("lat",_User.get_Lat());
                params.put("lon",_User.get_Lng());
                params.put("userid",_User.get_User_Id());
                params.put("msg",msg);



                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);








    }


    void registerFCMToken(final String token) {
        Log.d("tokenid",token);
        StringRequest fcmRegister = new StringRequest(Request.Method.POST, Webservices.UPDATE_USER_TOKENID(getActivity()),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(!jsonObject.getBoolean("status"))
                            {
                                Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in TokenRegister please try again");
                            }
                        } catch (JSONException e) {
                            Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in TokenRegister please try again");
                            e.printStackTrace();
                        }
                        // Console.LogMSG(HomeScreen.this, response.toString());
                        //call near by user api

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in TokenRegister please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("userid", _User.get_User_Id());
                params.put("tokenid", token);
                params.put("deviceType","Android");
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(fcmRegister);
    }




    private String safe_Text_Message() {
        String msg = "";

        String final_msg = "Hi, " + name + " has tapped the safe button. " + name + " is now safe and the alert has been cancelled.";

        Log.e("zeus", "final message is : " + final_msg);
        return final_msg;

    }


    private String FinalTextMessage() {
        String msg = "";

        String final_msg = "";

        if (Utils._LATITUDE != 0.0 && Utils._LONGITUDE != 0.0) {

            String custome_Message = "Hello,(" + name + ")";
          /*  Custome_Message_And_Access_Token _Custome_Message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());

            if (_Custome_Message != null) {
                if (!_Custome_Message.get_Custome_Message().equals("")) {
                    custome_Message = _Custome_Message.get_Custome_Message();
                }

            }*/

            String defult = custome_Message + "is in trouble and needs your help at";


            final_msg = defult + ""
                    + "( http://maps.google.com/maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE + " ), And Location("
                    + " "
                    + getAddress(Utils._LATITUDE,
                    Utils._LONGITUDE)
                    + ")";

        } else {

            String custome_Message = "Hello,(" + name + ")";

            Utils.GET_LAST_KNOWN_LOCATION(getActivity());

            String defult = custome_Message + "is in trouble and needs your help at";


            final_msg = defult + ""
                    + "( http://maps.google.com/maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE + " ), And Location("
                    + " "
                    + getAddress(Utils._LATITUDE,
                    Utils._LONGITUDE)
                    + ")";

        }

        Log.e("zeus", "final message is : " + final_msg);
        return final_msg;
    }

    private String FinalTextMessage_u() {
        String msg = "";

        String final_msg = "";

        if (Utils._LATITUDE != 0.0 && Utils._LONGITUDE != 0.0) {

            String custome_Message = "." + name + " ";
          /*  Custome_Message_And_Access_Token _Custome_Message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());

            if (_Custome_Message != null) {
                if (!_Custome_Message.get_Custome_Message().equals("")) {
                    custome_Message = _Custome_Message.get_Custome_Message();
                }

            }*/

            String defult = custome_Message + "is in trouble and needs your help at";


            final_msg = defult + ""
                    + "( http://maps.google.com/maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE + " ), And Location("
                    + " "
                    + getAddress(Utils._LATITUDE,
                    Utils._LONGITUDE)
                    + ")";

        } else {

            String custome_Message = "." + name + " ";

            Utils.GET_LAST_KNOWN_LOCATION(getActivity());

            String defult = custome_Message + "is in trouble and needs your help at";


            final_msg = defult + ""
                    + "( http://maps.google.com/maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE + " ), And Location("
                    + " "
                    + getAddress(Utils._LATITUDE,
                    Utils._LONGITUDE)
                    + ")";

        }

        Log.e("zeus", "final message is : " + final_msg);
        return final_msg;
    }


    public String getAddress(double lat, double lon) {
        String add = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {


            addresses = geocoder.getFromLocation(lat, lon, 1);

            if (addresses!=null){

                Address obj = addresses.get(0);

                add = obj.getAddressLine(0);


                //      GUIStatics.latitude = obj.getLatitude();
                //      GUIStatics.longitude = obj.getLongitude();
                //      GUIStatics.currentCity= obj.getSubAdminArea();
                //     GUIStatics.curren1tState= obj.getAdminArea();

                if (obj.getSubLocality() != null)
                    add = add + "," + obj.getSubLocality();

                if (obj.getLocality() != null)
                    add = add + "," + obj.getLocality();


                if (obj.getCountryName() != null)
                    add = add + "," + obj.getCountryName();

                Log.e("IGA", "Address : " + add);

            }



        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("", "exception in getAddress : " + e.toString());
            add = "Unknown Location";
        }
        return add;
    }



    //get list of contect

    /*private void get_Contact_List_From_parse() {




        String register_User_Url = Webservices.GET_USER_CONTACTS(getActivity(), _User.get_User_Id());


        Utils.show_Dialog(getActivity(), "Fetching Contacts");
        Log.e("jeeeni", "login url : " + register_User_Url);

        lstcontacts.clear();

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                    if (_Status) {


                        JSONArray _contacts = response.getJSONArray("0");

                        Contact _Contact = null;

                        for (int i = 0; i < _contacts.length(); i++) {
                            JSONObject SingleContact = _contacts.getJSONObject(i);
                            _Contact = new Contact();


                            _Contact.set_Contact_Id(SingleContact.getString("id"));
                            _Contact.set_Contact_Name(SingleContact.getString("contact_name"));
                            _Contact.set_Contact_Parent_Id(SingleContact.getString("unique_id"));
                            _Contact.set_Phone_Number(SingleContact.getString("user_phone_number"));

                            lstcontacts.add(_Contact);


                        }




                    } else {

                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    Utils.hide_dialog();
                    Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Utils.hide_dialog();
                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);



    }*/
    //message

    /*private void sendMessagetoaddedContacts() {

        _Number_To_Which_Message_Is_Sent.clear();
        for (Contact product : lstcontacts) {
            getnumberofSentSMS++;

            Log.e("", "phone number is : " + getnumberofSentSMS + " " + product.get_Phone_Number() + " : name = " + product.get_Contact_Name());

            if (FinalTextMessage().equals("")) {
                Utils.TOAST_ERROR_RESPONSE(getActivity(), "Not Able To Get Latitude And Longitude.");
            } else {

                if (_Number_To_Which_Message_Is_Sent.size() > 0) {

                    for (int i = 0; i < _Number_To_Which_Message_Is_Sent.size(); i++) {

                        if (!_Number_To_Which_Message_Is_Sent.get(i).equals(product.get_Phone_Number())) {
                            _Number_To_Which_Message_Is_Sent.add(product.get_Phone_Number());
                            Log.e("Zeus ", "phone :" + product.get_Phone_Number());

                            if (!product.get_Phone_Number().equals(_User.get_Phone())) {

                                Log.e("zeus", "user alert status : " + _Send_Alert_To_Contacts);



                                if (_Send_Alert_To_Contacts) {

                                    sendSMS(product.get_Phone_Number(), safe_Text_Message(), product.get_Contact_Name());
                                } else {
                                    sendSMS(product.get_Phone_Number(), FinalTextMessage(), product.get_Contact_Name());
                                }



                            }

                        } else {
                            break;
                        }

                    }

                } else {
                    Log.e("Zeus ", "phone :" + product.get_Phone_Number());
                    if (!product.get_Phone_Number().equals(_User.get_Phone())) {
                        if (_Send_Alert_To_Contacts) {
                            sendSMS(product.get_Phone_Number(), safe_Text_Message(),product.get_Contact_Name());
                        } else {
                            sendSMS(product.get_Phone_Number(), FinalTextMessage(), product.get_Contact_Name());
                        }
                        _Number_To_Which_Message_Is_Sent.add(product.get_Phone_Number());
                    }

                }


            }


        }
    }


    private void sendSMS(String phoneNumber, String message, final String name) {
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();
        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
                new Intent(getActivity(), SmsSentReceiver.class), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0,
                new Intent(getActivity(), SmsDeliveredReceiver.class), 0);
        try {
            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> mSMSMessage = sms.divideMessage(message);
            for (int i = 0; i < mSMSMessage.size(); i++) {
                sentPendingIntents.add(i, sentPI);
                deliveredPendingIntents.add(i, deliveredPI);
            }
            sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage,
                    sentPendingIntents, deliveredPendingIntents);

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Utils.TOAST_SUCCESS_RESPONSE(getActivity(), "Message Sent To " + name);
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

            Utils.TOAST_ERROR_RESPONSE(getActivity(), "SMS sending failed...");
        }

    }*/

  /*  private void get_users_those_have_same_city_and_are_In_Danger() {

        if (!Utils.GET_USER_CITY_NAME(getActivity()).equals("")) {

            contactsQuery_Current_Location = ParseUser.getQuery();
            contactsQuery_Current_Location.whereEqualTo("current_location", Utils.GET_USER_CITY_NAME(getActivity()));
            contactsQuery_Current_Location.whereEqualTo("is_safe", true);
            contactsQuery_Current_Location.whereEqualTo("is_login", true);
            Utils.show_Dialog(getActivity(), "Fetching Contacts");

            Log.e("zeus", "calling 6 mile radius users");

            contactsQuery_Current_Location.findInBackground(new FindCallback<ParseObject>() {

                @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {
                    if (e == null) {


                        if (parseObjects != null) {
                            Log.e("zeus", "size of peoples in 6 mile radius : " + parseObjects.size());
                            if (parseObjects.size() > 0) {


                                for (int i = 0; i < parseObjects.size(); i++) {

                                    Float user_Lat = Float.parseFloat(parseObjects.get(i).getString("lat"));
                                    Float user_Lng = Float.parseFloat(parseObjects.get(i).getString("lng"));
                                    int total_Users = parseObjects.size();
                                    if (!_Parse_User.getString("username").equals(parseObjects.get(i).getString("username"))) {
                                        Log.e("zeus", "email is : " + parseObjects.get(i).getString("username"));

                                        find_Total_Distance_Between_Start_And_End_Location("" + Utils._LATITUDE,
                                                "" + Utils._LONGITUDE, "" + user_Lat, "" + user_Lng, i, total_Users, parseObjects.get(i));
                                    }

                                    if (i == parseObjects.size() - 1) {

                                        Log.e("zeus", "size of peoples in 6 mile radius : " + lstcontacts.size());
                                        if (Utils.DETECT_INTERNET_CONNECTION((Activity) Utils._Context)) {
                                            if (getActivity() != null) {
                                                Utils.hide_dialog();
                                            }
                                            Log.e("Zeus", "now calling parse contacts");
                                            get_Contact_List_From_parse();
                                        } else {
                                        }
                                    }


                                }


                            } else {
                                Log.e("Zeus", "No one in 6 mile radius sieze 0");

                                if (Utils.DETECT_INTERNET_CONNECTION((Activity) Utils._Context)) {

                                    if (getActivity() != null) {
                                        Utils.hide_dialog();
                                    }
                                    Log.e("Zeus", "now calling parse contacts");
                                    get_Contact_List_From_parse();
                                } else {

                                }
                            }


                        } else {
                            Log.e("array null", "arary is null");
                        }


                    } else

                    {
                        Utils.TOAST_NO_INTERNET_CONNECTION(getActivity(), "" + getResources().getString(R.string.went_wrong));
                    }

                    Utils.hide_dialog();
                }
            });
        } else {

            Log.e("zeus", "not able to get the city name of user");
            Utils.TOAST_ERROR_RESPONSE(getActivity(), "Gps is not working properly. Turn it on and come back");


            Log.e("zeus", "Lat : " + Utils._LATITUDE + " lng : " + Utils._LONGITUDE);
            Utils.GET_LAST_KNOWN_LOCATION(getActivity());

            if (Utils.GET_USER_CITY_NAME(getActivity()).equals("")) {
                get_users_those_have_same_city_and_are_In_Danger();
            }

        }
    }*/

    /*  private void find_Total_Distance_Between_Start_And_End_Location(String lat_a, String lng_a, String lat_b, String lng_b, final int current_user, final int total_users, final ParseObject parseObject) {
          String url = "http://maps.google.com/maps/api/directions/json?origin=" + lat_a + "," +
                  lng_a + "&destination=" + lat_b + "," +
                  lng_b + "&sensor=false&units=metric";
          url = url.replace(" ", "%20");
          Log.e("ZEUS", "diatance url : " + url);
          JsonObjectRequest _CALCUALTE_DISTANCE = new JsonObjectRequest(Request.Method.GET,
                  url, null, new Response.Listener<JSONObject>() {

              @Override
              public void onResponse(JSONObject response) {

                  try {


                      if (!response.isNull("status")) {
                          if (response.getString("status").equals("OK")) {


                              JSONArray _Routes = response.getJSONArray("routes");

                              for (int i = 0; i < _Routes.length(); i++) {
                                  JSONObject _Route = _Routes.getJSONObject(i);


                                  JSONArray _Legs = _Route.getJSONArray("legs");

                                  for (int j = 0; j < _Legs.length(); j++) {

                                      JSONObject _Diatance = _Legs.getJSONObject(i);

                                      JSONObject _Distance_Object = _Diatance.getJSONObject("distance");
                                      String _Total_Distance = _Distance_Object.getString("text");

                                      Log.e("zeus", "6 mile radius contact distance : " + _Total_Distance);
                                      boolean is_In_Km = false;
                                      if (_Total_Distance.contains("km")) {
                                          is_In_Km = true;
                                          _Total_Distance = _Total_Distance.replace("km", "").replace(" ", "");

                                      } else if (_Total_Distance.contains("m")) {

                                          _Total_Distance = _Total_Distance.replace("m", "").replace(" ", "");

                                      }


                                      Float diatance = Float.parseFloat(_Total_Distance);


                                      if (is_In_Km) {
                                          diatance = diatance * 1000;

                                      }

                                      //  Log.e("zeus", "6 mile radius contact emails : " + parseObjects.get(i).getString("username"));
                                      //   Log.e("zeus", "6 mile radius contact distance : " + diatance);

                                      if (diatance < 804.672f) {

                                          //lstcontacts.add(parseObject);
                                          Log.e("zeus", "total distance is : " + _Total_Distance);
                                      }

                                  }


                              }


                          } else {
                              Utils.TOAST_ERROR_RESPONSE(getActivity(), "Error Please Try Again");
                          }
                      }

                      Log.e("zeus", "current user number : " + current_user);
                      Log.e("zeus", "total user number : " + total_users);


                      if (current_user == total_users) {

                      }


                  } catch (Exception e) {
                      e.printStackTrace();
                  }

              }

          }, new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {


              }
          }) {

              @Override
              protected Map<String, String> getParams() {
                  Map<String, String> params = new HashMap<String, String>();
                  return params;
              }

          };

          // Adding request to request queue
          ZeusApplication.getInstance().addToRequestQueue(_CALCUALTE_DISTANCE,
                  ZeusApplication.TAG);


      }
  */
    @Override
    public void onPause() {
        super.onPause();

       /* if (contactsQuery_Current_Location != null) {
            contactsQuery_Current_Location.cancel();
        }

        if (contactsQuery != null) {
            contactsQuery.cancel();
        }*/

    }


    public void post_On_Facebook() throws UnsupportedEncodingException {
/*
        Feed feed = new Feed.Builder()
                .setName("Zeus Alert, I'm In Trouble!")
                .setCaption("Click to see his location")
                *//*.setDescription("This person is in trouble and needs your immediate help!")*//*
                .setDescription("This person is in trouble and needs your immediate help!")
                .setMessage("")
                .setPicture("http://www.zeusapp.me/images/zeusapp.png")
                *//*.setPicture("http://www.zeusapp.me/images/zeusapp.png")*//*
                .setLink(textForFb(msg))
               *//* .setLink("" + textForFb())*//*

                //.setPrivacy(Priva)
                .build();*/

        Feed feed = new Feed.Builder()
                .setMessage("This person is in trouble and need your immediat help")
                .setName("Zeus Alert, I'm In Trouble!")
                .setCaption("Zeus Alert, I'm In Trouble!")
                .setDescription("This person is in trouble and need your immediat help")
                .setPicture("http://zeusalert.com/zeusic.jpeg")
                .setLink(textForFb(msg))
                .build();

        Log.e("updating", "updating : ");

        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Posting Status On Facebook");

/*

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/feed",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            */
/* handle the result *//*

                    }
                }
        ).executeAsync();


        Bundle params = new Bundle();
        params.putString("message", "welcome to fbsdg");



*/
/* make the API call *//*

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/feed",
                params,
                HttpMethod.POST,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            */
/* handle the result *//*

                        Log.e("test post resaponce",""+response.toString());
                        Log.e("test post resaponce",""+response.toString());
                    }


                }
        ).executeAsync();

*/



        mSimpleFacebook.publish(feed,  new OnPublishListener() {

            public void onException(Throwable throwable) {

                Log.e("ecxception", "throwable : " + throwable.toString());
                //      hide_Dialog();
                //  Utils.TOAST_ERROR_RESPONSE(getActivity(), "onException : " + throwable.toString());
            }

            @Override
            public void onFail(String reason) {
                //      hide_Dialog();
                Log.e("ecxception", "fail reason : " + reason.toString());
                //  Utils.TOAST_ERROR_RESPONSE(getActivity(), "onFail : " + reason.toString());

            }

            @Override
            public void onThinking() {
                //     show_Dialog("Posting On Facebook.", getActivity());

            }


            @Override
            public void onComplete(String response) {
                super.onComplete(response);

                Log.e("ecxception", "success : " + response.toString());
                Utils.TOAST_ERROR_RESPONSE(getActivity(), "Status posted on Facebook");


                Tracker t = ((ZeusApplication) getActivity().getApplication()).getTracker(ZeusApplication.TrackerName.APP_TRACKER);
                t.setScreenName("Facebook Status Published");
                t.send(new HitBuilders.AppViewBuilder().build());
            }
        });


        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Posting Status On Facebook = grapg code");
        Bundle params = new Bundle();
        //params.putString("message", textForFb(msg));
        params.putString("link", textForFb(msg));
/* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/feed",
                params,
                HttpMethod.POST,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Posting Status On Facebook = completed = grp");
                    }
                }
        ).executeAsync();




       /* SimpleFacebook.getInstance().publish(feed, new OnPublishListener() {
            @Override
            public void onException(Throwable throwable) {

                Log.e("ecxception", "throwable : " + throwable.toString());
                //      hide_Dialog();
                //  Utils.TOAST_ERROR_RESPONSE(getActivity(), "onException : " + throwable.toString());
            }

            @Override
            public void onFail(String reason) {
                //      hide_Dialog();
                Log.e("ecxception", "fail reason : " + reason.toString());
               //  Utils.TOAST_ERROR_RESPONSE(getActivity(), "onFail : " + reason.toString());

            }

            @Override
            public void onThinking() {
                //     show_Dialog("Posting On Facebook.", getActivity());

            }

            @Override
            public void onComplete(String response) {
                //       hide_Dialog();
                Log.e("ecxception", "success : " + response.toString());
                Utils.TOAST_ERROR_RESPONSE(getActivity(), "Status posted on Facebook");


                Tracker t = ((ZeusApplication) getActivity().getApplication()).getTracker(ZeusApplication.TrackerName.APP_TRACKER);
                t.setScreenName("Facebook Status Published");
                t.send(new HitBuilders.AppViewBuilder().build());


            }
        });*/
    }

    public void open_login() {
        final OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onFail(String reason) {

                Log.w("failed", "Failed to login");
            }

            @Override
            public void onException(Throwable throwable) {

                Log.e("exception", "Bad thing happened", throwable);
            }

            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // change the state of the button or do whatever you want


                try {
                    post_On_Facebook();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e(" invalid url"," invalid encoding");
                }
            }

            @Override
            public void onCancel() {
                Log.w("onCancel", "onCancel");
            }

        };

        mSimpleFacebook.login(onLoginListener);

    }

    private String textForFb(String m) {
        String msg = "";

        String final_msg = "";

        if (Utils._LATITUDE != 0.0 && Utils._LONGITUDE != 0.0) {


            String final_msg_u ="maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE;
            final_msg ="https://zeusalert.com/zeus23.php?contenttitle=";
            String toenc =m+"&contenturl="+final_msg_u+"&content=ZeusALert&lat="+Utils._LATITUDE +"&lng="+Utils._LONGITUDE;
//            final_msg ="zeusalert.com/zeus20.php?contenttitle="+m+"&contenturl="+final_msg_u+"&content=Zeus ALert";

            try {
                final_msg+= URLEncoder.encode(toenc, "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
                final_msg+=toenc;
            }


        } else {
            String final_msg_u ="maps?q=" + Utils._LATITUDE + "," + Utils._LONGITUDE;
            final_msg ="https://zeusalert.com/zeus23.php?contenttitle=";
            String toenc =m+"&contenturl="+final_msg_u+"&content=ZeusALert&lat="+Utils._LATITUDE +"&lng="+Utils._LONGITUDE;
//            final_msg ="zeusalert.com/zeus20.php?contenttitle="+m+"&contenturl="+final_msg_u+"&content=Zeus ALert";

            try {
                final_msg+= URLEncoder.encode(toenc, "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
                final_msg+=toenc;
            }

        }

        Log.e("textForFb", "final message is : " + final_msg);
        return final_msg;
    }



    @Override
    public void onStop() {
        super.onStop();
      /*  if (contactsQuery_Current_Location != null) {
            contactsQuery_Current_Location.cancel();
        }
        if (contactsQuery != null) {
            contactsQuery.cancel();
        }
*/

    }



}
