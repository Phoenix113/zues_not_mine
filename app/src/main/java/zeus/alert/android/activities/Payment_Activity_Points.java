package zeus.alert.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import zeus.alert.android.R;
import zeus.alert.android.utils.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by Moubeen on 1/11/16.
 */

public class Payment_Activity_Points extends FragmentActivity {


    @InjectView(R.id.get_it_now)
    Button _Get_It_Now;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_payment_points);

        ButterKnife.inject(this);
        Utils.IS_PAYMENT_ACTIVITY_OPEN=true;

        Log.e("zeus","paymeny points");
        _Get_It_Now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.DETECT_INTERNET_CONNECTION(Payment_Activity_Points.this)) {
                    Intent intent = new Intent(Payment_Activity_Points.this, PaymentActivity.class);
                    startActivity(intent);
                }
                else{
                    Utils.TOAST_ERROR_RESPONSE(Payment_Activity_Points.this,"No Internet Connection.");
                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.IS_PAYMENT_ACTIVITY_OPEN=true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.IS_PAYMENT_ACTIVITY_OPEN=false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.IS_PAYMENT_ACTIVITY_OPEN=false;
    }
}
