package zeus.alert.android.models;

/**
 * Created by Moubeen Warar on 11/19/2015.
 */
public class Custome_Message_And_Access_Token {

    private String _Custome_Message = "";
    private String _Access_Tocked = "";
    private boolean _Fb_Checked = false;


    public boolean is_Fb_Checked() {
        return _Fb_Checked;
    }

    public void set_Fb_Checked(boolean _Fb_Checked) {
        this._Fb_Checked = _Fb_Checked;
    }

    public String get_Custome_Message() {
        return _Custome_Message;
    }

    public void set_Custome_Message(String _Custome_Message) {
        this._Custome_Message = _Custome_Message;
    }


    public String get_Access_Tocked() {
        return _Access_Tocked;
    }

    public void set_Access_Tocked(String _Access_Tocked) {
        this._Access_Tocked = _Access_Tocked;
    }
}
