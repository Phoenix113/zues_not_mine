package zeus.alert.android.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;
import zeus.alert.android.webservices.Webservices;
import zeus.alert.android.business_objext.UserFunctions;
import zeus.alert.android.utils.Utils;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author Nouman
 */
public class ForgotActivity extends Activity {
    EditText et_forgotemail = null;
    Button btn_recover = null;
    JSONObject jason;
    private String email;
    String id,email_id,forgot_otp,number,uniq_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.forgot_layout);
        ActionBar actionBar = getActionBar();

        et_forgotemail = (EditText) findViewById(R.id.et_forgot_email);
        btn_recover = (Button) findViewById(R.id.btn_forgot_recover);
        btn_recover.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (et_forgotemail.getText().length() > 0) {

                    if (Utils.IS_EMAIL_VALID(et_forgotemail.getText().toString().trim())) {
                        Utils.HIDE_KEYBOARD(et_forgotemail,ForgotActivity.this);
                        email = et_forgotemail.getText().toString().trim();

                        forgetPasswordApi(email);

                    } else {
                        Utils.TOAST_ERROR_RESPONSE(ForgotActivity.this, "Enter valid email address");
                    }
                }
            }
        });
    }

    private void forgetPasswordApi(String email) {
        String ForgetPassword_URl = Webservices.ForgetSentOtp(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ForgetPassword_URl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        //    Utils.hide_dialog();
                        //Showing toast message of the response
                        Log.d("rathod",""+s);
                        try {
                            JSONObject response = new JSONObject(s);
                            boolean _Status = response.getBoolean("status");
                            if (_Status) {



                                JSONObject jsonObject= response.getJSONObject("user");

                                id = jsonObject.getString("id");
                                uniq_id = jsonObject.getString("unique_id");
                                email_id = jsonObject.getString("email");
                                forgot_otp = jsonObject.getString("forgot_otp");
                                number = jsonObject.getString("phone_number");


                                Intent intent =new Intent(ForgotActivity.this,PasswordOtp.class);
                                intent.putExtra("unique_id",uniq_id);
                                intent.putExtra("forgot_otp",forgot_otp);
                                startActivity(intent);


                            } else {
                                Utils.TOAST_ERROR_RESPONSE(ForgotActivity.this, "" + response.getString("error_msg"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.TOAST_ERROR_RESPONSE(ForgotActivity.this, "Something wrong");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Showing toast
                        //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<String, String>();
                params.put("email", ForgotActivity.this.email);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class RecoverPasswordSync extends AsyncTask<String, Void, Void> {
        // flag for Internet connection status
        Context c;
        //	ProgressDialogPopup dialog;
        Boolean tag = false;
        String error_msg = "";

        public RecoverPasswordSync(Context con) {
            this.c = con;
//			dialog = new ProgressDialogPopup(c);
//			dialog.setCancelable(false);
//			dialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //	dialog.show();
        }

        @Override
        protected Void doInBackground(String... arg0) {
            UserFunctions functions = new UserFunctions();
            try {
                // if (isOnline()) {
                jason = functions.ForgetPassowrd(arg0[0]);
                // check for SignUP response
                String res = jason.getString("success");
                if (Integer.parseInt(res) == 1) {
                    tag = true;

                } else {
                    tag = false;
                    res = jason.getString("error");
                    if (Integer.parseInt(res) == 1) {
                        error_msg = jason.getString("error_msg");
                        // Error while signing up
                    }
                }
                // } else {
                // tag = false;
                // error_msg = "No Internet Access";
                // }
            } catch (Exception e) {
                //Log.e("DO IN BACKGROUD Exception:", e.getMessage());
                error_msg = e.getMessage();
//				if (dialog.isShowing())
//					dialog.dismiss();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//			if (dialog.isShowing())
//				dialog.dismiss();
            if (!tag) {
                Toast.makeText(c, "Error:" + error_msg, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(
                        c,
                        "Recovry Email sent to your email account Please check.",
                        Toast.LENGTH_SHORT).show();
                Tracker t = ((ZeusApplication) getApplication())
                        .getTracker(ZeusApplication.TrackerName.APP_TRACKER);
                t.setScreenName("Recovry Email Sent.");
                t.send(new HitBuilders.AppViewBuilder().build());
                finish();
            }

        }

        public void showProgressDialog(Context context, String title,
                                       String message) {
            // Setting Dialog Title
//			dialog.setTitle(title);
//			// Setting Dialog Message
//			dialog.setMessage(message);
//			dialog.setCancelable(false);
//			// Showing Alert Message
//			dialog.show();
        }

    }

}
