package zeus.alert.android.custom_views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class MyEditView extends EditText {
	public MyEditView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public MyEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public MyEditView(Context context) {
		super(context);
		init();
	}

	private void init() {
		if (!isInEditMode()) {
			Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
					"Lato_Light.ttf");
			setTypeface(tf);
			setTextColor(Color.BLACK);
			setHintTextColor(Color.GRAY);

		}
	}
}
