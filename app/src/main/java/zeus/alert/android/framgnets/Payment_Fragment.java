package zeus.alert.android.framgnets;



        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v7.app.AlertDialog;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;

        import com.android.volley.AuthFailureError;
        import com.android.volley.NoConnectionError;
        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.TimeoutError;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;
        import zeus.alert.android.R;
        import zeus.alert.android.activities.App_Home_Page;
        import zeus.alert.android.activities.BaseListSample;
        import zeus.alert.android.activities.PaymentActivity;
        import zeus.alert.android.activities.ZeusApplication;
        import zeus.alert.android.models.Coupon;
        import zeus.alert.android.models.User;
        import zeus.alert.android.models.UserPassword;
        import zeus.alert.android.utils.LogDisplay;
        import zeus.alert.android.utils.Utils;
        import zeus.alert.android.webservices.Webservices;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.UnsupportedEncodingException;
        import java.util.HashMap;
        import java.util.Hashtable;
        import java.util.Map;

        import butterknife.ButterKnife;
        import butterknife.InjectView;


public class Payment_Fragment extends Fragment {

    private User _User = null;
    private UserPassword _UserPassword = null;
    int user_type;
    String user_password;
    String user_password1;
    private static String currentplan = "";
    private static String currentoff = "";

    @InjectView(R.id.get_it_now)
    Button _Get_It_Now;

    Button btnCancelSubscription;

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;
    private String user_id;


    public static Payment_Fragment newInstance() {

        Payment_Fragment f = new Payment_Fragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_payment_points, container, false);

        ButterKnife.inject(this, view);

        btnCancelSubscription = (Button) view.findViewById(R.id.btn_cancel);


        _User = Utils.GET_USER_FROM_SHARED_PREFS(getActivity());
        try {
            _UserPassword = Utils.GET_PASSWORD_FROM_SHARED_PREFS(getActivity());
             user_password1 = _UserPassword.get_User_Password();
        }catch (Exception ex){
            Log.d("r@thod "," get passwrod :: "+ex.getMessage().toString());
        }

        user_type = Integer.parseInt(_User.get_User_Type());
        user_password =_User.get_Password();
        user_id = _User.get_User_Id();

        Log.d("RRR","user_type :: "+user_type);
        Log.d("RRR","user_type :: "+user_id);

        getCurrentSubscription();






            _Get_It_Now.setText("Get It Now");



        Log.e("zeus", "paymeny points");
        _Get_It_Now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                checkSubscription();

                /*if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {
                       Intent intent = new Intent(getActivity(), PaymentActivity.class);
                        startActivity(intent);

                } else {
                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "No Internet Connection.");
                }*/
            }
        });

        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseListSample.mMenuDrawer.toggleMenu();
            }
        });



      /*  btnCancelSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder  builder=new AlertDialog.Builder(getActivity());
                builder.setMessage("ENTER PASSWORD ");
                //builder.setTitle("password");
                final  EditText EdtUserPSW =new EditText(getContext());
                builder.setView(EdtUserPSW);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String PSW=EdtUserPSW.getText().toString().trim();
                        Log.d("RRR","User Password:: "+user_password1);

                        try{
                            if (user_password1.equals(PSW)){
                                Log.d("RRR","Password IS MATCH:: "+PSW);
                                cancelSubscription();
                            }else{
                                Log.d("RRR","Password DOES't MATCH:: "+PSW);
                                Utils.TOAST_WRONG_CREDENTIAL(getActivity(), "Password Mismatch");
                            }
                        }catch (Exception ex){

                            Log.e("RRR","paymnet fragmetn  password "+ex.getMessage().toString());

                        }

                    }
                });
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();
            }
        });
*/




        return view;
    }

    private void checkSubscription() {
        //http://kaprat.com/dev/zeusapp/API/CheckSubscription.php?userUniqueID=5886ea4681
        String url= Webservices.CheckSubscription(getActivity(),
                _User.get_User_Id());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("RRRR","Response CHECK SUBSCRIPTION :: "+ response.toString());


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean _status = jsonObject.getBoolean("status");
                    String _message = jsonObject.getString("error_msg");
                    if (_status){
                        if (_message.contains("You have already subscribed in zeus app.Your Subscription will expire on")){
                            Utils.TOAST_ERROR_RESPONSE(getActivity(), ""+_message);
                        }else if (_message.contains("Your Subscription is Expire")){
                                Intent intent = new Intent(getActivity(), PaymentActivity.class);
                                startActivity(intent);
                        }else {

                        }
                    }else {
                        if (_message.contains("No records found in our database")){
                            Intent intent = new Intent(getActivity(), PaymentActivity.class);
                            startActivity(intent);
                        }else {
                            Utils.TOAST_ERROR_RESPONSE(getActivity(), ""+_message);
                        }
                    }
                } catch (JSONException e) {
                    LogDisplay.displayLog(getActivity()," JSON EXCEPTION :: "+e.getMessage());
                    e.printStackTrace();
                }catch (Exception ex){
                    LogDisplay.displayLog(getActivity()," Exception :: "+ex.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (volleyError instanceof NoConnectionError){

                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "No  Internet Connection." );

                }else if (volleyError.networkResponse == null){
                    if (volleyError.getClass().equals(TimeoutError.class)){
                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Oops. Connection Timeout!" );
                    }
                }else{
                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "Something Went Wrong Please Try Again" );
                }
                Log.d("RRR","volleyError  "+volleyError);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> stringMap=new HashMap<String, String>();
                return stringMap;
            }
        };


        RequestQueue requestQueue=Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }
    private void api_cancelSubscription() {
        //http://kaprat.com/dev/zeusapp/API/CheckSubscription.php?userUniqueID=5886ea4681
        String url= Webservices.cancel_api_Subscription(getActivity(),
                _User.get_User_Id());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                btnCancelSubscription.setVisibility(View.GONE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (volleyError instanceof NoConnectionError){

                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "No  Internet Connection." );

                }else if (volleyError.networkResponse == null){
                    if (volleyError.getClass().equals(TimeoutError.class)){
                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "Oops. Connection Timeout!" );
                    }
                }else{
                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "Something Went Wrong Please Try Again" );
                }
                Log.d("RRR","volleyError  "+volleyError);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> stringMap=new HashMap<String, String>();
                return stringMap;
            }
        };


        RequestQueue requestQueue=Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }
    public void update_user_types() {




        if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {


            Utils.show_Dialog(getActivity(),"Updating User ..");
            String Update_User_Url = Webservices.UPDATE_USER_TYPES(getActivity());
            //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_User_Url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {
                                    btnCancelSubscription.setVisibility(View.GONE);

                                  User  user=new User();
                                    JSONObject jsonObject= response.getJSONObject("user");

                                    user.set_User_Id(jsonObject.getString("uid"));
                                    user.set_User_Name(jsonObject.getString("username"));
                                    user.set_Email(jsonObject.getString("email"));
                                    user.set_Phone(jsonObject.getString("phone_number"));

                                    if(jsonObject.getString("is_safe").equals("1"))
                                    {
                                        user.set_Safe(true);
                                    }else {

                                        user.set_Safe(false);
                                    }
                                    if (jsonObject.getString("is_login").equals("1")){
                                        user.set_Login(true);
                                    }else {
                                        user.set_Login(false);
                                    }

                                    user.set_Lat(jsonObject.getString("lat"));
                                    user.set_Lng(jsonObject.getString("lon"));
                                    user.set_User_Type(jsonObject.getString("usertype_id"));
                                    user.set_Height(jsonObject.getString("height"));
                                    user.set_Weight(jsonObject.getString("weight"));
                                    user.set_Img_Path(jsonObject.getString("img_path"));


                                    Utils.SAVE_USER_TO_SHAREDPREFS(getActivity(), user);
                                    _User=Utils.GET_USER_FROM_SHARED_PREFS(getActivity());



                                } else {
                                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                                }
                                Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Utils.hide_dialog();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            // Utils.hide_dialog();

                            //Showing toast
                            //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();
                    params.put("userid",_User.get_User_Id());
                    params.put("usertype_id", "0");

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);



        } else {
            Utils.TOAST_ERROR_RESPONSE(getActivity(), "No Internet Connection.");
        }




        Utils.hide_dialog();





    }
    private void cancelSubscription() {
        /*cancel subscription
        'http://kaprat.com/dev/zeusapp/API/CancelSubscription.php?useruniqueID=122'*/

        //String cnc_sub_url = "http://kaprat.com/dev/zeusapp/API/CancelSubscription.php?"+"useruniqueID="+_User.get_User_Id();
        String url= Webservices.CANCEL_SUBSCRIPTION(getActivity(),_User.get_User_Id());

        StringRequest stringRequest=new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.d("RRR ","CancelSubscription response :: "+ s);

                try {
                    JSONObject jsonObject=new JSONObject(s);
                    boolean _status=jsonObject.getBoolean("status");
                    String _msg = jsonObject.getString("msg");

                    if (_status ) {
                        //update_user_types();
                        Utils.TOAST_WRONG_CREDENTIAL(getActivity(), ""+_msg);
                    }
                    else{
                        Utils.TOAST_WRONG_CREDENTIAL(getActivity(), ""+_msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<String, String>();
                //params.put("useruniqueID",_User.get_User_Id());
                return params;
                //return super.getParams();
            }
        };

        RequestQueue requestQueue= Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    private void getCurrentSubscription() {


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.GETPLAN(getActivity());
        StringRequest Subscription = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {

                            JSONObject res=new JSONObject(response);


                            //  JSONObject subscribed_plans= res.getJSONObject("subscribed_plans"); //new JSONObject(res.optString("plans"));
                            JSONArray data = res.getJSONArray("subscribed_plans");

                            if(data.length() != 0){
                                btnCancelSubscription.setVisibility(View.VISIBLE);
                                btnCancelSubscription.setText("Cancel Subscription");
                                btnCancelSubscription.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        api_cancelSubscription();
                                    }
                                });


                            }
                            else{
                                btnCancelSubscription.setVisibility(View.GONE);
                            }
                                //currentplan = data.optJSONObject(0).optString("plan_title","")+" Expires in "+ data.optJSONObject(0).optString("validity_text","") ;
                            if(data.length() != 0){}
                                                    //     _currentplan.setText(currentplan);

                            JSONArray dataof = res.optJSONArray("offer_subscriptions");

                            if(dataof.length() != 0)
                                currentoff = dataof.optJSONObject(0).optString("offer_title","")+" For "+ dataof.optJSONObject(0).optString("validity_text","") ;

                           /* if(dataof.length() != 0)
                                _currentoffer.setText(currentoff);*/

                           /* if(dataof.length() != 0) {

                                    btnCancelSubscription.setVisibility(View.VISIBLE);
                                    btnCancelSubscription.setText("Cancel Subscription");
                                    btnCancelSubscription.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            api_cancelSubscription();
                                        }
                                    });

                                }
                                else {
                                    btnCancelSubscription.setVisibility(View.GONE);
                                }
*/
                               /* _currentoffer.setVisibility(View.VISIBLE);
                                _currentoffer.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        try {
                                            custome_message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(PaymentActivity.this);
                                            fb();
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
*/


                            


                        } catch (JSONException e) {

                            Log.d("rathod","JSON EXCEPTION :: "+e.getMessage());
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        } catch (Exception e){}

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("rathod","onErrorResponse :: "+error.getMessage());
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("useruniqueID",_User.get_User_Id());

                Log.d("USR","_User.get_User_Id() :: "+_User.get_User_Id());
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(Subscription,
                Utils.VOLEY_TAG);

    }


}

