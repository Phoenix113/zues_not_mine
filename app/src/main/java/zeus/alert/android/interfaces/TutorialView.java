package zeus.alert.android.interfaces;

public interface TutorialView {
    void onNextClick();
    void onBackClick();
}
