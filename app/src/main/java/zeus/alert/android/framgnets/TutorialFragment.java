package zeus.alert.android.framgnets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import zeus.alert.android.R;
import zeus.alert.android.activities.App_Base_Activity;
import zeus.alert.android.interfaces.TutorialView;

public class TutorialFragment extends Fragment {

    TutorialView tutorialView;
    private Button nextButton;
    private Button previousButton;
    private int pageNumber;
    private Button startZeus;

    public TutorialFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;
        switch (pageNumber) {
            case 0:
                v = inflater.inflate(R.layout.tutorial_first_frag, container, false);
                nextButton = (Button) v.findViewById(R.id.nextBtnFirst);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onNextClick();
                    }
                });
                break;
            case 1:
                v = inflater.inflate(R.layout.tutorial_second_frag, container, false);
                nextButton = (Button)v.findViewById(R.id.nextBtnSecond);
                previousButton = (Button)v.findViewById(R.id.prevBtnSecond);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onNextClick();
                    }
                });

                previousButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onBackClick();
                    }
                });
                break;
            case 2:
                v = inflater.inflate(R.layout.tutorial_third_frag, container, false);
                nextButton = (Button)v.findViewById(R.id.nextBtnThird);
                previousButton = (Button)v.findViewById(R.id.prevBtnThird);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onNextClick();
                    }
                });

                previousButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onBackClick();
                    }
                });
                break;
            case 3:
                v = inflater.inflate(R.layout.tutorial_fourth_frag, container, false);
                nextButton = (Button)v.findViewById(R.id.nextBtnFourth);
                previousButton = (Button)v.findViewById(R.id.prevBtnFourth);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onNextClick();
                    }
                });

                previousButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onBackClick();
                    }
                });
                break;
            case 4:
                v = inflater.inflate(R.layout.tutorial_fifth_frag, container, false);

                nextButton = (Button)v.findViewById(R.id.nextBtnFifth);
                previousButton = (Button)v.findViewById(R.id.prevBtnFifth);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onNextClick();
                    }
                });

                previousButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onBackClick();
                    }
                });

                break;
            case 5:
                v = inflater.inflate(R.layout.tutorial_sixth_frag, container, false);
                previousButton = (Button)v.findViewById(R.id.prevBtnSixth);
                startZeus = (Button)v.findViewById(R.id.start_zeus);

                startZeus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), App_Base_Activity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });

                previousButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tutorialView.onBackClick();
                    }
                });

                break;
                default:
                    v = inflater.inflate(R.layout.tutorial_first_frag, container, false);
        }

        return v;
    }


    public void setFragmentNumber(int fragmentNumber) {
        this.pageNumber = fragmentNumber;

    }

    public void setListener(TutorialView tutorialView) {
        this.tutorialView = tutorialView;
    }
}