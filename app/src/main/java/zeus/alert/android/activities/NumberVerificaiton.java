package zeus.alert.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import zeus.alert.android.R;
import zeus.alert.android.adapter.Adapter_Countries;
import zeus.alert.android.interfaces.Select_Country_Listener;
import zeus.alert.android.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

public class NumberVerificaiton extends Activity implements Select_Country_Listener {

    EditText _Number_ET;
    ListView listView;
    ImageView back_arrow;


    ArrayList<String> _Names = new ArrayList<String>();

    private Adapter_Countries _Adapter_categories;

    private String _Selected_Country = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.smsverification);

        _Names = getIntent().getStringArrayListExtra("country_names");

        _Number_ET = (EditText) findViewById(R.id.numberVerify);

        listView = (ListView) findViewById(R.id.listView);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "");
                setResult(RESULT_CANCELED, returnIntent);

                finish();
            }
        });




        _Adapter_categories = new Adapter_Countries(this, R.layout.adapter_country, _Names);
        listView.setAdapter(_Adapter_categories);


        _Number_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = _Number_ET.getText().toString()
                        .toLowerCase(Locale.getDefault());
                _Adapter_categories.filter_List_View(text);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });




    }




    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "");
        setResult(RESULT_CANCELED, returnIntent);
        Utils.HIDE_KEYBOARD(NumberVerificaiton.this, _Number_ET);
        finish();
    }


    @Override
    public void _Selected_Country(String country_Name) {


        _Selected_Country = country_Name;

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", _Selected_Country);
        setResult(RESULT_OK, returnIntent);
        Utils.HIDE_KEYBOARD(NumberVerificaiton.this, _Number_ET);
        finish();

    }
}
