package zeus.alert.android.adapter;

/**
 * Created by Moubeen Warar on 11/12/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import zeus.alert.android.R;

import zeus.alert.android.models.Coupon;

import java.util.List;

/**
 * @author Nouman
 */
public class CouponListAdapter extends BaseAdapter {
    Activity activity;
    List<Coupon> contacts;

    // private LayoutInflater inflater = null;
    public CouponListAdapter(Activity c, List<Coupon> contacts) {
        activity = c;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return contacts.get(position);
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(
                    R.layout.adapter_coupon_item_layout, parent, false);

            holder = new ViewHolder();
            holder.tvname = (TextView) convertView
                    .findViewById(R.id.tv_editcontact_name);
//            holder.imgPic = (CircleImageView) convertView
//                    .findViewById(R.id.item_image);
            // holder.closeview = (ImageView) convertView
            // .findViewById(R.id.iv_contactclose);
            holder.tvphone = (TextView) convertView
                    .findViewById(R.id.tv_editcontact_phone);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
            /*
             * holder.closeview.setOnClickListener(new OnClickListener() {
			 *
			 * @Override public void onClick(View arg0) { // TODO Auto-generated
			 * method stub int viewposition = listview.getPositionForView(arg0);
			 * Contacts cons = contacts.get(viewposition); DeteleContactAsync
			 * del = new DeteleContactAsync(activity); del.execute(
			 * String.valueOf(contacts.get(viewposition).contact_Id),
			 * String.valueOf(viewposition)); } });
			 */
        //     holder.imgPic.setImageDrawable(activity.getResources().getDrawable(
        //           R.drawable.ph_1));
        holder.tvname.setText(contacts.get(position).get_Promo_Name());
        holder.tvphone.setText(contacts.get(position).get_Description());

        //    get_User_Image(contacts.get(position).getString("email"), holder.imgPic);

        return convertView;
    }

/*    public void get_User_Image(String email, final CircleImageView _User_Profile) {

        ParseQuery query = new ParseQuery("ImageUpload");

        query.whereEqualTo("email", email);

        Log.e("", "updating user data : ");

        query.findInBackground(new FindCallback<ParseObject>() {


            @Override
            public void done(List<ParseObject> parseUsers, ParseException e) {

                if (parseUsers != null && parseUsers.size() > 0) {
                    Log.e("", "user exists : ");
                    ParseFile imageFile = (ParseFile) parseUsers.get(0).get("ImageFile");
                    imageFile.getDataInBackground(new GetDataCallback() {
                        public void done(byte[] data, ParseException e) {
                            if (e == null) {
                                // data has the bytes for the image

                                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                                _User_Profile.setImageBitmap(bmp);

                            } else {
                                // something went wrong
                            }
                        }
                    });

                }


            }
        });


    }*/

    class ViewHolder {
        ImageView closeview;
        //  CircleImageView imgPic;
        // String ImagePath;
        // ProgressBar progressBar;
        TextView tvname, tvphone, tvlisttime;
    }
}