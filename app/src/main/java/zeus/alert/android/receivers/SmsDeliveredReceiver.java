package zeus.alert.android.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import zeus.alert.android.utils.Utils;

/**
 * Created by Moubeen Warar on 11/13/2015.
 */
public class SmsDeliveredReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent arg1) {
        try {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Utils.TOAST_SUCCESS_RESPONSE(context, "Message is sent successfully");
                    break;
                case Activity.RESULT_CANCELED:
                    Utils.TOAST_ERROR_RESPONSE(context, "Error in sending Message");
                    break;
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
