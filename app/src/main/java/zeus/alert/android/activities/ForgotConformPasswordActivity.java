package zeus.alert.android.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;
import zeus.alert.android.webservices.Webservices;

import zeus.alert.android.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author Nouman
 */
public class ForgotConformPasswordActivity extends Activity {
    EditText EdtPassword = null;
    EditText EdtConformPSW = null;
    Button BtnConfirm = null;
    Button BtnCancel = null;

    String psw,confpsw;
    String unique_id;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        final String user_id = getIntent().getExtras().getString("unique_id");
        unique_id=user_id;


        setContentView(R.layout.activity_forgot_conform_password);
        ActionBar actionBar = getActionBar();

        EdtPassword = (EditText) findViewById(R.id.password);
        EdtConformPSW = (EditText) findViewById(R.id.confirm_password);
        BtnConfirm = (Button) findViewById(R.id.btn_confirm_psw);
        BtnCancel = (Button) findViewById(R.id.btn_cancel);
        BtnConfirm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (EdtPassword.getText().length() > 0) {
                    if (EdtConformPSW.getText().length() > 0) {
                        psw =EdtPassword.getText().toString().trim();
                        confpsw =EdtConformPSW.getText().toString().trim();
                        if (psw.equals(confpsw)) {

                            changePassword(EdtConformPSW.getText().toString().trim());
                        }
                        else {
                            Utils.TOAST_ERROR_RESPONSE(ForgotConformPasswordActivity.this, "Password doesn't match");
                        }
                    }else{
                        EdtConformPSW.setError("Please, Enter confirm password");
                        EdtConformPSW.requestFocus();
                    }
                }else{
                    EdtPassword.setError("Please, Enter password");
                    EdtConformPSW.requestFocus();
                }
            }
        });

        BtnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EdtPassword.setText("");
                EdtConformPSW.setText("");
            }
        });
    }

    private void changePassword(final String newpassword) {
        String ForgetPassword_URl = Webservices.ForgetPasswordUpdate(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ForgetPassword_URl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("rathod",""+s);
                        try {
                            JSONObject response = new JSONObject(s);
                            boolean _Status = response.getBoolean("status");
                            if (_Status) {
                                //Toast.makeText(ForgotConformPasswordActivity.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();

                                LayoutInflater inflater = ForgotConformPasswordActivity.this.getLayoutInflater();
                                View alertLayout = inflater.inflate(R.layout.ot_ob, null);
                                AlertDialog.Builder alert = new AlertDialog.Builder(ForgotConformPasswordActivity.this);
                                TextView text,button;
                                text = (TextView)alertLayout.findViewById(R.id.ot_ob_t1);
                                button = (TextView)alertLayout.findViewById(R.id.ot_ob_b1);
                                text.setText("Password changed successfully");
                                button.setText("Login");
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(dialog.isShowing())
                                        {
                                            dialog.dismiss();
                                            Intent i = new Intent(ForgotConformPasswordActivity.this,App_Base_Activity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    }
                                });
                                alert.setView(alertLayout);
                                dialog = alert.create();
                                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.show();

                            } else {
                                Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "" + response.getString("error_msg"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Showing toast
                        //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<String, String>();
                params.put("unique_id",unique_id);
                params.put("password", newpassword);
                Log.d("RRR","unique_id :: "+unique_id);
                Log.d("RRR","password :: "+newpassword);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
