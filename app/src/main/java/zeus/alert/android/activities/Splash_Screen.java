package zeus.alert.android.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import zeus.alert.android.R;
import zeus.alert.android.models.Tutorial;
import zeus.alert.android.service.Zeus_Service;
import zeus.alert.android.utils.Utils;

import com.sromku.simple.fb.SimpleFacebook;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

import static zeus.alert.android.activities.ZeusApplication.TAG;

/**
 * Created by Moubeen Warar on 11/7/2015.
 */

public class Splash_Screen extends Activity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private SimpleFacebook mSimpleFacebook;
    private Runnable mRunnable;

    Handler mHandler = new Handler();
    static Dialog dialog;

    Tutorial _Tutorial;


    @SuppressLint("PackageManagerGetSignatures")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("zeus.alert.android", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;

                md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("hash key", hashKey);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ButterKnife.inject(this);

        dialog = new Dialog(Splash_Screen.this);

        dialog.setContentView(R.layout.customdialog);

        dialog.setCancelable(false);


        dialog.setTitle(getResources().getString(R.string.warning));


        final Button Ok = (Button) dialog.findViewById(R.id.Btn_Ok);
        Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });
        if (checkAndRequestPermissions()) {
            Intent intent = new Intent(this, Zeus_Service.class);
            startService(intent);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

            mSimpleFacebook = SimpleFacebook.getInstance(Splash_Screen.this);
//            Utils.TRANSLUCENT_STATUS_BAR(this);

            IntentFilter filter = new IntentFilter("android.location.PROVIDERS_CHANGED");
            this.getApplication().registerReceiver(mReceivedSMSReceiver, filter);

            open_App_Base_Screen();
        } else {
            Log.d(TAG, "onCreate: " + "no permissions granted");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
    }

    private void open_App_Base_Screen() {
        mRunnable = new Runnable() {

            @Override
            public void run() {

                Utils._USER = Utils.GET_USER_FROM_SHARED_PREFS(Splash_Screen.this);

                _Tutorial = Utils.GET_TUTORIAL_FROM_SHARED_PREFS(Splash_Screen.this);


                Utils.GET_LAST_KNOWN_LOCATION(getApplicationContext());
                try {
                    if (Utils._LATITUDE != 0.0 && Utils._LONGITUDE != 0.0) {
                        Intent intent = new Intent(Splash_Screen.this, App_Base_Activity.class);
                        if (Utils.DETECT_INTERNET_CONNECTION(Splash_Screen.this)) {
                            if (_Tutorial != null&&_Tutorial.is_Tutorial_Shown()) {
                                    if (Utils._USER != null&&Utils._USER.is_Login()) {
                                        intent = new Intent(Splash_Screen.this, App_Home_Page.class);
                                    }
                            } else {
                                intent = new Intent(Splash_Screen.this, Tutorial_Activity.class);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Utils.TOAST_ERROR_RESPONSE(Splash_Screen.this, "No Internet Connection");
                        }
                    } else {

                        dialog.show();

                    }
                } catch (Exception e) {
                    e.getMessage();
                }
                finish();
            }


        };
        mHandler.postDelayed(mRunnable, 2500);
    }

    BroadcastReceiver mReceivedSMSReceiver = new BroadcastReceiver() {
        String locationMode;

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void onReceive(final Context context, Intent intent) {

            try {

                ContentResolver contentResolver = context.getContentResolver();


                int mode = Settings.Secure.getInt(
                        contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);

                if (mode == Settings.Secure.LOCATION_MODE_OFF) {


                    if (!dialog.isShowing()) {

                        Log.d("rathod", "show");
                        dialog.show();
                    }

                    // Location is turned OFF!
                } else {
                    // Location is turned ON!


                    dialog.dismiss();


                    if (_Tutorial != null) {

                        if (_Tutorial.is_Tutorial_Shown()) {
                            if (Utils._USER != null) {

                                if (Utils._USER.is_Login()) {

                                    Intent intent1 = new Intent(Splash_Screen.this, App_Home_Page.class);
                                    startActivity(intent1);


                                } else {

                                    Intent intent1 = new Intent(Splash_Screen.this, App_Base_Activity.class);
                                    startActivity(intent1);
                                }

                            } else {
                                Intent intent1 = new Intent(Splash_Screen.this, App_Base_Activity.class);
                                startActivity(intent1);
                            }
                        } else {
                            Intent intent1 = new Intent(Splash_Screen.this, Tutorial_Activity.class);
                            startActivity(intent1);

                           /* Utils.IS_FRISTTIMELOGIN=true;
                            Intent intent2 = new Intent(Splash_Screen.this, App_Base_Activity.class);
                            startActivity(intent2);*/
                        }

                    } else {

                        Intent intent1 = new Intent(Splash_Screen.this, Tutorial_Activity.class);
                        startActivity(intent1);

                      /*  Utils.IS_FRISTTIMELOGIN=true;
                        Intent intent2 = new Intent(Splash_Screen.this, App_Base_Activity.class);
                        startActivity(intent2);*/

                    }

                    Log.d("rathod", "hide1");


                    // Get the Mode value from Location system setting
                    LocationManager locationManager = (LocationManager) context.
                            getSystemService(Context.LOCATION_SERVICE);
                    assert locationManager != null;
                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                            && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        locationMode = "High accuracy. Uses GPS, Wi-Fi, and mobile networks to determine location";
                    } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        locationMode = "Device only. Uses GPS to determine location";
                    } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        locationMode = "Battery saving. Uses Wi-Fi and mobile networks to determine location";
                    }
                    Log.d("Sid", "Location status :: " + locationMode);
                }
            } catch (Exception e) {

                Log.d("rathod", "error" + e.getMessage());
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (checkAndRequestPermissions()) {
            Intent intent = new Intent(this, Zeus_Service.class);
            startService(intent);
            open_App_Base_Screen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRunnable != null && mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.d(TAG, "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "sms & location services permission granted");
                        // process the normal flow
                        if (checkAndRequestPermissions()) {
                            Intent intent = new Intent(this, Zeus_Service.class);
                            startService(intent);
                            open_App_Base_Screen();
                        }
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK(
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast enablePermissions = Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG);
                            enablePermissions.show();
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage("SMS and Location Services Permission required for this app")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
