package zeus.alert.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import zeus.alert.android.R;
import zeus.alert.android.interfaces.Select_Country_Listener;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Mobeen Farooq on 3/25/2015.
 */
public class Adapter_Countries extends BaseAdapter {

    private Context context;
    private int singleRow;

    private ArrayList<String> _Countries = new ArrayList<String>();
    ;
    private ArrayList<String> _Countries_Filter = new ArrayList<String>();

    Select_Country_Listener select_country_listener;
    private String _Country_Name = "";

    public Adapter_Countries(Context context, int hitListLayout,
                             ArrayList<String> _Countries) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this.singleRow = hitListLayout;

        this._Countries = _Countries;

        //     Log.e("", "_Selected_Category_Name constructor : " + _Selected_Category_Name);
        _Countries_Filter.addAll(_Countries);

        select_country_listener = (Select_Country_Listener) context;


    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return _Countries_Filter.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


        final String _category_Name =
                _Countries_Filter.get(position);

        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(singleRow, parent, false);
            viewHolder = new ViewHolder();
            viewHolder._Country_Name = (TextView) convertView
                    .findViewById(R.id.country_name_TextView);


            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        if (_Country_Name.equals(_category_Name)) {
            viewHolder._Country_Name.setTextColor(context.getResources().getColor(R.color.zeusColor));
        }

        viewHolder._Country_Name.setId(position);
        viewHolder._Country_Name.setText(_category_Name);
        viewHolder._Country_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                select_country_listener._Selected_Country(_Countries_Filter.get(position));
                _Country_Name=_Countries_Filter.get(position);
                viewHolder._Country_Name.setTextColor(context.getResources().getColor(R.color.zeusColor));


            }
        });


        return convertView;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return null == _Countries_Filter ? 0 : _Countries_Filter.size();
    }

    protected class ViewHolder {


        TextView _Country_Name;


    }

    public void filter_List_View(String charText) {
        charText =
                charText.toLowerCase(Locale.getDefault());


        _Countries_Filter.clear();

        if (charText.length() == 0) {

            _Countries_Filter.addAll(_Countries);
        } else {

            for (int tel_Ref_Count = 0; tel_Ref_Count < _Countries.size(); tel_Ref_Count++) {

                if (_Countries.get(tel_Ref_Count).toLowerCase().startsWith(charText.toLowerCase())) {


                    _Countries_Filter.add(_Countries.get(tel_Ref_Count));

                }


            }


        }

        notifyDataSetChanged();
    }

}
