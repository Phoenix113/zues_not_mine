package zeus.alert.android.webservices;

import android.content.Context;
import android.util.Log;

import zeus.alert.android.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by moubeen on 8/1/16.
 */
public class Webservices {


    private static String USERNAME = "username=";
    private static String EMAIL = "email=";
    private static String CITY = "city=";
    private static String COUNTRY = "country=";
    private static String PHONE_NUMBER = "phone_number=";
    private static String DISTANCE= "dis=";
    private static String PASSWORD = "password=";
    private static String LATITUTDE = "lat=";
    private static String LONGITUDE = "lon=";
    private static String USERTYPE_ID="usertype_id=";
    private static String USER_PHONE_NUMBER = "user_phone_number=";


    private static String CONTACT_NAME = "contact_name=";

    private static String UNIQUE_ID = "unique_id=";



    private static String USER_UNIQUE_ID = "useruniqueID=";
    private static String USER_SUBSCRIPTION_ID = "&subscriptionID=";
    private static String USER_PLAN_ID = "&PlanID=";
    private static String USER_CUSTOMER_ID = "&CustomerID=";
    private static String USER_EXPIRY_MONTH = "&ExpMonth=";
    private static String USER_CREATED_DATE = "&CreatedAt=";




    private static String AND = "&";


    public static String UPDATE_USER_LAT_LNG(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_user_lat_lng);
    }
    public static String REGISTORUSERMESSAGESEND(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.regmsgsend);
    }
    private static String get_Server_Base_Url(Context context) {
        return context.getResources().getString(R.string.base_url);
    }

    public static String NEARBYUSER(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.nearbyser);
    }
    public static String MESSAGESEND(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.msgsend);
    }
    public static String GETUSERDETAIL(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.getUserDeatils);
    }


    public static String UPDATE_USER_TOKENID(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_user_tokenid);
    }
    public static String UPDATE_USER_STATUS(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_user_status);
    }
    public static String UPDATE_USER_TYPES(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_user_types);
    }
    public static String UPDATE_USER_payment(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_user_paymentsuccess);
    }
    public static String ForgetSentOtp(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.forget_password);
    }
    public static String ForgetPasswordUpdate(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.forget_newpassword);
    }
    public static String UPDATE_USER(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_user);
    }
    public static String REGISTER_USER(Context context, String username, String password, String email, String lat, String lng, String country, String phoneNumber,String usertype_id) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.register_user) +
                USERNAME + Encode_String(username) + AND + EMAIL + Encode_String(email) + AND + PASSWORD + Encode_String(password) + AND + LATITUTDE + lat + AND + LONGITUDE + lng +
               AND + COUNTRY + Encode_String(country) + AND + PHONE_NUMBER + phoneNumber+AND + USERTYPE_ID + usertype_id;
    }

    public static String LOGIN_USER(Context context, String username, String password) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.login_user) +
                USERNAME + Encode_String(username) + AND + PASSWORD + Encode_String(password);
    }


    public static String ADD_USER(Context context, String userid, String username, String phone) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.add_user) +
                CONTACT_NAME + Encode_String(username) + AND + USER_PHONE_NUMBER + Encode_String(phone) + AND + UNIQUE_ID + userid;
    }


    public static String GET_USER_CONTACTS(Context context, String userid) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.get_contacts) +
                UNIQUE_ID + userid;
    }

    public static String GET_PROMOCODE(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.get_coupons);
    }
    public static String GET_USER_PLANS(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.get_plans);
    }


    public static String UPDATE_USER_CONTACT(Context context, String userid, String contactId, String name, String phonenumber) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.update_contact) +
                UNIQUE_ID + userid + AND + USER_PHONE_NUMBER + Encode_String(phonenumber) + AND + CONTACT_NAME + Encode_String(name) + AND + "id=" + contactId;
    }


    public static String DELETE_USER_CONTACT(Context context, String userid, String contactId) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.delete_contact) +
                UNIQUE_ID + userid + AND + "id=" + contactId;
    }


    public static String LIVE_MAP_USERS(Context context, String lat, String lon, String phonenumber,String dis,String userid) {
        if(context!=null){
            return get_Server_Base_Url(context) + context.getResources().getString(R.string.live_map_users) +
                    LATITUTDE + lat + AND + LONGITUDE + lon + AND + PHONE_NUMBER + phonenumber+AND+DISTANCE+dis+AND+"uniqueuserid="+userid;
        }
        return "";

    }

    public static String CANCEL_SUBSCRIPTION(Context context, String useruniqueID) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.cancel_subscription) +
                USER_UNIQUE_ID + useruniqueID;
    }

    public static String GETPLAN(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.getplans);
    }

    public static String SHAREOFFER(Context context) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.shareoffer);
    }


    private static String Encode_String(String string) {

        Log.d("rathod","orginal:="+string);
        try {

            string = URLEncoder.encode(string, "UTF-8");


        } catch (UnsupportedEncodingException e) {

            Log.e("zeus", "exception in webserice Send_Notification UTF-8 not supported");
            e.printStackTrace();

        }
        Log.d("rathod","ecode:="+string);
        return string;

    }


    public static String INSERT_SUBSCRIPTION_DETAILS(Context context, String useruniqueID,
                                                     String subscriptionID, String planID,
                                                     String customerID, int stripe_exp_month,
                                                     String stripe_created_formatedDate) {
        String ss = get_Server_Base_Url(context) +""+ context.getResources().getString(R.string.insert_subscription_details) +""+
                USER_UNIQUE_ID +""+ useruniqueID +""+
                USER_SUBSCRIPTION_ID +""+ subscriptionID +""+
                USER_PLAN_ID +""+ planID +""+
                USER_CUSTOMER_ID +""+ customerID +""+
                USER_EXPIRY_MONTH +""+ stripe_exp_month +""+
                USER_CREATED_DATE +""+ stripe_created_formatedDate;

        Log.d("RRRR","INSERT_SUBSCRIPTION_DETAILS :: "+ ss);

        return get_Server_Base_Url(context) +""+ context.getResources().getString(R.string.insert_subscription_details) +""+
                USER_UNIQUE_ID +""+ useruniqueID +""+
                USER_SUBSCRIPTION_ID +""+ subscriptionID +""+
                USER_PLAN_ID +""+ planID +""+
                USER_CUSTOMER_ID +""+ customerID +""+
                USER_EXPIRY_MONTH +""+ stripe_exp_month +""+
                USER_CREATED_DATE +""+ stripe_created_formatedDate;
    }

    public static String CheckSubscription(Context context, String useruniqueID) {
        String ss = get_Server_Base_Url(context) +""+ context.getResources().getString(R.string.checkSubscription) +""+
                USER_UNIQUE_ID +""+ useruniqueID ;

        Log.d("RRRR"," CheckSubscription  url:: "+ ss);

        return get_Server_Base_Url(context) +""+ context.getResources().getString(R.string.checkSubscription) +""+
                USER_UNIQUE_ID +""+ useruniqueID ;
    }

    public static String cancel_api_Subscription(Context context, String useruniqueID) {
        String ss = get_Server_Base_Url(context) +""+ context.getResources().getString(R.string.unsubscribe) +""+
                USER_UNIQUE_ID +""+ useruniqueID ;

        Log.d("RRRR"," CheckSubscription  url:: "+ ss);

        return get_Server_Base_Url(context) +""+ context.getResources().getString(R.string.unsubscribe) +""+
                USER_UNIQUE_ID +""+ useruniqueID ;
    }


    /*
    * Triton Updates
    * */

    public static String socialcheck(Context context,String facebook_id, String username) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.sociallogincheck)+"facebook_id="+facebook_id+"&username="+Encode_String(username)+"&email=kgkgkg";
    }

    public static String REGISTER_USER_fb(Context context, String username, String password, String email, String lat, String lng, String country, String phoneNumber,String usertype_id,String facebook_id) {
        return get_Server_Base_Url(context) + context.getResources().getString(R.string.register_user_fb) +
                USERNAME + Encode_String(username) + AND + EMAIL + Encode_String(email) + AND + PASSWORD + Encode_String(password) + AND + LATITUTDE + lat + AND + LONGITUDE + lng +
                AND + COUNTRY + Encode_String(country) + AND + PHONE_NUMBER + phoneNumber+AND + USERTYPE_ID + usertype_id+AND + "facebook_id=" + facebook_id;
    }

}
