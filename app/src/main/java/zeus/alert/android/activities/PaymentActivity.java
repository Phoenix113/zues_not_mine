package zeus.alert.android.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;

import zeus.alert.android.dialog.ErrorDialogFragment;
import zeus.alert.android.dialog.ProgressDialogFragment;
import zeus.alert.android.interfaces.PaymentForm;
import zeus.alert.android.models.Credit_Card_Crecentials;
import zeus.alert.android.models.Custome_Message_And_Access_Token;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.LogDisplay;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.SendButton;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.kyleduo.switchbutton.SwitchButton;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;

import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Coupon;
import com.stripe.model.Customer;
import com.stripe.model.Plan;
import com.stripe.model.PlanCollection;
import com.stripe.model.Subscription;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;


import static com.stripe.Stripe.apiKey;

public class PaymentActivity extends FragmentActivity {

public static String currentplan = "";
public static String currentoff = "";
    CallbackManager callbackManager;
   public static final String PUBLISHABLE_KEY = "pk_live_E3G3cra3gIcQmFaI31rwcd9I";
 // public static final String PUBLISHABLE_KEY = "pk_test_ML4zr0cLOPd3eB3uj68icfD0";

    Custome_Message_And_Access_Token custome_message;
    private ProgressDialogFragment progressFragment;

    @InjectView(R.id.email)
    EditText _Email;

    @InjectView(R.id.number)
    EditText _Card_Number;

    @InjectView(R.id.currentplan)
    TextView _currentplan;

    @InjectView(R.id.currentoffer)
    TextView _currentoffer;

    @InjectView(R.id.date)
    EditText _Date;

    @InjectView(R.id.cvc)
    EditText _Cvc;

    @InjectView(R.id.Edt_CouponCode)
    EditText _Edt_CouponCode;

    @InjectView(R.id.back_arrow)
    ImageView relativeRl;

    @InjectView(R.id.pay)
    Button _Pay;

    @InjectView(R.id.sb_nofade)
    SwitchButton switchButton;

    @InjectView(R.id.spn_plan)
    Spinner _SpnPlan;

    @InjectView(R.id.sharebuttom)
    ShareButton sharebuttom;

    String _email = "";
    String _CardNumber = "";
    int month = -1;
    int year = -1;
    int cvc = -1;
    User _User;
    private int stripe_exp_month;
    private String stripe_created_formatedDate;
    List<Plan>  PlanList;
    Plan Selectedplan;

    private SimpleFacebook mSimpleFacebook;
    static Coupon c;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);
        ButterKnife.inject(this);
        mSimpleFacebook = SimpleFacebook.getInstance(PaymentActivity.this);

        progressFragment = ProgressDialogFragment.newInstance(R.string.progressMessage);
        sharebuttom.setVisibility(View.GONE);
        /*
        * tritons
        * */
        // test
       //apiKey =  "sk_test_oa5XMJTkP8V6cfikcnS5xIQ3";

        //live
       apiKey =  "sk_live_RBEHFfevvLJI5PDadqnFb2DY";
        //fun();
        _User=Utils.GET_USER_FROM_SHARED_PREFS(getApplicationContext());
        _currentplan.setText("");
        _currentoffer.setText("");
        _currentoffer.setVisibility(View.GONE);

        getCurrentSubscription();

        /*
        * already there
        * */
//       apiKey =  "sk_test_oa5XMJTkP8V6cfikcnS5xIQ3";

       // apiKey = "sk_live_RBEHFfevvLJI5PDadqnFb2DY";

        get_Plan_List_from_Stripe();

        relativeRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    save_credentials_to_shared_prefs(true);
                } else {
                    Credit_Card_Crecentials credit_card_crecentials = new Credit_Card_Crecentials();
                    credit_card_crecentials.set_Is_Checked(false);
                    Utils.SAVE_CREDIT_CARD_CREDENTIALS_TO_SHAREDPREFS(PaymentActivity.this, credit_card_crecentials);

                }

            }
        });


        _Edt_CouponCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("change text",_Edt_CouponCode.getText().toString());
                //getCoupon(_Edt_CouponCode.getText().toString());


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        _Date.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    CharSequence charSequence=_Date.getText();
                    int size=charSequence.length();
                    if (size==5){
                        charSequence=charSequence.subSequence(0,3);
                        _Date.setText(charSequence);

                    }
                }
                return false;
            }
        });



        _Date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                LogDisplay.displayLog(getApplicationContext(),"Before Change :: "+s.length());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                LogDisplay.displayLog(getApplicationContext(),"onTextChanged :: "+s.length());
            }

            @Override
            public void afterTextChanged(Editable s) {
                LogDisplay.displayLog(getApplicationContext(),"afterTextChanged :: "+s.length());

                /*Log.d("sid "," s :: "+s);*/

                if (s.length()==4){
                    CharSequence st = s.subSequence(0,2);
                    st = st + "/"+s.subSequence(2,4);
                    LogDisplay.displayLog(getApplicationContext(),"st :: "+st);
                    _Date.setText(st);
                }

            }
        });





        _Pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                _email = _Email.getText().toString();
                if (_Card_Number.getText().toString().equals("")) {
                    Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter card number");
                } else {
                    _CardNumber = _Card_Number.getText().toString().trim();
                }

                String date = _Date.getText().toString().trim();

                if (_Cvc.getText().toString().equals("")) {
                    Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter valid cvc");
                } else {
                    cvc = Integer.parseInt(_Cvc.getText().toString().trim());
                }


                if (Utils._Is_Email_Valid(_email)) {
                    if (!_CardNumber.equals("")) {

                        if (cvc != -1) {

                            if (date.length() > 5) {

                                Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");

                            } else if (date.contains("/")) {

                                int _is_contain_dash_more_then_once = 0;

                                for (int i = 0; i < date.length(); i++) {

                                    char c = date.charAt(i);

                                    if (c == '/') {
                                        _is_contain_dash_more_then_once++;
                                    }

                                }

                                if (_is_contain_dash_more_then_once > 1) {

                                    Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");

                                } else {
                                    if (date.length() == 5) {
                                        String[] month_and_date = date.split("/");
                                        Log.e("zeus", "month : " + month_and_date[0]);
                                        Log.e("zeus", "year : " + month_and_date[1]);

                                        month = Integer.parseInt(month_and_date[0]);
                                        year = Integer.parseInt(month_and_date[1]);


                                        if (Utils.DETECT_INTERNET_CONNECTION(PaymentActivity.this)) {



                                               //verify_coupon("qwerty");
                                            create_card(_email, _CardNumber, month, year, cvc);

                                        } else {
                                            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "No Internet Connection.");
                                        }


                                    } else {
                                        Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");
                                    }


                                }


                            } else {
                                Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");
                            }

                        } else {
                            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter cvc");
                        }

                    } else {
                        Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter card number");
                    }


                } else {
                    Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid Email Format");
                }


            }
        });


        _Email.setEnabled(false);
        _Email.setText(_User.get_Email());


        Credit_Card_Crecentials credit_card_crecentials = Utils.GET_CREDIT_CARD_CREDENTIALS_FROM_SHARED_PREFS(PaymentActivity.this);

        if (credit_card_crecentials != null) {

            if (credit_card_crecentials.is_Is_Checked()) {
                _Email.setText("" + credit_card_crecentials.get_Email());
                _Card_Number.setText("" + credit_card_crecentials.get_CardNumber());
                _Date.setText("" + credit_card_crecentials.get_Month() + "/" + credit_card_crecentials.get_Year());
                _Cvc.setText("" + credit_card_crecentials.getCvc());

                switchButton.setChecked(true);
            }


        }


        /*if (Utils.IS_PREMIUM_PURCHASED) {
            _Pay.setText("Premium Purchased");
            _Pay.setEnabled(false);
        }*/

    }






    private void save_credentials_to_shared_prefs(boolean isChecked) {


        _email = _Email.getText().toString();
        if (_Card_Number.getText().toString().equals("")) {
            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter card number");
            switchButton.setChecked(false);
        } else {
            _CardNumber = _Card_Number.getText().toString().trim();


        }

        String date = _Date.getText().toString().trim();

        if (_Cvc.getText().toString().equals("")) {
            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter valid cvc");
            switchButton.setChecked(false);
        } else {
            cvc = Integer.parseInt(_Cvc.getText().toString().trim());

        }


        if (Utils._Is_Email_Valid(_email)) {
            if (!_CardNumber.equals("")) {

                if (cvc != -1) {

                    if (date.length() > 5) {

                        Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");
                        switchButton.setChecked(false);

                    } else if (date.contains("/")) {

                        int _is_contain_dash_more_then_once = 0;

                        for (int i = 0; i < date.length(); i++) {

                            char c = date.charAt(i);

                            if (c == '/') {
                                _is_contain_dash_more_then_once++;
                            }

                        }

                        if (_is_contain_dash_more_then_once > 1) {

                            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");
                            switchButton.setChecked(false);

                        } else {
                            if (date.length() == 5) {
                                String[] month_and_date = date.split("/");
                                Log.e("zeus", "month : " + month_and_date[0]);
                                Log.e("zeus", "year : " + month_and_date[1]);

                                month = Integer.parseInt(month_and_date[0]);
                                year = Integer.parseInt(month_and_date[1]);


                                //    create_card(_email,_CardNumber,month,year,cvc);

                                Credit_Card_Crecentials credit_card_crecentials = new Credit_Card_Crecentials();

                                credit_card_crecentials.set_CardNumber(_CardNumber);
                                credit_card_crecentials.set_Email(_email);
                                credit_card_crecentials.set_Month(month);
                                credit_card_crecentials.set_Year(year);
                                credit_card_crecentials.setCvc(cvc);
                                credit_card_crecentials.set_Is_Checked(true);

                                Utils.SAVE_CREDIT_CARD_CREDENTIALS_TO_SHAREDPREFS(PaymentActivity.this, credit_card_crecentials);


                            } else {
                                Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");
                                switchButton.setChecked(false);
                            }


                        }


                    } else {
                        Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid expiry date");
                        switchButton.setChecked(false);
                    }

                } else {
                    Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter cvc");
                    switchButton.setChecked(false);
                }

            } else {
                Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Please enter card number");
                switchButton.setChecked(false);
            }


        } else {
            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Invalid Email Format");
            switchButton.setChecked(false);
        }


    }

    private void create_card(String email, String cardNumber, int month, int year, int cvc) {


        Log.e("zeus", "card number : " + cardNumber);
        final Card card = new Card(
                "" + cardNumber,
                month,
                year,
                "" + cvc);

        card.setName(email);

        boolean validation = card.validateCard();
        if (validation) {
            startProgress();
            new Stripe().createToken(
                    card,
                    PUBLISHABLE_KEY,
                    new TokenCallback() {
                        public void onSuccess(Token token) {


                          //  send_Tocken_To_Parse(token);

                            try{
                                stripe_exp_month = token.getCard().getExpMonth();

                                LogDisplay.displayLog(getApplicationContext(),"stripe_exp_month :: "+stripe_exp_month);

                                Date stripe_created = token.getCreated(); // get created date //Fri Jan 20 05:09:49 EST 2017
                                Log.e("zeuss", "Created Date :: " +stripe_created);

                                Calendar calendar=Calendar.getInstance();
                                calendar.setTime(stripe_created);

                                stripe_created_formatedDate =  calendar.get(Calendar.YEAR) + "/" +
                                                        (calendar.get(Calendar.MONTH) + 1) + "/" +
                                                          calendar.get(Calendar.DATE);
                                LogDisplay.displayLog(getApplicationContext(),"stripe_created_formatedDate :: "+
                                        stripe_created_formatedDate);

                            }catch (Exception e){}
                            if(_Edt_CouponCode!=null&&!_Edt_CouponCode.getText().toString().isEmpty())
                                ValidCoupon(_Edt_CouponCode.getText().toString());

                            CreateCustomer(token.getId());
                            finishProgress();
                        }

                        public void onError(Exception error) {
                            handleError(error.getLocalizedMessage());
                            finishProgress();
                        }
                    });
        } else if (!card.validateNumber()) {
            handleError("The card number that you entered is invalid");
        } else if (!card.validateExpiryDate()) {
            handleError("The expiration date that you entered is invalid");
        } else if (!card.validateCVC()) {
            handleError("The CVC code that you entered is invalid");
        } else {
            handleError("The card details that you entered are invalid");
        }
    }

    private void ValidCoupon(final String cp) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    c=Coupon.retrieve(cp);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.d("rathod","error Vaildation of Coupon");
                }
                return null;
            }

            protected void onPostExecute(Void result) {

            };

        }.execute();

    }

    private void CreateCustomer(String tokenId) {

        final Map<String, Object> customerParams = new HashMap<String, Object>();
        customerParams.put("source",tokenId);
        customerParams.put("description", _User.get_User_Name());
        customerParams.put("email",_User.get_Email());

        new AsyncTask<Void, Void, Void>() {
            Customer customer;
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    customer = Customer.create(customerParams);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.d("rathod","error in Create custometer");
                            }
                return null;
            }

            protected void onPostExecute(Void result) {
                if (customer!=null){
                    Log.d("rathod","Customer Created Successfully...");
                    Log.d("rathod","customer id := "+customer.getId());
                    CreateSubscption(customer.getId(),PlanList.get(_SpnPlan.getSelectedItemPosition()).getId());
                }

            };

        }.execute();

    }



    public void CreateSubscption(final String cust_id, final String planId) {

        final Map<String, Object> subscriptionParams = new HashMap<String, Object>();
        subscriptionParams.put("customer", cust_id);
        subscriptionParams.put("plan", planId);
        if(c!=null){
            if(c.getValid()){
                subscriptionParams.put("coupon", c.getId());
            }
        }
        new AsyncTask<Void, Void, Void>() {

            Subscription subscription;

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    subscription =  Subscription.create(subscriptionParams);
                } catch (Exception e) {
                    Log.d("Rathod","error in subscription");
                }
                return null;
            }

            protected void onPostExecute(Void result) {

                if (subscription!=null){



                    Log.d("rathod","subscription Created Successfully...");


                    if(Selectedplan!=null){
                        String Msg= "";

                        Double FInalTotal = 0.0;
                        Double Discount=0.0;
                        Double Subtotal=Double.parseDouble(Selectedplan.getAmount().toString())/100;

                      //  Log.d("rathod","Sub Total:="+Subtotal);


                        if(c!=null)
                        {
                            if(c.getValid()){
                                //Log.d("rathod","Coupon ="+c.getId());

                                Msg="Sub Total"+Subtotal+ " "+Selectedplan.getCurrency()+"\n"+"Coupon : "+c.getId();

                                if(c.getPercentOff()!=null){
                                    Discount=Subtotal * Double.parseDouble(c.getPercentOff().toString())/100;
                                    FInalTotal=Subtotal-Discount;
                                    Msg="Sub Total : "+Subtotal+ " "+Selectedplan.getCurrency()+"\n"+"Coupon : "+c.getId()+"\n"+"Saving : "+Discount+" "+Selectedplan.getCurrency()+ "\n"+"Discount: "+c.getPercentOff().toString()+" %"+"\n"+"Total : "+FInalTotal+" "+Selectedplan.getCurrency();
                                }
                                if(c.getAmountOff()!=null){
                                    Discount=Discount+ (Subtotal - Double.parseDouble(c.getAmountOff().toString())/100);
                                    FInalTotal=Subtotal-Discount;
                                    Msg="Sub Total"+Subtotal+ " "+Selectedplan.getCurrency()+"\n"+"Coupon : "+c.getId()+"\n"+"Saving : "+Discount+" "+Selectedplan.getCurrency()+"\n"+"Total : "+FInalTotal+" "+Selectedplan.getCurrency();

                                }
                                //Log.d("rathod","Saving :="+Discount);
                              //  Log.d("rathod","discount:="+c.getPercentOff()+"%");
                               // Log.d("rathod","Total:="+FInalTotal);

                            }else {
                                Msg="Total : "+Subtotal+ " "+Selectedplan.getCurrency();
                            }
                        }else {
                            Msg="Total : "+Subtotal+ " "+Selectedplan.getCurrency();
                        }

                        update_user_types(subscription,Subtotal);
                        //Log.d("rathod",""+Msg);

                        AlertDialog.Builder alertDialog=new AlertDialog.Builder(PaymentActivity.this);
                        alertDialog.setTitle("Payment Successful");
                        alertDialog.setMessage(Msg);
                        alertDialog.setCancelable(false);
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent=new Intent(PaymentActivity.this,App_Home_Page.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                        alertDialog.show();
                    }



                    Log.d("rathod","...over successfulyy done...");

                    // insertSubscriptionDetails(_User.get_User_Id(),subscription.getId(),planId,cust_id);.

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Coupon is not valid",Toast.LENGTH_SHORT).show();

                }
            };

        }.execute();


    }

    private void insertSubscriptionDetails(String useruniqueID, final String subscriptionID,
                                           final String PlanID, final String CustomerID) {

        LogDisplay.displayLog(getApplicationContext(),"insertSubscriptionDetails :: ");

        LogDisplay.displayLog(getApplicationContext(),"stripe_exp_month :: "+
                stripe_exp_month);

        LogDisplay.displayLog(getApplicationContext(),"stripe_created_formatedDate :: "+
                stripe_created_formatedDate);

        String url= Webservices.INSERT_SUBSCRIPTION_DETAILS(getApplicationContext(),
                _User.get_User_Id(),subscriptionID,PlanID,
                CustomerID,stripe_exp_month,stripe_created_formatedDate);

        //http://kaprat.com/dev/zeusapp/API/InsertSubscriptionDetails.php?useruniqueID=1&subscriptionID=1&PlanID=1&CustomerID=1&ExpMonth=12&CreatedAt=datetime
        /*String url_sub_det="http://kaprat.com/dev/zeusapp/API/InsertSubscriptionDetails.php?"+
                "useruniqueID="+_User.get_User_Id()+"&"+
                "subscriptionID="+subscriptionID+"&"+
                "PlanID="+PlanID+"&"+
                "CustomerID="+CustomerID+"&"+
                "ExpMonth="+stripe_exp_month+"&"+
                "CreatedAt="+stripe_created_formatedDate;
*/
        Log.d("zuess ","When call InsertSubscriptionDetails.php Api :: ");
        Log.d("zuess ","useruniqueID :: "+_User.get_User_Id());
        Log.d("zuess ","subscriptionID :: "+subscriptionID);
        Log.d("zuess ","PlanID :: "+PlanID);
        Log.d("zuess ","CustomerID :: "+CustomerID);
        Log.d("zuess ","ExpMonth :: "+stripe_exp_month);
        Log.d("zuess ","CreatedAt date :: "+stripe_created_formatedDate);


        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.d("RRR","Response  "+ s);

                try {
                    JSONObject jsonObject=new JSONObject(s);

                    boolean _status= jsonObject.getBoolean("status");

                    if (_status){

                        Log.d("RRR","   True    ");

                        //Toast.makeText(PaymentActivity.this, "True", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(PaymentActivity.this, "false", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (Exception  ex) {
                    ex.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                if (volleyError instanceof NoConnectionError)
                {
                    Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "No  Internet Connection." );

                }else if (volleyError.networkResponse == null){
                    if (volleyError.getClass().equals(TimeoutError.class)){
                        Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "Oops. Connection Timeout!" );
                    }
                }else{
                    Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "Something Went Wrong Please Try Again" );
                }
                Log.d("RRR","volleyError  "+volleyError);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<String, String>();
                return params;
            }
        };

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void update_user_types(final Subscription _subscription,Double amt) {




        if (Utils.DETECT_INTERNET_CONNECTION(PaymentActivity.this)) {


            Utils.show_Dialog(PaymentActivity.this,"Updating User ..");
            String Update_User_Url = Webservices.UPDATE_USER_payment(getApplicationContext());
//            String Update_User_Url = Webservices.UPDATE_USER_TYPES(getApplicationContext());
            //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_User_Url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {


/*

                                    JSONObject jsonObject= response.getJSONObject("user");

                                    _User.set_User_Id(jsonObject.getString("uid"));
                                    _User.set_User_Name(jsonObject.getString("username"));
                                    _User.set_Email(jsonObject.getString("email"));
                                    _User.set_Phone(jsonObject.getString("phone_number"));

                                    if(jsonObject.getString("is_safe").equals("1"))
                                    {

                                        _User.set_Safe(true);
                                    }else {

                                        _User.set_Safe(false);
                                    }
                                    if (jsonObject.getString("is_login").equals("1")){
                                        _User.set_Login(true);
                                    }else {
                                        _User.set_Login(false);
                                    }

                                    _User.set_Lat(jsonObject.getString("lat"));
                                    _User.set_Lng(jsonObject.getString("lon"));
                                    _User.set_User_Type(jsonObject.getString("usertype_id"));
                                    _User.set_Height(jsonObject.getString("height"));
                                    _User.set_Weight(jsonObject.getString("weight"));

                                    _User.set_Img_Path(jsonObject.getString("img_path"));



                                    Utils.SAVE_USER_TO_SHAREDPREFS(getApplicationContext(), _User);
                                    Utils.IS_PREMIUM_PURCHASED=true;
*/
                                    Utils.TOAST_SUCCESS_RESPONSE(PaymentActivity.this, "Premium Purchased Successfully");





                                } else {
                                    Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "" + response.getString("msg"));
                                }
                                Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Utils.hide_dialog();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            // Utils.hide_dialog();

                            //Showing toast
                            //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();
                    params.put("useruniqueid",_User.get_User_Id());
                    params.put("amount", ""+(_subscription.getPlan().getAmount() / 1));
                    params.put("currency", _subscription.getPlan().getCurrency().toString());
                    params.put("CusSubscriptionID", _subscription.getId());
                    Log.e("useruniqueid",_User.get_User_Id());
                    Log.e("amount", _subscription.getPlan().getAmount().toString());
                    Log.e("currency", _subscription.getPlan().getCurrency().toString());
                    Log.e("CusSubscriptionID", _subscription.getId());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);



        } else {
            Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "No Internet Connection.");
        }




        Utils.hide_dialog();





    }


    /*
    public void chargeCustomer(final String cust_id) {
        //Toast.makeText(getApplicationContext(), "Incharge" + cust_id, Toast.LENGTH_SHORT).show();

        Log.d("rathod","charge cust id:= "+cust_id);

        final Map<String, Object> chargeParams = new HashMap<String, Object>();
        chargeParams.put("amount",4000);
        chargeParams.put("currency","usd");
        chargeParams.put("customer",cust_id);


        new AsyncTask<Void, Void, Void>() {

            Charge charge;
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //apiKey = "sk_test_NttCGIiu66rF64MR6aAtpDxa";
                    charge = Charge.create(chargeParams);;


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.d("Rathod","error in Charge");
                }
                return null;
            }

            protected void onPostExecute(Void result) {
                if (charge!=null){
                    CreatePlan(cust_id);
                    Log.d("rathod","Charge Successfully...");
                    Log.d("rathod","Charge id:="+charge.getId());
                    //  Toast.makeText(PaymentActivity.this, "Card Charged : " + charge.getCreated() + "\nPaid : " +charge.getId(), Toast.LENGTH_LONG).show();
                }


            };

        }.execute();


    }
*/

    public void saveCreditCard(PaymentForm form) {

    }



    private void get_Plan_List_from_Stripe() {

        final List<String> PlanName=new ArrayList<>();
        final List<String> Planamount=new ArrayList<>();

        final Map<String, Object> planParams = new HashMap<String, Object>();
        planParams.put("limit", 10);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {

                    PlanCollection list=Plan.list(planParams);
                    PlanList=list.getData();

                    for (Plan plan:PlanList){

                        PlanName.add(plan.getName());
                        Planamount.add(""+plan.getAmount());
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.d("rathod","error in get Plan List");

                }
                return null;
            }

            protected void onPostExecute(Void result) {
                if (PlanList!=null&&PlanList.size()>0){
                    ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(PaymentActivity.this, R.layout.spn_txt,PlanName);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    _SpnPlan.setAdapter(spinnerArrayAdapter);
                    _SpnPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                            Double Total=Double.parseDouble(Planamount.get(position))/100;
                            _Pay.setText(Total+" "+PlanList.get(position).getCurrency()+" Pay");
                             Selectedplan=PlanList.get(position);
                            }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });
                }

            };

        }.execute();











    }

 /*   private void send_Tocken_To_Parse(Token token) {



        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("customerId", token.getId());


        ParseCloud.callFunctionInBackground("pay", params, new FunctionCallback<String>() {


            @Override
            public void done(String response, com.parse.ParseException e) {

                if (e == null) {
                    Log.e("Response", "no exceptions! " + response.toString());


                    if (response.toString().contains("Purchase made")) {
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        currentUser.put("premium_purchased", true);
                        currentUser.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {

                                    Utils.IS_PREMIUM_PURCHASED = true;
                                    Utils.TOAST_SUCCESS_RESPONSE(PaymentActivity.this, "Premium Purchased Successfully");
                                    if (Utils.IS_PREMIUM_PURCHASED) {
                                        _Pay.setText("Premium Purchased");
                                        _Pay.setEnabled(false);
                                        AppEventsLogger logger = AppEventsLogger.newLogger(PaymentActivity.this);
                                        logger.logPurchase(BigDecimal.valueOf(4.32), Currency.getInstance("USD"));
                                    }
                                } else {

                                    Utils.TOAST_SUCCESS_RESPONSE(PaymentActivity.this, "Try Again. Something went wrong");
                                    Log.e("", "update user error : " + e.toString());

                                }
                            }
                        });
                    }


                    //   Toast.makeText(RegisterScreen.this,  "no exceptions! " + response.toString(), Toast.LENGTH_LONG).show();

                    // Code sent successfully you have to wait it or ask the user to enter the code for verification
                } else {
                    Log.e("Response", "Exception: " + e);

                }

            }
        });
    }*/

    private void startProgress() {
        progressFragment.show(getSupportFragmentManager(), "progress");
    }

    private void finishProgress() {
        progressFragment.dismiss();
    }

    private void handleError(String error) {
        DialogFragment fragment = ErrorDialogFragment.newInstance(R.string.validationErrors, error);
        fragment.show(getSupportFragmentManager(), "error");
    }

//    private TokenList getTokenList() {
//        return (TokenList)(getSupportFragmentManager().findFragmentById(R.id.token_list));
//    }



    private void getCurrentSubscription() {


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.GETPLAN(getApplicationContext());
        StringRequest Subscription = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {

                           JSONObject res=new JSONObject(response);


                          //  JSONObject subscribed_plans= res.getJSONObject("subscribed_plans"); //new JSONObject(res.optString("plans"));
                            JSONArray data = res.getJSONArray("subscribed_plans");

                            if(data.length() != 0)
                            currentplan = data.optJSONObject(0).optString("plan_title","")+" Expires in "+ data.optJSONObject(0).optString("validity_text","") ;
                            if(data.length() != 0)
                            _currentplan.setText(currentplan);

                            JSONArray dataof = res.optJSONArray("offer_subscriptions");

                            if(dataof.length() != 0)
                            currentoff = dataof.optJSONObject(0).optString("offer_title","")+" For "+ dataof.optJSONObject(0).optString("validity_text","") ;

                            if(dataof.length() != 0)
                            _currentoffer.setText(currentoff);

                            if(dataof.length() != 0) {
                                _currentoffer.setVisibility(View.VISIBLE);
                                _currentoffer.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        try {
                                            custome_message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(PaymentActivity.this);
                                            fb();
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });



                            }
                            else{



                            }


                        } catch (JSONException e) {

                            Log.d("rathod","JSON EXCEPTION :: "+e.getMessage());
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        } catch (Exception e){}

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("rathod","onErrorResponse :: "+error.getMessage());
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("useruniqueID",_User.get_User_Id());

                Log.d("USR","_User.get_User_Id() :: "+_User.get_User_Id());
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(Subscription,
                Utils.VOLEY_TAG);

    }


    private void sharedofferSubscription() {

        String url = Webservices.SHAREOFFER(getApplicationContext());
        Log.e("shareposturl" , url);
        StringRequest Subscription = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);
                        startActivity(new Intent(getApplicationContext(),PaymentActivity.class));
                        finish();
                       Log.e("share activate responce",""+response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("rathod","onErrorResponse :: "+error.getMessage());
                Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this,"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("useruniqueID",_User.get_User_Id());

                Log.d("USR","_User.get_User_Id() :: "+_User.get_User_Id());
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(Subscription,
                Utils.VOLEY_TAG);

    }



    public  void fb() throws UnsupportedEncodingException {

                if (mSimpleFacebook == null) {
                    mSimpleFacebook = SimpleFacebook.getInstance(PaymentActivity.this);
                }

                if (mSimpleFacebook.isLogin()) {

                    Log.e("user", "user is login");



                    Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "posting on facebook.");

                    post_On_Facebook();

                    /*
                    Bundle parameters = new Bundle();
                    parameters.putString("message", "this is a test");// the message to post to the wall
                    parameters.putString("caption", "zeus caphon");// the message to post to the wall
                    parameters.putString("picture", "http://www.zeusapp.me/images/zeusapp.png");// the message to post to the wall
                    parameters.putString("name", "Zeus Title");// the message to post to the wall
                    parameters.putString("link", textForFb(msg));
                    */

                } else {
                    Log.e("user", "user is not login");
                    open_login();
                }



    }

    public void open_login() {
        final OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onFail(String reason) {

                Log.w("failed", "Failed to login");
            }

            @Override
            public void onException(Throwable throwable) {

                Log.e("exception", "Bad thing happened", throwable);
            }

            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // change the state of the button or do whatever you want


                try {
                    post_On_Facebook();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e(" invalid url"," invalid encoding");
                }
            }

            @Override
            public void onCancel() {
                Log.w("onCancel", "onCancel");
            }

        };

        mSimpleFacebook.login(onLoginListener);

    }

    public void post_On_Facebook() throws UnsupportedEncodingException {
        fun();



        //Toast.makeText(this, "posting 1", Toast.LENGTH_SHORT).show();
/*
        Feed feed = new Feed.Builder()
                .setName("Zeus Alert, I'm In Trouble!")
                .setCaption("Click to see his location")
                *//*.setDescription("This person is in trouble and needs your immediate help!")*//*
                .setDescription("This person is in trouble and needs your immediate help!")
                .setMessage("")
                .setPicture("http://www.zeusapp.me/images/zeusapp.png")
                *//*.setPicture("http://www.zeusapp.me/images/zeusapp.png")*//*
                .setLink(textForFb(msg))
               *//* .setLink("" + textForFb())*//*

                //.setPrivacy(Priva)
                .build();*/

        Feed feed = new Feed.Builder()
                .setMessage("Alert your Neighbours in case you are in trouble")
                .setName("Zeus Alert")
                .setCaption("Zeus Alert, I'm In Trouble!")
                .setDescription("Android and IOS app available.")
                .setPicture("http://zeusalert.com/zeusic.jpeg")
                .setLink("http://zeusalert.com")
                .build();

        Log.e("updating", "updating : ");

        Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Posting Status On Facebook");




       /* mSimpleFacebook.publish(feed, true, new OnPublishListener() {

            public void onException(Throwable throwable) {

                Log.e("ecxception", "throwable : " + throwable.toString());
                //      hide_Dialog();
                  Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "onException : " + throwable.toString());

            }

            @Override
            public void onFail(String reason) {
                //      hide_Dialog();
                Log.e("ecxception", "fail reason : " + reason.toString());
                  Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "onFail : " + reason.toString());
                Toast.makeText(PaymentActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onThinking() {
                //     show_Dialog("Posting On Facebook.", getActivity());
                Toast.makeText(PaymentActivity.this, "thinking", Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onComplete(String response) {

                Toast.makeText(PaymentActivity.this, "posted", Toast.LENGTH_SHORT).show();

                Log.e("ecxception", "success : " + response.toString());
                Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Status posted on Facebook");


                Tracker t = ((ZeusApplication) PaymentActivity.this.getApplication()).getTracker(ZeusApplication.TrackerName.APP_TRACKER);
                t.setScreenName("Facebook Status Published");
                t.send(new HitBuilders.AppViewBuilder().build());

                super.onComplete(response);
            }
        });
*/



       /* SimpleFacebook.getInstance().publish(feed, new OnPublishListener() {
            @Override
            public void onException(Throwable throwable) {

                Log.e("ecxception", "throwable : " + throwable.toString());
                //      hide_Dialog();
                //  Utils.TOAST_ERROR_RESPONSE(getActivity(), "onException : " + throwable.toString());
            }

            @Override
            public void onFail(String reason) {
                //      hide_Dialog();
                Log.e("ecxception", "fail reason : " + reason.toString());
               //  Utils.TOAST_ERROR_RESPONSE(getActivity(), "onFail : " + reason.toString());

            }

            @Override
            public void onThinking() {
                //     show_Dialog("Posting On Facebook.", getActivity());

            }

            @Override
            public void onComplete(String response) {
                //       hide_Dialog();
                Log.e("ecxception", "success : " + response.toString());
                Utils.TOAST_ERROR_RESPONSE(getActivity(), "Status posted on Facebook");


                Tracker t = ((ZeusApplication) getActivity().getApplication()).getTracker(ZeusApplication.TrackerName.APP_TRACKER);
                t.setScreenName("Facebook Status Published");
                t.send(new HitBuilders.AppViewBuilder().build());


            }
        });*/
    }

public void fun(){
    Log.d("Tests", "Testing graph API wall post");
    ShareLinkContent content = new ShareLinkContent.Builder()
            .setContentUrl(Uri.parse("http://zeusalert.com"))
            .build();
    ShareDialog shareDialog;
    shareDialog = new ShareDialog(this);

    AppEventsLogger.activateApp(this);
    callbackManager = CallbackManager.Factory.create();
    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            if((!(result.getPostId().toString().equals(null))) || (!result.getPostId().toString().equals("")))
            sharedofferSubscription();
        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onError(FacebookException error) {

        }
    });

    shareDialog.show(content);

   /* ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
            .putString("og:type", "fitness.course")
            .putString("og:title", "Sample Course")
            .putString("og:description", "This is a sample course.")
            .putInt("fitness:duration:value", 100)
            .putString("fitness:duration:units", "s")
            .putInt("fitness:distance:value", 12)
            .putString("fitness:distance:units", "km")
            .putInt("fitness:speed:value", 5)
            .putString("fitness:speed:units", "m/s")
            .build();
    ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
            .setActionType("fitness.runs")
            .putObject("fitness:course", object)
            .build();
    ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
            .setPreviewPropertyName("fitness:course")
            .setAction(action)
            .build();
    ShareApi share = new ShareApi(content);
    if(share.canShare()){
        Toast.makeText(this, "true", Toast.LENGTH_SHORT).show();
        share.share(content, null);
        share.share(content, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(PaymentActivity.this, "succes on automatic", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(PaymentActivity.this, "canceled on automatic", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(PaymentActivity.this, "error on automatic "+error.toString(), Toast.LENGTH_SHORT).show();
                Log.e("error on automatic ",error.toString());
            }
        });
    }
    else {
        Toast.makeText(this, "false", Toast.LENGTH_SHORT).show();
       // shareDialog.show(content);

    }

*/


    /*
    * new try
    * */

    //sharebuttom.setShareContent(content);
    sharebuttom.setVisibility(View.GONE);

}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }catch (NullPointerException e){
            Utils.TOAST_ERROR_RESPONSE(PaymentActivity.this, "Enable Fb in setting & login to Fb before Sharing.");
        }
    }

}
