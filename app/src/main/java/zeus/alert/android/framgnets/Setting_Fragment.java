package zeus.alert.android.framgnets;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import zeus.alert.android.R;

import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.models.Custome_Message_And_Access_Token;
import zeus.alert.android.utils.Utils;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnNewPermissionsListener;

import java.security.MessageDigest;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Setting_Fragment extends Fragment {

    @InjectView(R.id.custome_Message_LL)
    LinearLayout _Custome_Message_LL;

    @InjectView(R.id.custome_Message)
    TextView _Custome_Message;


    @InjectView(R.id.emergency_Message)
    TextView _Emergency_Contacts;

    @InjectView(R.id.security_setting_LL)
    RelativeLayout _Security_Setting_LL;

    @InjectView(R.id.chkWindows)
    CheckBox _Fb_Check_Box;

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;


    @InjectView(R.id.about)
    TextView _About;

    @InjectView(R.id.about_Rl)
    RelativeLayout _About_Rl;


    private String _Custome_Message_St = "";
    Custome_Message_And_Access_Token custome_message;

    private SimpleFacebook mSimpleFacebook;

    public static Setting_Fragment newInstance() {

        Setting_Fragment f = new Setting_Fragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSimpleFacebook = SimpleFacebook.getInstance();




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        ButterKnife.inject(this, view);
        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseListSample.mMenuDrawer.toggleMenu();
            }
        });


        ImageView fb = (ImageView) view.findViewById(R.id.facebook);
        ImageView tw = (ImageView) view.findViewById(R.id.twitter);


        _Custome_Message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_saved_Message();
                show_emial_Dialog_Send_Email();
            }
        });

        _Custome_Message_LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_saved_Message();
                show_emial_Dialog_Send_Email();
            }
        });


        get_saved_Message();
//        if (App_Home_Page.Is_Premium_Clicked) {
//            if (!BaseListSample._Is_Premium_Purchased) {
//                Utils.SHOW_PURCHASE_DIALOG(getActivity());
//                App_Home_Page.Is_Premium_Clicked = false;
//            }
//
//        }


        custome_message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());
        if (custome_message == null) {
            custome_message = new Custome_Message_And_Access_Token();
        } else {
            if (custome_message.is_Fb_Checked()) {
                _Fb_Check_Box.setChecked(true);
            } else {
                _Fb_Check_Box.setChecked(false);
            }
        }
        _Fb_Check_Box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                if (b) {
                    open_login();
                } else {
                    custome_message.set_Fb_Checked(false);
                }

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);
            }
        });


        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openchrome("https://www.facebook.com/ZeusAlert");
            }
        });
        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openchrome("https://twitter.com/ZeusAlert");
            }
        });


        _About.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openchrome("http://zeusapp.me/about");

            }
        });

        _About_Rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openchrome("http://zeusapp.me/about");

            }
        });


        return view;


    }

    public void openchrome(String url) {
        String urlString = url;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intent.setPackage(null);
            startActivity(intent);
        }
    }

    public void get_saved_Message() {
        custome_message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());

        if (custome_message != null) {
            _Custome_Message_St = custome_message.get_Custome_Message();
        }
    }


    public void show_emial_Dialog_Send_Email() {
        final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.dialog_edit_text, null);
        final EditText _Edit_text = (EditText) convertView.findViewById(R.id.dialog_ET);
        _Edit_text.requestFocus();
        _Edit_text.setFocusable(true);
        _Edit_text.setFocusableInTouchMode(true);

        if (_Custome_Message_St == null || _Custome_Message.equals("")) {
            _Edit_text.setText("");
        } else {
            _Edit_text.setText("" + _Custome_Message_St);
        }

        InputMethodManager keyboard = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(_Edit_text, 0);


        mMaterialDialog.setView(convertView);
        mMaterialDialog.setPositiveButton(getActivity().getResources().getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String _Custome_Message = _Edit_text.getText().toString();


                custome_message = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());
                if (custome_message == null) {
                    custome_message = new Custome_Message_And_Access_Token();
                }


                custome_message.set_Custome_Message(_Custome_Message);
                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);
                mMaterialDialog.dismiss();

            }
        });

        mMaterialDialog.setNegativeButton(getActivity().getResources().getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mMaterialDialog.dismiss();

            }
        });

        mMaterialDialog.show();
    }

    public void open_login() {
        final OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onFail(String reason) {

                Log.w("failed", "Failed to login");

                custome_message.set_Fb_Checked(false);
                _Fb_Check_Box.setChecked(false);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);

            }

            @Override
            public void onException(Throwable throwable) {

                Log.e("exception", "Bad thing happened", throwable);

                custome_message.set_Fb_Checked(false);
                _Fb_Check_Box.setChecked(false);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);

            }

            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // change the state of the button or do whatever you want
                Log.e("zeus", "login succesfull setting fragment : ");
               get_Permisssion();
                custome_message.set_Fb_Checked(true);
                _Fb_Check_Box.setChecked(true);
            }

            @Override
            public void onCancel() {
                Log.w("onCancel", "onCancel");

                custome_message.set_Fb_Checked(false);
                _Fb_Check_Box.setChecked(false);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);

            }

        };

        mSimpleFacebook.login(onLoginListener);

    }

    public void get_Permisssion() {
        Permission[] permissions = new Permission[]{
                Permission.PUBLISH_ACTION
        };

        OnNewPermissionsListener onNewPermissionsListener = new OnNewPermissionsListener() {

            @Override
            public void onSuccess(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // updated access token

                Log.e("zeus", "permisison granted for action : ");

             //   custome_message.set_Fb_Checked(true);
               // _Fb_Check_Box.setChecked(true);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);
            }

            @Override
            public void onCancel() {
                // user canceled the dialog
             //   custome_message.set_Fb_Checked(false);
               // _Fb_Check_Box.setChecked(false);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);

                Log.w("onCancel", "onCancel get permission");
            }

            @Override
            public void onFail(String reason) {
                // failed
               // custome_message.set_Fb_Checked(false);
              //  _Fb_Check_Box.setChecked(false);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);

                Log.e("failed", "Failed to get permission="+reason);
            }

            @Override
            public void onException(Throwable throwable) {
                // exception from facebook
              //  custome_message.set_Fb_Checked(false);
                //_Fb_Check_Box.setChecked(false);

                Utils.SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(getActivity(), custome_message);

                Log.e("exception", "Bad get permission", throwable);
            }

        };

        mSimpleFacebook.requestNewPermissions(permissions, onNewPermissionsListener);
    }

    @Override
    public void onResume() {
        super.onResume();


        Custome_Message_And_Access_Token customeMessageAndAccessToken = Utils.GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(getActivity());

        if (_Fb_Check_Box != null) {
            if (customeMessageAndAccessToken != null) {
                if (customeMessageAndAccessToken.is_Fb_Checked()) {
                    _Fb_Check_Box.setChecked(true);
                } else {
                    _Fb_Check_Box.setChecked(false);
                }
            }
        }
    }
}
