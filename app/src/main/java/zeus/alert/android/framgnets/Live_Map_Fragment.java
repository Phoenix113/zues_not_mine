package zeus.alert.android.framgnets;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import zeus.alert.android.R;

import zeus.alert.android.activities.App_Home_Page;
import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.activities.Track_User_Activity;
import zeus.alert.android.activities.ZeusApplication;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.SolarCalculations;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Live_Map_Fragment extends Fragment implements OnMapReadyCallback {


    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    @InjectView(R.id.track_and_help_now)
    Button _Track_And_Help_Now;

    @InjectView(R.id.user_information_layout)
    LinearLayout _User_Info_Layout;


    @InjectView(R.id.username)
    TextView _UserName;

    @InjectView(R.id.height)
    TextView _Height;

    @InjectView(R.id.weight)
    TextView _Weight;

    @InjectView(R.id.distance)
    TextView _Distance;

    @InjectView(R.id.item_image)
    CircleImageView _User_Image;


    private GoogleMap mMap;

    String _Total_Distance;
    private HashMap<Marker, Integer> mHashMap = new HashMap<Marker, Integer>();
    private int position = -1;
    String username = "";
    String weight = "";
    String height = "";
    String distance = "";
    Handler mHandler = new Handler();
    private Runnable mRunnable;
    private User _User = null;
    public App_Home_Page activity;

    private Context context;


    String dis = "1";
    JsonObjectRequest register_User_Request_To_Server;


    ArrayList<User> _User_In_Trouble;

    public static Live_Map_Fragment newInstance() {

        Live_Map_Fragment f = new Live_Map_Fragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_live_map, container, false);
        ButterKnife.inject(this, view);
        _User_In_Trouble = new ArrayList<User>();
        Utils.GET_LAST_KNOWN_LOCATION(getActivity());

        _User = Utils.GET_USER_FROM_SHARED_PREFS(getActivity());

        context = getActivity();

      /*  if (!Utils.CHECK_GPS(getActivity())) {
            Utils.OPEN_GPS_ACTIVITY(getActivity());

        }*/

        /*try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            // TODO handle this situation

            Log.e("", "exception in map initilizer : " + e.toString());
        }*/


        if (Utils.IS_COMING_FROM_SMS_RECEIVER) {

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.addRule(RelativeLayout.BELOW, _User_Info_Layout.getId());


            // show_data_of_User_That_Is_In_Trouble();
            Utils.IS_COMING_FROM_SMS_RECEIVER = false;
        } else {
            _Track_And_Help_Now.setVisibility(View.GONE);
            _User_Info_Layout.setVisibility(View.GONE);

            if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {


                ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
// This schedule a runnable task every 2 minutes
                scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        Log.d("ttt", "tttt");

                        try {
                            get_users_those_have_same_city_and_are_In_Danger();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 0, 2, TimeUnit.SECONDS);


            } else {
                Utils.TOAST_SUCCESS_RESPONSE(getActivity(), "No Internet Connection");
            }
            try {
                SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                supportMapFragment.getMapAsync(this);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "eroor", Toast.LENGTH_SHORT).show();
            }


        }

        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseListSample.mMenuDrawer.toggleMenu();
            }
        });

        _Track_And_Help_Now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), Track_User_Activity.class);
                intent.putExtra("username", username);
                intent.putExtra("height", height);
                intent.putExtra("weight", weight);
                intent.putExtra("distance", distance);
                startActivity(intent);

            }
        });

        return view;


    }

/*    private void show_data_of_User_That_Is_In_Trouble() {


        if (Utils._Parse_User_In_Trouble != null) {
            username = Utils._Parse_User_In_Trouble.getString("name");
            height = Utils._Parse_User_In_Trouble.getString("height");
            weight = Utils._Parse_User_In_Trouble.getString("weight");
            distance = Utils._Parse_User_In_Trouble_Distance;
            _UserName.setText("Name : " + Utils._Parse_User_In_Trouble.getString("name"));

            if (height == null) {
                _Height.setText("Height : Not Given");
            } else {

                _Height.setText("Height : " + height);

            }
            if (weight == null) {
                _Weight.setText("Weight : Not Given");
            } else {
                _Weight.setText("Weight : " + weight);
            }


            _Distance.setText("Distance : " + Utils._Parse_User_In_Trouble_Distance);


            if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {

            } else {
                Utils.TOAST_ERROR_RESPONSE(getActivity(), "Picture Can't load. No Internet Connection.");
            }
            //  get_User_Image("" + Utils._Parse_User_In_Trouble.getUsername());





                if (mMap != null) {

                    mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Utils._Parse_User_In_Trouble_Lat
                                    , Utils._Parse_User_In_Trouble_Lng), 14.0f));

                        }
                    });
                }


        }
    }*/

    private void setUpMapIfNeeded() {


        if (mMap != null) {

            Calendar cal = Calendar.getInstance();

            double lat = 0;
            double lon = 0;

            if (_User.get_Lat() != null && _User.get_Lng() != null) {

                lat = Double.parseDouble(_User.get_Lat());
                lon = Double.parseDouble(_User.get_Lng());
            }//39.7446154,-88.684969

            double sunHeight = SolarCalculations.CalculateSunHeight(lat, lon, cal);

            if (sunHeight > 0) {//daylight mode
                // this.recreate();
                Log.d("Mode", "Day Light mode");

                try {

                    boolean success =
                            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(),
                                    R.raw.default_style));


                    if (!success) {
                        Log.e("Sid", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("Sid", "Can't find style. Error: ", e);
                }
            } else if (sunHeight < 0 && sunHeight >= -6) {//civil dusk
                Log.d("Mode", "Civil dusk mode");
                //   this.recreate();
            } else if (sunHeight < -6) {//night mode

                try {
                    boolean success = mMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    getActivity(), R.raw.style_json));
                    // MapStyleOptions.loadRawResourceStyle(getActivity(),MapStyleOptions.CONTENTS_FILE_DESCRIPTOR);

                    if (!success) {
                        Log.e("Sid", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("Sid", "Can't find style. Error: ", e);
                }

                Log.d("Mode", "Night mode");
                // this.recreate();
            }


            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {

                    _User = Utils.GET_USER_FROM_SHARED_PREFS(getActivity());


                    Log.d("shiv", "" + _User.get_User_Name());

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Utils._LATITUDE
                            , Utils._LONGITUDE), 14.0f));
                    _User.set_Lat("" + Utils._LATITUDE);
                    _User.set_Lng("" + Utils._LONGITUDE);
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(_User.get_Lat()), Double.parseDouble(_User.get_Lng())))
                            .title(_User.get_User_Name())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.mar_1)));

                    mHashMap.put(marker, 0);
                    _User_In_Trouble.add(_User);


                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {


                @Override
                public boolean onMarkerClick(Marker marker) {

                    if (mHashMap.size() > 0) {
                        Log.d("RRR", "marker :: " + marker);
                        try {
                            if (mHashMap.get(marker) != null) {
                                position = mHashMap.get(marker);
                                marker.showInfoWindow();
                                Log.d("RRR", " marker hash map is not null :: ");
                            }
                        } catch (Exception e) {
                            Log.d("RRR", "Exception  " + e.getMessage());
                        }

                    }
                    return false;
                }

            });

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                // Use default InfoWindow frame
                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                // Defines the contents of the InfoWindow
                @Override
                public View getInfoContents(Marker arg0) {


                    // Getting view from the layout file info_window_layout
                    View v = getActivity().getLayoutInflater().inflate(R.layout.windowlayout, null);
                    // Log.d("pos",""+arg0.getPosition());

                    if (position >= 0) {

                        User user = _User_In_Trouble.get(position);
                        // Getting reference to the TextView to set latitude
                        TextView user_name = (TextView) v.findViewById(R.id.user_Name);

                        // Getting reference to the TextView to set longitude
                        TextView user_Height = (TextView) v.findViewById(R.id.user_Height);
                        TextView user_weight = (TextView) v.findViewById(R.id.user_weight);
                        TextView user_Distance = (TextView) v.findViewById(R.id.user_distance);
                        CircleImageView circleImageView = (CircleImageView) v.findViewById(R.id.item_image);

                        Log.d("shiv", "" + user.get_User_Name());
                        if (user != null) {


                            user_name.setText("Name : " + user.get_User_Name());
                            user_Height.setText("Height : " + user.get_Height());
                            user_weight.setText("Weight : " + user.get_Weight());


                            try {
                                String dis = find_Total_Distance_Between_Start_And_End_Location(user.get_Lat(), user.get_Lng(), "" + Utils._LATITUDE, "" + Utils._LONGITUDE);
                                if (!dis.isEmpty() || !dis.equals("")) {
                                    user_Distance.setText("Distance : " + dis + " Miles");
                                }
                            } catch (Exception e) {
                                user_Distance.setText("Distance : " + 0.0 + " Miles");
                            }


                            if (user.get_Img_Path() != null && !user.get_Img_Path().isEmpty() && !user.get_Img_Path().trim().equals("") && !user.get_Img_Path().equalsIgnoreCase("null")) {
                                Log.d("shiv", user.get_Img_Path());

                                Picasso.with(getActivity())
                                        .load(user.get_Img_Path())
                                        .error(R.drawable.ph_1)
                                        .resize(100, 100).centerCrop()
                                        .into(circleImageView);


                            } else {
                                circleImageView.setImageResource(R.drawable.ph_1);
                            }
                        }
                    }
                    // Returning the view containing InfoWindow contents
                    return v;
                }
            });
        }
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
        //  ft.detach(this).attach(this).commit();
    }

    /*  private void get_users_those_have_same_city_and_are_In_Danger() {
          try{
          if (!Utils.CHECK_GPS(getActivity())) {
              Utils.OPEN_GPS_ACTIVITY(getActivity());
          }

          if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {
            Utils.show_Dialog(getActivity(), "Updating User Status");
              //    String Update_User_Url = Webservices.UPDATE_USER_STATUS(getActivity());
              //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();
              String url = Webservices.LIVE_MAP_USERS(getActivity(), "" + Utils._LATITUDE, "" + Utils._LONGITUDE, _User.get_Phone());

              Log.d("rathod", url);


            *//*  StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {

                                } else {
                                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                                }
                                Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Utils.hide_dialog();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            // Utils.hide_dialog();

                            //Showing toast
                            //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);*//*



        } else {
            Utils.TOAST_ERROR_RESPONSE(getActivity(), "No Internet Connection.");
        }
        }catch(Exception e){Log.d("map",""+e);};

    }
*/
    private void get_users_those_have_same_city_and_are_In_Danger() {
        try {
            mMap.clear();
            mHashMap.clear();
        } catch (Exception e) {
        }


        // if (!Utils.GET_USER_CITY_NAME(getContext()).equals("")) {
        if ((Utils._LATITUDE != null) || (Utils._LATITUDE != 0.0)) {

            if (_User.get_User_Type().equals("1")) {
                dis = "3";
            } else {
                dis = "0.5";
            }

            String url = Webservices.LIVE_MAP_USERS(getContext(), "" + Utils._LATITUDE, "" + Utils._LONGITUDE, _User.get_Phone().trim().replace(" ", ""), dis, _User.get_User_Id());
            Log.d("", url);
            if (url == null) return;

            register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    Log.e("zeus", "response : " + response);
                    Log.d("RRR", "near by user api call" + response);

                    try {

                        boolean _Status = response.getBoolean("status");

                        // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                        if (_Status) {

                            JSONArray jsonArray = response.getJSONArray("0");


                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                User user = new User();


                                String Hst;
                                try {

                                    if (jsonObject.getString("height").length() > 2) {
                                        Hst = "" + jsonObject.getString("height").charAt(0) + " ft " + jsonObject.getString("height").charAt(1) + jsonObject.getString("height").charAt(2) + " inch ";

                                    } else {
                                        Hst = jsonObject.getString("height");
                                    }

                                } catch (Exception e) {
                                    Hst = jsonObject.getString("height");
                                }

                                Log.d("rathod", Hst);
                                Log.d("rathod", jsonObject.getString("username"));


                                user.set_Lng(jsonObject.getString("lon"));
                                user.set_Lat(jsonObject.getString("lat"));
                                user.set_User_Name(jsonObject.getString("username"));
                                user.set_Phone(jsonObject.getString("phone_number"));
                                user.set_Height(Hst);
                                user.set_Weight(jsonObject.getString("weight") + " lb");
                                user.set_Distance(jsonObject.getString("distance"));
                                user.set_Img_Path(jsonObject.getString("img_path"));


                                /*
                                * Sid some changes's here
                                * */
                                //   if(jsonObject.getInt("is_safe") == 1){
                                ADD_MARKER_ON_MAP(jsonObject.getString("username"),
                                        Double.parseDouble(jsonObject.getString("lat")),
                                        Double.parseDouble(jsonObject.getString("lon")), i + 1);
                                // }

                                _User_In_Trouble.add(user);


                            }
                        }


                    } catch (Exception e) {

                        Log.e("zues", "exception in register user : " + e.toString());
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    Log.e("jeeeni", "exception in register user activity while registring user : ");
                    Log.e("jeeeni", "error is : " + error.toString());
                    //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                    //      startActivity(go_To_App_Home_Page);


                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }

            };

            // Adding request to request queue
            ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server, Utils.VOLEY_TAG);
        } else {

            Utils.TOAST_ERROR_RESPONSE(getActivity(), "Gps is not working properly. Turn it on and come back");

        }


    }

    private void ADD_MARKER_ON_MAP(final String name, final double lat, final double lng, final int position) {


        Log.d("map", name);
        Log.d("map", "lat=" + lat + "lon=" + lng);

        if (mMap != null) {


            Log.d("map", name);
            Log.d("map", "lat=" + lat + "lon=" + lng);
            //Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("Name : " + name));
            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("Name : " + name).icon(BitmapDescriptorFactory.fromResource(R.drawable.mar_2)));
            //marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_marker));

            mHashMap.put(marker, position);


        }
    }


    private String find_Total_Distance_Between_Start_And_End_Location(String lat_a, String lng_a, String lat_b, String lng_b) {


        String url = "http://maps.google.com/maps/api/directions/json?origin=" + lat_a + "," +
                lng_a + "&destination=" + lat_b + "," +
                lng_b + "&sensor=false&units=metric";
        url = url.replace(" ", "%20");
        Log.e("ZEUS", "diatance url : " + url);

        JsonObjectRequest _CALCUALTE_DISTANCE = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {


                    if (!response.isNull("status")) {
                        if (response.getString("status").equals("OK")) {


                            JSONArray _Routes = response.getJSONArray("routes");

                            for (int i = 0; i < _Routes.length(); i++) {
                                JSONObject _Route = _Routes.getJSONObject(i);


                                JSONArray _Legs = _Route.getJSONArray("legs");

                                for (int j = 0; j < _Legs.length(); j++) {

                                    JSONObject _Diatance = _Legs.getJSONObject(i);

                                    JSONObject _Distance_Object = _Diatance.getJSONObject("distance");
                                    _Total_Distance = _Distance_Object.getString("text");

                                    Log.e("zeus", "6 mile radius contact distance : " + _Total_Distance);
                                    boolean is_In_Km = false;

                                    if (_Total_Distance.contains("km")) {
                                        is_In_Km = true;
                                        _Total_Distance = _Total_Distance.replace("km", "").replace(" ", "");

                                    } else if (_Total_Distance.contains("m")) {

                                        _Total_Distance = _Total_Distance.replace("m", "").replace(" ", "");

                                    }


                                    Float diatance = Float.parseFloat(_Total_Distance);


                                    if (is_In_Km) {
                                        diatance = diatance * 1000;

                                    }

                                    //  Log.e("zeus", "6 mile radius contact emails : " + parseObjects.get(i).getString("username"));
                                    //   Log.e("zeus", "6 mile radius contact distance : " + diatance);

                                    if (diatance < 804.672f) {

                                        //lstcontacts.add(parseObject);
                                        Log.e("zeus", "total distance is : " + _Total_Distance);


                                    }

                                }


                            }


                        } else {
                            Utils.TOAST_ERROR_RESPONSE(getActivity(), "Error Please Try Again");
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(_CALCUALTE_DISTANCE,
                ZeusApplication.TAG);
        Log.d("dis", _Total_Distance);
        return _Total_Distance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        setUpMapIfNeeded();


                    }
                });
            }
        }).start();
    }

}
