package zeus.alert.android.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import zeus.alert.android.R;
import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.billing.IabHelper;
import zeus.alert.android.billing.IabResult;
import zeus.alert.android.billing.Inventory;
import zeus.alert.android.billing.Purchase;
import zeus.alert.android.detectinternetconnection.ConnectionDetector;
import zeus.alert.android.models.Contacts;
import zeus.alert.android.models.Credit_Card_Crecentials;
import zeus.alert.android.models.Custome_Message_And_Access_Token;
import zeus.alert.android.models.Tutorial;
import zeus.alert.android.models.User;
import zeus.alert.android.models.UserPassword;
import com.google.gson.Gson;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;


import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Moubeen Warar on 11/7/2015.
 */
public class Utils {


    public static User _USER = null;
    private static ConnectionDetector cd;

    private static Display _Display;
    private static int screenTotalWidth;
    private static int screenTotalHeight;
    public static Double _LATITUDE = 0.0;
    public static Double _LONGITUDE = 0.0;
    final String ITEM_SKU = "zeus_alerts";
    private static SpotsDialog spotsDialog = null;
    public static boolean IS_BASE_ACTIVITY_SHOWN = false;

    public static Context _Context = null;
    private static AlertDialog dialog;

    public static boolean IS_COMING_FROM_NOTIFICATION = false;
    public static boolean IS_LOGOUT_CLICKED = false;

    public static boolean IS_PREMIUM_PURCHASED = false;

    public static boolean IS_PAYMENT_ACTIVITY_OPEN = false;

    public static boolean IS_COMING_FROM_SMS_RECEIVER = false;
    public static boolean IS_APP_OPEN = false;
    public static Bitmap _Parse_User_In_trouble_Bitmap = null;
    public static Double _Parse_User_In_Trouble_Lat = null;
    public static Double _Parse_User_In_Trouble_Lng = null;
    public static String _Parse_User_In_Trouble_Distance = "";
    public static String _USER_CITY_NAME = "";
    public static boolean _Is_Logout_Clicked = false;
    public static boolean IS_FRISTTIMELOGIN = false;

    public static final String VOLEY_TAG = "volley";


    public Utils(Display _Display, Activity context) {


        this._Display = _Display;
        setScreenTotalWidth(800);
        setScreenTotalHeight(1280);

    }

    private static void setScreenTotalWidth(int screenTotalWidth) {
        Utils.screenTotalWidth = screenTotalWidth;
    }

    private static void setScreenTotalHeight(int screenTotalHeight) {
        Utils.screenTotalHeight = screenTotalHeight;
    }

    public static void TRANSLUCENT_STATUS_BAR(Activity activity) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = activity.getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

    }

    @Deprecated
    public static ColorStateList CHANGE_TEXT_COLOR(Context context,
                                                   int colorXml) {
        ColorStateList colorStateList = null;
        try {

            XmlResourceParser xmlResourceParser = context.getResources()
                    .getXml(colorXml);

            colorStateList = ColorStateList.createFromXml(context.getResources(), xmlResourceParser);

        } catch (Exception e) {
            // TODO: handle exception
        }

        return colorStateList;

    }

    public static boolean DETECT_INTERNET_CONNECTION(Activity context) {


        cd = new ConnectionDetector(context);

        if (cd.isConnectingToInternet())
            return true;
        else
            return false;

    }

    public static void TOAST_NO_INTERNET_CONNECTION(Context context, String text) {

        //  Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
        SnackbarManager.show(
                Snackbar.with(context)
                        .type(SnackbarType.MULTI_LINE)
                        .color(context.getResources().getColor(R.color.zeusColor))
                        .text(text));
    }

    public static void TOAST_WRONG_CREDENTIAL(Context context, String text) {

        //  Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
        SnackbarManager.show(
                Snackbar.with(context)
                        .type(SnackbarType.MULTI_LINE)
                        .color(context.getResources().getColor(R.color.zeusColor))
                        .text(text));
    }

    public static void TOAST_ERROR_RESPONSE(Context context, String text) {
        try {
            //  Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
            SnackbarManager.show(
                    Snackbar.with(context)
                            .type(SnackbarType.MULTI_LINE)
                            .color(context.getResources().getColor(R.color.zeusColor))
                            .text(text));

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public static void TOAST_SUCCESS_RESPONSE(Context context, String text) {

        //  Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
        SnackbarManager.show(
                Snackbar.with(context)
                        .type(SnackbarType.MULTI_LINE)
                        .color(context.getResources().getColor(R.color.zeusColor))
                        .text(text));

    }


    public static boolean IS_EMAIL_VALID(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static void HIDE_KEYBOARD(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void SAVE_USER_TO_SHAREDPREFS(Context context, User _User) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.remove("user").apply();
        Gson gson = new Gson();
        String json = gson.toJson(_User);
        prefsEditor.putString("user", json);
        prefsEditor.commit();

    }

    public static User GET_USER_FROM_SHARED_PREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("user", "");

        return gson.fromJson(json, User.class);
    }

    public static void RAEMOVAE_SHARED_PREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.remove("user").apply();
        prefsEditor.commit();

    }


    public static void SAVE_PASSWORD_TO_SHAREDPREFS(Context context, UserPassword _UserPassword) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.remove("password").apply();
        Gson gson = new Gson();
        String json = gson.toJson(_UserPassword);
        prefsEditor.putString("password", json);
        prefsEditor.commit();

    }

    public static UserPassword GET_PASSWORD_FROM_SHARED_PREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("password", "");

        return gson.fromJson(json, UserPassword.class);
    }



    public static void SAVE_TUTORIAL_TO_SHAREDPREFS(Context context, Tutorial _Tutorial) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(_Tutorial);
        prefsEditor.putString("tutorial", json);
        prefsEditor.commit();

    }

    public static Tutorial GET_TUTORIAL_FROM_SHARED_PREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("tutorial", "");

        return gson.fromJson(json, Tutorial.class);
    }


    public static void SAVE_CUSTOME_MESSAGE_TO_SHAREDPREFS(Context context, Custome_Message_And_Access_Token _Custome_message) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(_Custome_message);
        prefsEditor.putString("custome_message", json);
        prefsEditor.commit();

    }



    public static Custome_Message_And_Access_Token GET_CUSTOME_MESSAGE_FROM_SHARED_PREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("custome_message", "");

        return gson.fromJson(json, Custome_Message_And_Access_Token.class);
    }


    public static void SAVE_CONTACTS_TO_SHAREDPREFS(Context context, Contacts _Contacts) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(_Contacts);
        prefsEditor.putString("contacts", json);
        prefsEditor.commit();

    }

    public static Contacts GET_CONTACTS_FROM_SHAREDPREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("contacts", "");

        return gson.fromJson(json, Contacts.class);
    }


    public static void SAVE_CREDIT_CARD_CREDENTIALS_TO_SHAREDPREFS(Context context, Credit_Card_Crecentials card_crecentials) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(card_crecentials);
        prefsEditor.putString("card_crecentials", json);
        prefsEditor.commit();

    }

    public static Credit_Card_Crecentials GET_CREDIT_CARD_CREDENTIALS_FROM_SHARED_PREFS(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("card_crecentials", "");

        return gson.fromJson(json, Credit_Card_Crecentials.class);
    }


    @SuppressWarnings("deprecation")
    public static int getScreenWidth(int width) {
        return (Math.round(width * getDisplay().getWidth()
                / getScreenTotalWidth()));
    }

    private static Display getDisplay() {
        return _Display;
    }

    private static int getScreenTotalWidth() {
        return screenTotalWidth;
    }


    public void show_emial_Dialog(Context context, MaterialDialog mMaterialDialog) {
        mMaterialDialog = new MaterialDialog(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.progress_dialog, null);

        mMaterialDialog.setView(convertView);

        mMaterialDialog.show();


    }

    public static void CHANGE_COLOR_OF_ANDROID_STATUS_BAR(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            ((Activity) context).getWindow().setStatusBarColor(context.getResources().getColor(R.color.app_background_color));
        }
    }

  /*  public static boolean CHECK_GPS(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;
    }*/


  /*  public static void OPEN_GPS_ACTIVITY(final Context context) {


        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setTitle(context.getResources().getString(R.string.warning));
        mMaterialDialog.setMessage(context.getResources().getString(R.string.access_gps));
        mMaterialDialog.setPositiveButton(context.getResources().getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mMaterialDialog.dismiss();
                final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        mMaterialDialog.setNegativeButton(context.getResources().getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                _APP_SETTINGS = Utils.get_Application_Settings_From_Shared_Prefs(context);
//                _APP_SETTINGS.set_Location_hardCoded(true);
//                Utils.save_Application_Settings_To_Shared_Prefs(context, _APP_SETTINGS);

                mMaterialDialog.dismiss();
            }
        });
        mMaterialDialog.show();



    }*/

    public static void GET_LAST_KNOWN_LOCATION(Context context) {

        try{
            LocationManager lm = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);

            Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {

                Utils._LATITUDE = location.getLatitude();
                Utils._LONGITUDE = location.getLongitude();

                Log.e("zeus", "latitude is : " + Utils._LATITUDE);
                Log.e("zeus", "longitude is : " + Utils._LONGITUDE);
            }

        }catch (Exception e){}


    }

    public static Bitmap getBitmapFromUri(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static Bitmap compressed_Bitmap(Bitmap bitmap) {
        final int maxSize = 300;
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if (inWidth > inHeight) {
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, false);
        return resizedBitmap;
    }


    public static String GET_USER_CITY_NAME(Context context) {
        String cityName = "";
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(Utils._LATITUDE, Utils._LONGITUDE, 1);
            cityName = addresses.get(0).getAddressLine(1);
            Log.e("zeus", "city name : " + cityName);
            //Toast.makeText(context,"city name"+cityName,Toast.LENGTH_SHORT).show();

            String add = "";
            if (addresses.get(0).getSubLocality() != null)
                add = add + "," + addresses.get(0).getSubLocality();

            if (addresses.get(0).getLocality() != null)
                add = add + "," + addresses.get(0).getLocality();

            if (addresses.get(0).getAdminArea() != null)
                add = add + "," + addresses.get(0).getAdminArea();

            if (addresses.get(0).getCountryName() != null)
                add = add + "," + addresses.get(0).getCountryName();

            Log.e("IGA", "Address : " + add);
        } catch (Exception e) {

            Log.e("", "exception in getting location : " + e.toString());

        }

        if (cityName.equals("")) {
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(Utils._LATITUDE,
                        Utils._LONGITUDE, 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            String s = Utils._LATITUDE + "\n" + Utils._LATITUDE + "\n\nMy Current City is: "
                    + cityName;
        }

        Utils._USER_CITY_NAME = cityName;
        if(cityName != null)
        return cityName;
        else
        return "";
    }


    public static void SHOW_PURCHASE_DIALOG(Context context) {
        final String ITEM_SKU = "zeus_alerts";

        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result,
                                              Purchase purchase) {
                if (result.isFailure()) {
                    // Handle error
                    Log.e("", "faliure");
                    return;
                } else if (purchase.getSku().equals(ITEM_SKU)) {

                    Log.e("", "payment done succesfully");
                    consumeItem();

                }

            }
        };

        BaseListSample.mHelper.flagEndAsync();
        BaseListSample.mHelper.launchPurchaseFlow((Activity) context, ITEM_SKU, 10001,
                mPurchaseFinishedListener, "zeus_alerts");


    }




    private static void consumeItem() {
        final String ITEM_SKU = "zeus_alerts";
        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase,
                                                  IabResult result) {

                        if (result.isSuccess()) {
                            // BaseListSample._Is_Premium_Purchased = true;
                        } else {
                            // handle error
                        }
                    }
                };

        IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {


                if (result.isFailure()) {
                    // Handle failure
                } else {
                    BaseListSample.mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                            mConsumeFinishedListener);
                }
            }
        };
        BaseListSample.mHelper.queryInventoryAsync(mReceivedInventoryListener);


    }

    public static int text_view_text_size_dp(float size_in_px) {

        double TEXT_INC_RATIO = 1.0;
        if (getScreenTotalWidth() > 1920 && 1280 <= 2560) {
            TEXT_INC_RATIO = 0.6;
        } else {
            TEXT_INC_RATIO = 1;
        }


        int size = //size_in_px

                size = (int) (size_in_px * TEXT_INC_RATIO);

        return size;


    }

    public static void show_Dialog(Context context, String message) {

        dialog = new SpotsDialog(context, message);
        dialog.setTitle("Please Wait");
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void hide_dialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public static void HIDE_KEYBOARD(View myEditText, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    public static String toHtml(Object object) {
        StringBuilder stringBuilder = new StringBuilder(256);
        try {
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }
                Object val = field.get(object);
                stringBuilder.append("<b>");
                stringBuilder.append(field.getName().substring(1, field.getName().length()));
                stringBuilder.append(": ");
                stringBuilder.append("</b>");
                stringBuilder.append(val);
                stringBuilder.append("<br>");
            }
        } catch (Exception e) {
            // Do nothing
        }
        return stringBuilder.toString();
    }

    public static boolean _Is_Email_Valid(String email) {

//        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
//                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
//                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
//                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
//                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
//                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }


    public static void _Show_Ok_Dialog(final Context context, String title, String message) {

        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setTitle(title);
        mMaterialDialog.setMessage(message);
        mMaterialDialog.setPositiveButton(context.getResources().getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mMaterialDialog.dismiss();

            }
        });


        mMaterialDialog.show();

    }
}
