package zeus.alert.android.framgnets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import zeus.alert.android.R;

import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.activities.EditSingleContact;
import zeus.alert.android.activities.ZeusApplication;
import zeus.alert.android.adapter.ContactsListAdapter;
import zeus.alert.android.models.Contact;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Edit_Contact_Fragment extends Fragment {


    @InjectView(R.id.contact_ListView)
    ListView _Contact_LV;


    ContactsListAdapter adapter;

    @InjectView(R.id.no_contacts_added)
    TextView _No_Contacts_Tv;

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    private User _User;


    JsonObjectRequest register_User_Request_To_Server;

    private ArrayList<Contact> _Contacts = new ArrayList<Contact>();

    public static Edit_Contact_Fragment newInstance() {

        Edit_Contact_Fragment f = new Edit_Contact_Fragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_edit_contact, container, false);


        ButterKnife.inject(this, view);


        _User = Utils.GET_USER_FROM_SHARED_PREFS(getActivity());


        _Contact_LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long l) {


                if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {


                    Intent i = new Intent(getActivity(),
                            EditSingleContact.class);

                    Bundle bundle = new Bundle();
                    bundle.putParcelable("contact", _Contacts.get(arg2));
                    i.putExtras(bundle);

                    startActivity(i);
                } else {
                    Utils.TOAST_NO_INTERNET_CONNECTION(getActivity(), "" + getResources().getString(R.string.no_internet_connection));
                }


            }
        });


        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseListSample.mMenuDrawer.toggleMenu();
            }
        });


        return view;


    }

    private void get_Contact_List_From_parse() {

        //   ParseQuery<ParseObject> contactsQuery = ParseQuery.getQuery("Contacts");


        String register_User_Url = Webservices.GET_USER_CONTACTS(getActivity(), _User.get_User_Id());
        // Utils.show_Dialog(getActivity(), "Adding contact please wait.");

        Utils.show_Dialog(getActivity(), "Fetching Contacts");
        Log.e("jeeeni", "login url : " + register_User_Url);

        _Contacts.clear();

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                    if (_Status) {


                        JSONArray _contacts = response.getJSONArray("0");

                        Contact _Contact = null;

                        for (int i = 0; i < _contacts.length(); i++) {
                            JSONObject SingleContact = _contacts.getJSONObject(i);
                            _Contact = new Contact();


                            _Contact.set_Contact_Id(SingleContact.getString("id"));
                            _Contact.set_Contact_Name(SingleContact.getString("contact_name"));
                            _Contact.set_Contact_Parent_Id(SingleContact.getString("unique_id"));
                            _Contact.set_Phone_Number(SingleContact.getString("user_phone_number"));

                            _Contacts.add(_Contact);


                        }


                        if (_Contacts.size() > 0) {

                            _No_Contacts_Tv.setVisibility(View.GONE);
                            _Contact_LV.setVisibility(View.VISIBLE);

                            adapter = new ContactsListAdapter(getActivity(), _Contacts);
                            _Contact_LV.setAdapter(adapter);
                        } else {
                            _No_Contacts_Tv.setVisibility(View.VISIBLE);
                            _Contact_LV.setVisibility(View.GONE);
                        }

                    } else {

                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);



    }

    @Override
    public void onResume() {
        super.onResume();


        if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {
            get_Contact_List_From_parse();
        } else {
            _No_Contacts_Tv.setVisibility(View.VISIBLE);
            _Contact_LV.setVisibility(View.GONE);
            _No_Contacts_Tv.setText("No Internet connection. Try again");
            _No_Contacts_Tv.setTextSize(Utils.text_view_text_size_dp(16));
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.TOAST_NO_INTERNET_CONNECTION(getActivity(), "" + getResources().getString(R.string.no_internet_connection));
                        }
                    });
                }
            }).start();
        }
    }
}
