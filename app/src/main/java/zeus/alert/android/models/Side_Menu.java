package zeus.alert.android.models;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Side_Menu {

    private String _Menu_Name;
    private int _Menu_Image;
    private int color;
    private int _Row_Clicked = -1;

    public int get_Row_Clicked() {
        return _Row_Clicked;
    }

    public void set_Row_Clicked(int _Row_Clicked) {
        this._Row_Clicked = _Row_Clicked;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String get_Menu_Name() {
        return _Menu_Name;
    }

    public void set_Menu_Name(String _Menu_Name) {
        this._Menu_Name = _Menu_Name;
    }

    public int get_Menu_Image() {
        return _Menu_Image;
    }

    public void set_Menu_Image(int _Menu_Image) {
        this._Menu_Image = _Menu_Image;
    }
}
