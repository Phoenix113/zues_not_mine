package zeus.alert.android.receivers;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

public class GpsLocationReceiver extends BroadcastReceiver {
    String locationMode;

    @Override
    public void onReceive(Context context, Intent intent) {
       /* if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            Toast.makeText(context, "in android.location.PROVIDERS_CHANGED", Toast.LENGTH_SHORT).show();
        }*/

        ContentResolver contentResolver = context.getContentResolver();
        // Find out what the settings say about which providers are enabled
        int mode = Settings.Secure.getInt(
                contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);
        try {
            if (mode == Settings.Secure.LOCATION_MODE_OFF) {
                Toast.makeText(context, "Location  turn off...", Toast.LENGTH_SHORT).show();
                // Location is turned OFF!
            } else {
                // Location is turned ON!

                // Get the Mode value from Location system setting
                LocationManager locationManager = (LocationManager) context.
                        getSystemService(Context.LOCATION_SERVICE);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = "High accuracy. Uses GPS, Wi-Fi, and mobile networks to determine location";
                } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    locationMode = "Device only. Uses GPS to determine location";
                } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = "Battery saving. Uses Wi-Fi and mobile networks to determine location";
                }
                Log.d("Sid", "Location status :: " + locationMode);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}