package zeus.alert.android.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import zeus.alert.android.models.User;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;


/**
 * Created by Moubeen Warar on 4/16/2015.
 */
public class Zeus_Service extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    private Handler handler = new Handler();


    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;


    public static final String BROADCAST_ACTION = "Hello World";

    private static final int TWO_MINUTES = 1;
    public MyLocationListener listener;
    public Location previousBestLocation = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        intent = new Intent(BROADCAST_ACTION);
        mGoogleApiClient.connect();


        return START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();


    }


    @Override
    public IBinder onBind(Intent intent) {


        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (mGoogleApiClient.isConnected()) {
            Log.d("RRR", "onConnected call from Zeus_Service :: ");
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
            mLocationRequest.setInterval(10000); // Update location every second
            listener = new MyLocationListener();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, listener);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(final Location loc) {
                Log.i("*********************", "Location changed");
            if (isBetterLocation(loc, previousBestLocation)) {
           //     Log.e("Lat/Long : ", "" + loc.getLatitude() + "/" + loc.getLongitude());
                //  Log.d("Provider : ", "" + loc.getProvider());



                Utils._LATITUDE = Double.parseDouble(String.valueOf(loc.getLatitude()));
                Utils._LONGITUDE = Double.parseDouble(String.valueOf(loc.getLongitude()));

           // Log.d("rathod",""+Double.parseDouble(String.valueOf(loc.getLatitude())));
                    User user=Utils.GET_USER_FROM_SHARED_PREFS(getApplicationContext());

                if(user!=null){

                    UpdateLngLat(user.get_User_Id(),Double.parseDouble(String.valueOf(loc.getLatitude())),Double.parseDouble(String.valueOf(loc.getLongitude())));

                }


            }
        }


    }

    private void UpdateLngLat(final String user_id, final double lat, final double lng) {

        Log.d("rathod",""+lat);
        Log.d("RRR","latitude"+lat);


                //Utils.show_Dialog(getActivity(),"Updating Credentials");
                String Update_User_Url = Webservices.UPDATE_USER_LAT_LNG(getApplicationContext());
                //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();


                StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_User_Url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                //Disimissing the progress dialog
                                //    Utils.hide_dialog();
                                //Showing toast message of the response
                                Log.d("rathod",""+s);
                                try {
                                    JSONObject response = new JSONObject(s);
                                    boolean _Status = response.getBoolean("status");
                                    if (_Status) {


                                        JSONObject jsonObject= response.getJSONObject("user");
                                        User _User = new User();
                                        _User.set_User_Id(jsonObject.getString("uid"));
                                        _User.set_User_Name(jsonObject.getString("username"));
                                        _User.set_Email(jsonObject.getString("email"));
                                        _User.set_Phone(jsonObject.getString("phone_number"));

                                        if(jsonObject.getString("is_safe").equals("1"))
                                        {
                                            _User.set_Safe(true);
                                        }else {
                                            _User.set_Safe(false);
                                        }
                                        if (jsonObject.getString("is_login").equals("1")){
                                            _User.set_Login(true);
                                        }else {
                                            _User.set_Login(false);
                                        }
                                        Log.d("RRR","latitude"+jsonObject.getString("lat"));
                                        Log.d("RRR","lon"+jsonObject.getString("lon"));
                                        _User.set_Lat(jsonObject.getString("lat"));
                                        _User.set_Lat(jsonObject.getString("lon"));
                                        _User.set_User_Type(jsonObject.getString("usertype_id"));
                                        _User.set_Height(jsonObject.getString("height"));
                                        _User.set_Weight(jsonObject.getString("weight"));

                                        _User.set_Img_Path(jsonObject.getString("img_path"));
                                        Log.d("user",""+_User.get_User_Name());

                                        Utils.SAVE_USER_TO_SHAREDPREFS(getApplicationContext(), _User);




                                    } else {
                                        Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "" + response.getString("error_msg"));
                                    }
                                    Utils.hide_dialog();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Utils.hide_dialog();
                                    Log.d("rathod","er in res");
                                }


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                //Dismissing the progress dialog
                                // Utils.hide_dialog();

                                //Showing toast
                                //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                        }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {


                        Map<String,String> params = new Hashtable<String, String>();
                        params.put("userid", user_id);
                        params.put("lat",""+lat);
                        params.put("lng", ""+lng);


                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);










    }

    protected boolean isBetterLocation(Location location,
                                       Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use
        // the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be
            // worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
                .getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and
        // accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate
                && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

}
