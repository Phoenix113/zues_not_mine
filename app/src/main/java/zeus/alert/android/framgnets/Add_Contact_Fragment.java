package zeus.alert.android.framgnets;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import zeus.alert.android.activities.ZeusApplication;
import zeus.alert.android.models.User;

import zeus.alert.android.R;

import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Add_Contact_Fragment extends Fragment {


    @InjectView(R.id.add_contact_Btn)
    Button _Add_Contact;


    @InjectView(R.id.pick_from_contacts_Btn)
    Button _Pick_From_Contacts;


    @InjectView(R.id.name_ET)
    EditText _Name_Et;

    @InjectView(R.id.phone_number_ET)
    EditText _Phone_Number_Et;



    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;


    private static final int PICK_CONTACT = 1;


    JsonObjectRequest register_User_Request_To_Server;

    // login user name
    String _userName;

    // add into emergency contact.
    String _addUserName;
    String _addUserNumber;



    private User _User = null;

    public static Add_Contact_Fragment newInstance() {

        Add_Contact_Fragment f = new Add_Contact_Fragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_contact, container, false);

        ButterKnife.inject(this, view);

        _User = Utils.GET_USER_FROM_SHARED_PREFS(getActivity());

        _userName = _User.get_User_Name();

        _Pick_From_Contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickIntent();
            }
        });


        _Add_Contact.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {


                                                    String name = _Name_Et.getText().toString();
                                                    String phoneNumber = _Phone_Number_Et.getText().toString();


                                                    if (TextUtils.isEmpty(name) || TextUtils.isEmpty(phoneNumber) ) {
                                                        Utils.TOAST_WRONG_CREDENTIAL(getActivity(), "Please fill into all fields.");
                                                    } else {
                                                        if ( phoneNumber.contains(" ")){
                                                            Utils.TOAST_WRONG_CREDENTIAL(getActivity(), "Phonenumber cannot contain spaces.");

                                                        }else {

                                                            addContact();
                                                        }
                                                    }


                                                } else {
                                                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "No Internet Connection.");
                                                }

                                            }
                                        }

        );


        _Back_Button.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick(View view) {
                                                BaseListSample.mMenuDrawer.toggleMenu();
                                            }
                                        }

        );



//        if (App_Home_Page.Is_Premium_Clicked) {
//            if (!BaseListSample._Is_Premium_Purchased) {
//                Utils.SHOW_PURCHASE_DIALOG(getActivity());
//                App_Home_Page.Is_Premium_Clicked = false;
//            }
//
//        }
        return view;





    }

    private void PickIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK && data != null) {

                    Uri contactData = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactData, null, null,
                            null, null);
                    if (c.moveToFirst()) {

                        String id = c
                                .getString(c
                                        .getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone = c
                                .getString(c
                                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        _Name_Et.setText(c.getString(c
                                .getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getActivity().getContentResolver()
                                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                            null,
                                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                                    + " = " + id, null, null);
                            phones.moveToFirst();
                            _Phone_Number_Et.setText(phones.getString(phones
                                    .getColumnIndex("data1")));
                            phones.close();
                        }
                        Cursor emailCur = getActivity().getContentResolver().query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID
                                        + " = ?", new String[]{id}, null);
                        while (emailCur.moveToNext()) {
                            // This would allow you get several email addresses
                            // if the email addresses were stored in an array
                            String email = emailCur
                                    .getString(emailCur
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                         //   _Email_ET.setText(email);
                        }
                        emailCur.close();
                    }
                }
                break;
        }
    }

    private void addContact() {
        // TODO Auto-generated method stub

        final String name = _Name_Et.getText().toString();
        final String phoneNumber = _Phone_Number_Et.getText().toString();

        _addUserName = _Name_Et.getText().toString().trim();
        _addUserNumber = _Phone_Number_Et.getText().toString().trim();


        String register_User_Url = Webservices.ADD_USER(getActivity(), _User.get_User_Id(), name, phoneNumber);
        Utils.show_Dialog(getActivity(), "Adding contact please wait.");

        Log.e("jeeeni", "login url : " + register_User_Url);

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                   // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                    if (_Status) {

                        //send messgae when add to emergaency contact
                        //Hey! (First and Last Name) has added you as their emergency contact on Zeus Alert. - www.zeusalert.com
                        sendMessage("Hey! ( "+_userName+" ) has added you as their emergency contact on Zeus Alert. - www.zeusalert.com");

                        Utils.TOAST_SUCCESS_RESPONSE(getActivity(), "Contact Saved Successfully");
                  /*      String message = "Hello, (" +_User.get_User_Name() + ") has added you as an emergency contact for our application. Anytime (" + _User.get_User_Name() + ") is in danger, we will send you alerts in real-time. Thanks! Try our App Zeus – www.zeusapp.me";
                        sendSMS(phoneNumber, message, "" + name);*/
                    } else {

                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);


//        String email = _Email_ET.getText().toString();
//        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
//            Utils.TOAST_WRONG_CREDENTIAL(getActivity(), "Please fill into all fields.");
//        } else {
//
//            if (Utils._Is_Email_Valid(email)) {
//                Calendar c = Calendar.getInstance();
//                SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm");
//                // contacts.dateTime = sdf.format(c.getTime());
//                contact = new ParseObject("Contacts");
//                contact.put("user_object", ParseUser.getCurrentUser());
//                contact.put("name", name);
//                contact.put("phone", phoneNumber);
//                contact.put("email", email);
//                Log.e("", "objext id is : " + contact.getObjectId());
//
//                Utils.show_Dialog(getActivity(), "Saving Contact");
//                contact.saveInBackground(new SaveCallback() {
//
//
//                    @Override
//                    public void done(com.parse.ParseException e) {
//
//                        if (e == null) {
//
//                            Utils.TOAST_SUCCESS_RESPONSE(getActivity(), "Contact Saved Successfully");
//                            Log.e("", "contact saved :");
//
//                            Contacts_Details _Contact_Detail = new Contacts_Details();
//                            _Contact_Detail.set_Name(_Name_Et.getText().toString().trim());
//                            _Contact_Detail.set_Phone_Number(_Phone_Number_Et.getText().toString().trim());
//
//                            Contacts contacts = Utils.GET_CONTACTS_FROM_SHAREDPREFS(getActivity());
//
//                            if (contacts == null) {
//                                contacts = new Contacts();
//                                contacts.get_Contact_Details().add(_Contact_Detail);
//                                Utils.SAVE_CONTACTS_TO_SHAREDPREFS(getActivity(), contacts);
//
//                            } else {
//
//                                contacts.get_Contact_Details().add(_Contact_Detail);
//                                Utils.SAVE_CONTACTS_TO_SHAREDPREFS(getActivity(), contacts);
//
//                            }
//
//                            String message = "Hello, (" + ParseUser.getCurrentUser().getString("name") + ") has added you as an emergency contact for our application. Anytime (" + ParseUser.getCurrentUser().getString("name") + ") is in danger, we will send you alerts in real-time. Thanks! Try our App Zeus – www.zeusapp.me";
//
//                            sendSMS(phoneNumber, message, "" + name);
//
//                        } else {
//
//                            Utils.TOAST_ERROR_RESPONSE(getActivity(), "Oops!Can't Save Contact");
//
//                            Log.e("", "contact can't saved :" + e.getMessage());
//                        }
//                        Utils.hide_dialog();
//                    }
//                });
//            } else {
//                Utils.TOAST_ERROR_RESPONSE(getActivity(), "Invalid Email Address");
//            }
//
//
//        }

    }

// sms send when add conect
   /* private void sendSMS(String phoneNumber, String message, final String name) {
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();
        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
                new Intent(getActivity(), SmsSentReceiver.class), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0,
                new Intent(getActivity(), SmsDeliveredReceiver.class), 0);
        try {
            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> mSMSMessage = sms.divideMessage(message);
            for (int i = 0; i < mSMSMessage.size(); i++) {
                sentPendingIntents.add(i, sentPI);
                deliveredPendingIntents.add(i, deliveredPI);
            }
            sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage,
                    sentPendingIntents, deliveredPendingIntents);

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Utils.TOAST_SUCCESS_RESPONSE(getActivity(), "Message Sent To " + name);
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

            Utils.TOAST_ERROR_RESPONSE(getActivity(), "SMS sending failed...");
        }

    }*/


    //mesage send service
    private void sendMessage( final String msg) {
        Log.d("msg",msg);


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.REGISTORUSERMESSAGESEND(getContext());
        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                Utils.TOAST_SUCCESS_RESPONSE(getContext(),"Message Send Successfully");

                                // clean all edittext
                                _Name_Et.setText("");
                                _Phone_Number_Et.setText("");
                            }
                        } catch (JSONException e) {
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("num",_addUserNumber);
                params.put("msg",msg);
                Log.d("RRR","num :: "+_addUserNumber);
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);

    }

}
