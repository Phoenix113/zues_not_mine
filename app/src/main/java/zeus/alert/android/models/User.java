package zeus.alert.android.models;

import android.graphics.Bitmap;

/**
 * Created by Moubeen Warar on 11/7/2015.
 */
public class User {


    private String _User_Id = "";
    private Bitmap _Bitmap = null;
    private String _User_Name = "";
    private String _Password = "";
    private String _Email = "";
    private boolean _Login = false;
    private String _Phone = "";
    private boolean _Safe = true;
    private String _Lat;
    private String _Lng;
    private String _Height = "";
    private String _Weight = "";
    private String _Distance = "";
    private String _Img_Path="";
    private String _User_Type="";
    private String _U_Token;

    public String get_U_Token() {
        return _U_Token;
    }

    public void set_U_Token(String _U_Token) {
        this._U_Token = _U_Token;
    }

    public String get_User_Type() {
        return _User_Type;
    }

    public void set_User_Type(String _User_Type) {
        this._User_Type = _User_Type;
    }

    public String get_Img_Path() {
        return _Img_Path;
    }

    public void set_Img_Path(String _Img_Path) {
        this._Img_Path = _Img_Path;
    }

    public String get_Distance() {
        return _Distance;
    }

    public void set_Distance(String _Distance) {
        this._Distance = _Distance;
    }

    public String get_Height() {
        return _Height;
    }

    public void set_Height(String _Height) {
        this._Height = _Height;
    }

    public String get_Weight() {
        return _Weight;
    }

    public void set_Weight(String _Weight) {
        this._Weight = _Weight;
    }

    public String get_Lat() {
        return _Lat;
    }

    public void set_Lat(String _Lat) {
        this._Lat = _Lat;
    }

    public String get_Lng() {
        return _Lng;
    }

    public void set_Lng(String _Lng) {
        this._Lng = _Lng;
    }

    public boolean is_Safe() {
        return _Safe;
    }

    public void set_Safe(boolean _Safe) {
        this._Safe = _Safe;
    }

    public Bitmap get_Bitmap() {
        return _Bitmap;
    }

    public void set_Bitmap(Bitmap _Bitmap) {
        this._Bitmap = _Bitmap;
    }

    public String get_User_Id() {
        return _User_Id;
    }

    public void set_User_Id(String _User_Id) {
        this._User_Id = _User_Id;
    }

    public String get_Phone() {
        return _Phone;
    }

    public void set_Phone(String _Phone) {
        this._Phone = _Phone;
    }

    public String get_User_Name() {
        return _User_Name;
    }

    public void set_User_Name(String _User_Name) {
        this._User_Name = _User_Name;
    }

    public String get_Password() {
        return _Password;
    }

    public void set_Password(String _Password) {
        this._Password = _Password;
    }

    public boolean is_Login() {
        return _Login;
    }

    public void set_Login(boolean _Login) {
        this._Login = _Login;
    }

    public String get_Email() {
        return _Email;
    }

    public void set_Email(String _Email) {
        this._Email = _Email;
    }
}
