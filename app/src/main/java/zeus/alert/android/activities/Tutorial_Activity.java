package zeus.alert.android.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import zeus.alert.android.R;
import zeus.alert.android.framgnets.TutorialFragment;
import zeus.alert.android.interfaces.TutorialView;
import zeus.alert.android.utils.Utils;

public class Tutorial_Activity extends FragmentActivity {

    private ViewPager pager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        Utils.IS_FRISTTIMELOGIN=true;
        Utils.TRANSLUCENT_STATUS_BAR(this);
        pager = (ViewPager) findViewById(R.id.viewPager);

        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        pager.setOffscreenPageLimit(6);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(final int pos) {

            final TutorialFragment tutorialView= new TutorialFragment();
            tutorialView.setFragmentNumber(pos);
            tutorialView.setListener(new TutorialView() {
                @Override
                public void onNextClick() {
                    pager.setCurrentItem(pos + 1);

                }

                @Override
                public void onBackClick() {
                    pager.setCurrentItem(pos - 1);
                }
            });

            return tutorialView;
        }


        @Override
        public int getCount() {
            return 6;
        }
    }
}