package zeus.alert.android.models;

/**
 * Created by Moubeen Warar on 11/12/2015.
 */
public class Contacts_Details {


    private String _Name = "";
    private String _Phone_Number = "";
    private String _Object_Id = "";


    public String get_Object_Id() {
        return _Object_Id;
    }

    public void set_Object_Id(String _Object_Id) {
        this._Object_Id = _Object_Id;
    }

    public String get_Name() {
        return _Name;
    }

    public void set_Name(String _Name) {
        this._Name = _Name;
    }

    public String get_Phone_Number() {
        return _Phone_Number;
    }

    public void set_Phone_Number(String _Phone_Number) {
        this._Phone_Number = _Phone_Number;
    }
}
