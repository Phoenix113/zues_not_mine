package zeus.alert.android.activities;

import java.util.HashMap;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;

import com.facebook.login.DefaultAudience;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

public class ZeusApplication extends com.activeandroid.app.Application {

    // The following line should be changed to include the correct property id.
  private static final String PROPERTY_ID = "UA-56702533-1";
  //  private static final String PROPERTY_ID = " UA-87367922-1";


    // Logging TAG
    // private static final String TAG = "MyApp";

    public static int GENERAL_TRACKER = 0;




    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
        // roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
        // company.
    }
    private static final String APP_ID = "751309021669116";
    private static final String APP_NAMESPACE = "zeus";

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public static final String TAG = "Zeus";

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static ZeusApplication mInstance;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

     //   Parse.enableLocalDatastore(this);
    //    Parse.initialize(this, "TFBaGoL5bQ1iDCkUMjfKoPWwZBb8VYu4ZW4kHuUR",
      //          "BpIQCX5TKe1Re5T2urfx3VwpOPCecmkL71q7D0KG");
        mInstance = this;
        //   FacebookSdk.sdkInitialize(getApplicationContext());


      /*  ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push",
                            "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });*/
      //  ParseUser.enableAutomaticUser();
      //  ParseACL defaultACL = new ParseACL();
        // If you would like all objects to be private by default, remove this
        // line.
      //  defaultACL.setPublicReadAccess(true);
      //  defaultACL.setPublicWriteAccess(true);
       // ParseACL.setDefaultACL(defaultACL, true);


        Permission[] permissions = new Permission[] {
                Permission.PUBLIC_PROFILE,
                Permission.EMAIL,
                Permission.PUBLISH_ACTION };

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(  "" + getResources().getString(R.string.facebook_app_id))
                .setNamespace(APP_NAMESPACE)
                .setPermissions(permissions)
                .setDefaultAudience(DefaultAudience.FRIENDS)
                .setAskForAllPermissionsAtOnce(false)
                        .setGraphVersion("v2.3")
                .build();

        SimpleFacebook.setConfiguration(configuration);
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics
                    .newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
                    .newTracker(PROPERTY_ID) : analytics
                    .newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized ZeusApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
